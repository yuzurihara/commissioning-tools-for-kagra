# commissioning tools for KAGRA

Main developers are members from Aoyama Gakuin university

## Name
Commissioning tools for KAGRA 

## Description
* In this repo, we store and share the code for python script for making plots such as histogram, scatter plot, and for TCam image analysis.
* Mainly this project is lead by Aogaku members and ICRR members.

* このリポジトリは青学メンバーが開発したコードを共有する場所です

## Installation
* スクリプトはm31で走らせます。アカウントが必要でしたら[document](https://gwdoc.icrr.u-tokyo.ac.jp/cgi-bin/private/DocDB/ShowDocument?docid=12137) の中の`Main Data Server`を見てください
`.

* m31でコードを走らせるには、conda環境が必要です (m31に共通で使えるconda環境がありますが、役に立たないことがわかりました)
* conda環境の作り方は譲原が大林さんにすべて伝えたので、大林さんに聞いてください
  * 疑問点などは5分考えてわからなければ、slackで譲原に聞いてください


## 使い方
* スクリプトの中に使い方がわかるようなヘルプメッセージがほしいです
* または、スクリプトに引数を与えずに実行したときにヘルプメッセージが表示されてほしいです

## Support
* Contact the developper of each script or Yuzurihara.

## Roadmap
* These tool will support KAGRA's commissioning planned during 2022 summer.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

* (list up the names of Aogaku members)
* (ICRR members)

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
