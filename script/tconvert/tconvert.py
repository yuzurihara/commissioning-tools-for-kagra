#!/usr/bin/env python
# coding: utf-8

"""
script to convert time format from GPS/JST/UTC to GPS/JST/UTC
author : Hirotaka Yuzurihara
"""

from gwpy.time import tconvert, to_gps
import datetime
import argparse
import sys

#
# setting all options
#
parser = argparse.ArgumentParser(
    prog = 'tconvert.py', 
    description = 'script to convert time format from GPS/JST/UTC to GPS/JST/UTC',
    formatter_class=argparse.RawTextHelpFormatter    
    )

parser.add_argument("time_input", type=str,
                           help="[str] input time format\n\nsample\nJST to GPS : tconvert.py -o g '2023/03/03 0:0:0 JST'\nUTC to GPS : tconvert.py -o g '2023/03/03 0:0:0 UTC'\nGPS to UTC : tconvert.py -o u 1234567890\nGPS to JST : tconvert.py -o j 1234567890\nnow to GPS : tconvert.py -o g 'now'")
parser.add_argument('-o','--out', choices = ['GPS', 'gps', 'g', 'JST', 'jst', 'j', 'UTC', 'utc', 'u'], required = True,
                           help = '[choice] Select time format of output (ex : -o gps, -o jst, -o utc)')
args = parser.parse_args()
#################################################################################################

time_in = args.time_input
flag_out_time = args.out
timezone = ""

if flag_out_time in ["GPS", "gps", "g"]:
    time_out = to_gps(time_in)
elif flag_out_time in ["JST", "jst", "j"]:
    time_out = tconvert(time_in)
    time_out = time_out + datetime.timedelta(hours=9)    
    timezone = "JST"
elif flag_out_time in ["UTC", "utc", "u"]:
    time_out = tconvert(time_in)
    timezone = "UTC"
    time_out = tconvert(time_in)
else:
    print("error : option (%s) is invalid." % flag_out_time.out)
    sys.exit(1)
    
print("%s %s" % (time_out, timezone))
#print(type(time_out))

