#!/usr/bin/env python
# coding: utf-8
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
from gwpy.signal import filter_design
from gwpy.astro import inspiral_range
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
import scipy.stats
import math
import sys
import os
import warnings
# warnings.filterwarnings('ignore')
import matplotlib as mpl


# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
ch = sys.argv[0]
time_type = sys.argv[1]

# getting time
time_type == "GPS"
time_beg = sys.argv[2]
time_end = sys.argv[3]
gps_beg = int(time_beg)
gps_end = int(time_end)
gps_beg_head = int(gps_beg/100000)
gps_end_head = int(gps_end/100000)
data_beg = gps_beg
data_end = gps_end

if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head 
    cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  

data = TimeSeries.read(cache_file, ch, start=gps_beg, end=gps_end, nproc=4, verbose=True)
print(data)

# fftlength
fftl = sys.argv[4]
sg = data.spectrogram2(fftlength=fftl, overlap=int(fftl)/2, window='hann') ** (1 / 2.)
# data.plot(xlim=(1270281618,1270281618.1))

median = sg.percentile(50)
low = sg.percentile(10)
high = sg.percentile(90)
print(median)
print(low)
print(high)

# prepare saving png file
span = data_end - data_beg
dirname = "%s_%s/" % (ch[3:], span)
pngname1 = "fftl=%s[%s-%s]_asd_lin2.png" %(fftl, data_beg, data_end)
pngname2 = "fftl=%s[%s-%s]_stg_lin2.png" %(fftl, data_beg, data_end)
os.makedirs(dirname, exist_ok=True)
filename1 = dirname + pngname1
filename2 = dirname + pngname2

plot = Plot()
xmin = float(sys.argv[5])
xmid = float(sys.argv[6])
xmax = float(sys.argv[7])
color = sys.argv[8]


plot = plt.figure(figsize=(15, 6))
ax1 = plot.add_subplot(211,
    xscale='linear',
    xlim=(xmin, xmid),
    xlabel='Frequency [Hz]',
    yscale='log',
    ylabel='ASD [%s]'% median.unit,
)
ax1.plot_mmm(median, low, high, color=color)
ax1.set_title('%s \n [%s-%s]' %(ch, gps_beg, gps_end),fontsize=16)

ax2 = plot.add_subplot(212,
    xscale='linear',
    xlim=(xmin, xmax),
    xlabel='Frequency [Hz]',
    yscale='log',
    ylabel='ASD [%s]'% median.unit,
)
ax2.plot_mmm(median, low, high, color=color)
# ax2.set_title('%s \n [%s-%s]' %(ch, gps_beg, gps_end),fontsize=16)
plt.tight_layout()
plt.savefig(filename1)

# spectrogram
plot = sg.imshow(
    norm='log',
    # vmin=5e-7,
    # vmax=2e-3
)
ymin = median.df.value
ymax = max(median.frequencies.value)
ax = plot.gca()
ax.set_yscale('linear')
ax.set_ylim(ymin, ymax)
ax.colorbar(label=r'Amplitude Spectral Density [%s]' % sg.unit)
ax.set_title('%s \n [%s-%s] fftl=%s' %(ch, gps_beg, gps_end, fftl),fontsize=16)
plt.savefig(filename2)

# saving data file
savename = "fftl=%s[%s-%s]" %(fftl, data_beg, data_end)
np.savez(dirname+savename, 
         frequency=median.frequencies, 
         asd_10p=low.value, 
         asd_50p=median.value, 
         asd_90p=high.value
        )

print("good")
plt.show()