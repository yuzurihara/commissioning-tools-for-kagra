#!/bin/bash

echo "変更前"
cat Xarm.yml
echo "変更後"
sed -e 's/PRM/IMMT2/g' ./Xarm.yml > ./Xarm_new.yml
cat Xarm_new.yml

ndscope --nds localhost:8002 -t "2022/04/20 21:00:0 JST" Xarm_new.yml
