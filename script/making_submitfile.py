#!/usr/bin/env python
# coding: utf-8
"""
# This is making submit file code 
Making by Mayu Murakoshi
"""

from gwpy.time import from_gps, to_gps
import numpy as np
import argparse

# input parameter =======================================================================
plotparser = argparse.ArgumentParser(
    prog = 'making_submitfile', 
    description = 'The file making .sub file for cavity_scatter_timeseries_plot.py.',
    epilog = 'Command example : python making_submitfile.py -ch K1:LSC-TR_IRX_NORM_OUT_DQ -glitgps 1366350533 -type GPS -s 60'
    )

requiredNamed = plotparser.add_argument_group('required arguments')
requiredNamed.add_argument('-ch','--ch', choices = ['K1:LSC-TR_IRX_NORM_OUT_DQ', 'K1:LSC-TR_IRY_NORM_OUT_DQ'], help = '[choice] Name of the channel-IRX/IRY.')

prerequiredNamed = plotparser.add_argument_group('pre-required arguments (Select time type from GPS time or JST.)')
prerequiredNamed.add_argument('-type','--timetype', choices = ['JST', 'GPS'], help = '[choice] Which type of glitch time.')
prerequiredNamed.add_argument('-glitjstd','--glitchjstday', type=str, help = '[str] glitch JST day of ch. e.g., 2023/4/24')
prerequiredNamed.add_argument('-glitjsthms','--glitchjsthms', type=str, help = '[str] glitch JST hours, minutes, and seconds of ch. e.g., 14:48:35')
prerequiredNamed.add_argument('-glitgps','--glitchgpstime', type=int, help = '[int] glitch GPS time of ch.')

optionalNamed = plotparser.add_argument_group('optional arguments')
optionalNamed.add_argument('-s','--span', type = int, default = 60, help = '[int] Time of data to obtain. unit: seconds. Default = 60 s.')

args = plotparser.parse_args()

ch = args.ch
time_type = str(args.timetype)
jst_time_day = str(args.glitchjstday)
jst_time_hms = str(args.glitchjsthms)
gps = args.glitchgpstime
span = args.span
# ======================================================================================

# output input parame
print("this submit file for cavity_scatter_timeseries_plot.py")
print("channel  :", ch)
if time_type == "JST":
    print("jst      : %s %s" % (jst_time_day, jst_time_hms))
if time_type == "GPS":
    print("gps      :", gps)
print("span     : %s s" % span)

# making submit file
pythonfile = 'cavity_scatter_timeseries_plot'

f = open('%s.sub' % pythonfile, 'w')

if time_type == "JST":
    datalist = [
        '# submit file for runing %s.py\n' % pythonfile,
        '# Argments is [channel-IRX/IRY] [JST] [glitch_ymd] [glitch_hms] [span]\n',
        '\n',
        'Universe = Vanilla\n', 
        'request_memory = 10 GB\n',
        'request_cpus = 16\n',
        '\n',
        'Executable = %s.py\n' % pythonfile,
        'Log = %s.log\n' % pythonfile,
        'Output = %s.out\n'% pythonfile,
        'Error = %s.err\n'% pythonfile,
        'Getenv = True\n',
        '\n',
        'Arguments = %s %s %s %s %s\n' %(ch, time_type, jst_time_day, jst_time_hms, span),
        'Queue'
        ]

if time_type == "GPS":
    datalist = [
        '# submit file for runing %s.py\n' % pythonfile,
        '# Argments is [channel-IRX/IRY] [GPS] [glitch_time] [span]\n',
        '\n',
        'Universe = Vanilla\n', 
        'request_memory = 10 GB\n',
        'request_cpus = 16\n',
        '\n',
        'Executable = %s.py\n' % pythonfile,
        'Log = %s.log\n' % pythonfile,
        'Output = %s.out\n'% pythonfile,
        'Error = %s.err\n'% pythonfile,
        'Getenv = True\n',
        '\n',
        'Arguments = %s %s %s %s\n' %(ch, time_type, gps, span),
        'Queue'
        ]

f.writelines(datalist)
f.close()

print()
print("use 'condor_submit %s.sub'" % pythonfile)
