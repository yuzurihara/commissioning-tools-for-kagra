#!/usr/bin/env python
# coding: utf-8
"""
This is two channels Pearson's correlation coefficient histogram plot code
Making by Mayu Murakoshi
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict
from gwpy.plot import Plot
from matplotlib import pyplot as plt
from gwpy.time import from_gps, to_gps
import numpy as np
import sys
import os
import warnings
warnings.filterwarnings('ignore')

# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

print("---------------------------------------")
del sys.argv[0]
print("channel : %s " % sys.argv[0:2])

ch_x = sys.argv[0]
ch_y = sys.argv[1]
ch = sys.argv[0:2]
time_type = sys.argv[2]

ch_x_title = ch_x.replace('K1:', '')
ch_y_title = ch_y.replace('K1:', '')

# getting time
if time_type == "JST":
    glitch_year =sys.argv[3]
    glitch_month =sys.argv[4]
    glitch_day =sys.argv[5]
    glitch_time =sys.argv[6]
    glitch_time_title = glitch_time.replace(':', '')

    glitch_jst_beg = glitch_year + "/" + glitch_month + "/" + glitch_day + " " + glitch_time + " " + time_type
    glitch_jst_title = glitch_year + "-" + glitch_month + "-" + glitch_day + " " + glitch_time_title
    glitch_gps = int(to_gps( glitch_jst_beg ))

elif time_type == "GPS":
    glitch_time =sys.argv[3]
    glitch_gps = int(glitch_time)

# conversion of time
glitch_gps_beg = glitch_gps    # assume the glitch is for one second 
glitch_gps_end = glitch_gps + 1
glitch_gps_beg_head = int(glitch_gps_beg/100000)
glitch_gps_end_head = int(glitch_gps_end/100000)
# ---------------------------------------
back_gps_beg = glitch_gps - 60     # one minute before the glitch
back_gps_end = glitch_gps + 60     # one minute after the glitch
back_gps_beg_head = int(back_gps_beg/100000)
back_gps_end_head = int(back_gps_end/100000)

if time_type == "JST":  
    print("the glitch time（JST）: %s" % glitch_jst_beg)
    dataname = "pearson_img_%s_%s_[glitch %s %s].png" % (ch_x_title, ch_y_title, glitch_jst_title, time_type)
    
elif time_type == "GPS":
    print("the glitch time（GPS Time）: %s" % glitch_gps)
    dataname = "pearson_img_%s_%s_[glitch %s %s].png" % (ch_x_title, ch_y_title, glitch_gps, time_type)
    
# saving png file
dirname = "pearson_plot_image/"
os.makedirs(dirname, exist_ok=True)
filename = dirname + dataname
print("output file : %s" % filename)

# #========================================
# #========================================
i = 0
j = 0
pearson_glitch_data = []
pearson_back_data = []

# getting data
if glitch_gps_beg_head == glitch_gps_end_head:
    cache_file_glitch="/home/detchar/cache/Cache_GPS/%s.cache" % glitch_gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.cache" % glitch_gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.cache" % glitch_gps_end_head 
    cache_file_glitch="/tmp/%s_%s.cache" % (glitch_gps_beg, glitch_gps_end)

    with open(cache_file_glitch, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read()) 
data_glitch = TimeSeriesDict.read(cache_file_glitch, ch, start=glitch_gps_beg, end=glitch_gps_end, nproc=4, verbose=True)

if back_gps_beg_head == back_gps_end_head:
    cache_file_back="/home/detchar/cache/Cache_GPS/%s.cache" % back_gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.cache" % back_gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.cache" % back_gps_end_head 
    cache_file_back="/tmp/%s_%s.cache" % (back_gps_beg, back_gps_end)

    with open(cache_file_back, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  
data_back = TimeSeriesDict.read(cache_file_back, ch, start=back_gps_beg, end=back_gps_end, nproc=4, verbose=True)

# make same data size
if data_glitch[ch_x].sample_rate > data_glitch[ch_y].sample_rate:
    data_glitch[ch_y] = data_glitch[ch_y].resample(data_glitch[ch_x].sample_rate)
else:
    data_glitch[ch_x] = data_glitch[ch_x].resample(data_glitch[ch_y].sample_rate)

if data_back[ch_x].sample_rate > data_back[ch_y].sample_rate:
    data_back[ch_y] = data_back[ch_y].resample(data_back[ch_x].sample_rate)
else:
    data_back[ch_x] = data_back[ch_x].resample(data_back[ch_y].sample_rate)

# Pearson's correlation coefficient
S_xy_glitch = sum((data_glitch[ch_x] - np.mean(data_glitch[ch_x])) * (data_glitch[ch_y] - np.mean(data_glitch[ch_y])))
S_x_glitch = np.sqrt(sum((data_glitch[ch_x] - np.mean(data_glitch[ch_x])) ** 2))
S_y_glitch = np.sqrt(sum((data_glitch[ch_y] - np.mean(data_glitch[ch_y])) ** 2))
pearson_glitch = S_xy_glitch / (S_x_glitch * S_y_glitch)

for j in range(back_gps_beg, back_gps_end):
    start = j
    end = j+1
    data_back_xcrop = data_back[ch_x].crop(start, end)     # crop the data_back for each second
    data_back_ycrop = data_back[ch_y].crop(start, end)

    S_xy_back = sum((data_back_xcrop - np.mean(data_back_xcrop)) * (data_back_ycrop - np.mean(data_back_ycrop)))
    S_x_back = np.sqrt(sum((data_back_xcrop - np.mean(data_back_xcrop)) ** 2))
    S_y_back = np.sqrt(sum((data_back_ycrop - np.mean(data_back_ycrop)) ** 2))
    pearson_back = S_xy_back / (S_x_back * S_y_back)
    pearson_back_value = pearson_back.value
    pearson_back_data.append("{:.4f}".format(pearson_back_value))

# #========================================
# #========================================

print("=======================================")
print("<background_data>")
print("cache file of the back ground : %s" % cache_file_back)
print("the beginning of the back ground (GPS time) : %s" % back_gps_beg)
print("the end of the back ground (GPS time) : %s" % back_gps_end)
print("Pearson's correlation coefficient of the back ground : %s" % pearson_back_data)

print("---------------------------------------")
print("<glitch_data>")
print("cache file of the glitch : %s" % cache_file_glitch)
print("the beginning of the glitch (GPS time) : %s" % glitch_gps_beg)
print("the end of the glitch (GPS time) : %s" % glitch_gps_end)
print("Pearson's correlation coefficient of the glitch : %s" % "{:.4f}".format(pearson_glitch))
print(data_glitch)
print("=======================================")

##################################
# out put
# histogram

plt.hist(pearson_back_data, bins = 50, color = 'black')

if time_type == "JST":
    plt.axvline(x = pearson_glitch.value, linewidth=5, color='magenta', linestyle = ":", label = "glitch : %s " % glitch_jst_beg)
elif time_type == "GPS":
    plt.axvline(x = pearson_glitch.value, linewidth=5, color='magenta', linestyle = ":", label = "glitch : %s " % glitch_gps_beg)

plt.xlabel("pearson's correlation coefficient histogram")
plt.legend(loc='lower right', bbox_to_anchor=(1, 1))
plt.savefig(filename)
plt.show()