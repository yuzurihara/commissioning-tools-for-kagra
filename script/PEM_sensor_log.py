#!/usr/bin/env python
# coding: utf-8
"""
# This is one channel ASD and spectrogram plot for log scale code
Making by Mayu Murakoshi
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict 
from gwpy.plot import Plot
from matplotlib import pyplot as plt
from gwpy.time import from_gps, to_gps
import numpy as np
import sys
import os
# O3GKの期間
# between 2020/04/07 17:00:00 JST and 2020/04/21 09:00:00 JST.

# getting arguments
if __name__ == "__main__":
    print()
    print("--------------------------------------------------------------------")
    print("Arguments", sys.argv)
    print("number of Arguments:", len(sys.argv))
    print("type of Arguments", type(sys.argv))

# define
del sys.argv[0]

ch = sys.argv[0]
gps_beg = int(sys.argv[1])
gps_end = int(sys.argv[2])
fftlength = int(sys.argv[3])
color = sys.argv[4]
dirname = sys.argv[5]

if len(sys.argv) == 8:
    freq_min = float(sys.argv[6])
    freq_max = float(sys.argv[7])
    # freq_max_len = len(str(int(freq_max)))

length = gps_end - gps_beg
ch_title = ch.replace('K1:', '')
time_type = 'GPS'

# gps_beg = int(to_gps( jst_beg ))
# utc_beg = from_gps(gps_beg)
# gps_end = gps_beg + 600
gps_beg_head = int(gps_beg/100000)
gps_end_head = int(gps_end/100000)

# gps_end_plus = gps_end + 32400    # add nine hours to convert to JST
# print(gps_end_plus)
# jst_end = from_gps(gps_end_plus)
# print(jst_end)

# get the data 
i = 0  

if gps_beg_head == gps_end_head:
    cache_file = "/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head 
    cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read()) 

data = TimeSeries.read(cache_file, ch, start=gps_beg, end=gps_end, nproc=4, verbose=True)

# getting time
# if time_type == "JST":
# time_beg_y =sys.argv[1]
# time_beg_m =sys.argv[2]
# time_beg_d =sys.argv[3]
# time_beg_hms =sys.argv[4]
# time_beg_hms_title = time_beg_hms.replace(':', '')

# ###################################
# ###################################
# jst_beg = time_beg_y + "-" + time_beg_m + "-" + time_beg_d + " " + time_beg_hms + " " + time_type
# jst_beg_title = time_beg_y + "-" + time_beg_m + "-" + time_beg_d + " " + time_beg_hms_title
# dataname1 = "acc_test_log_img_%s_[%s %s].png" % (ch_title, jst_beg_title, time_type)
    
# elif time_type == "GPS":

# set directory name and file name
img_dirname = "%s/image/" % dirname
data_dirname = "%s/data/" % dirname
os.makedirs(img_dirname, exist_ok=True)
os.makedirs(data_dirname, exist_ok=True)

if 'freq_min' in locals():
    asd_title = "asd_log%s-%s_img_%s_[%s-_length%ss_FFT%ss].png" % (freq_min, freq_max, ch_title, gps_beg, length, fftlength)
    specgram_title = "specgram_log%s-%s_img_%s_[%s-_length%ss_FFT%ss].png" % (freq_min, freq_max, ch_title, gps_beg, length, fftlength)
    data_title = 'data_log%s-%s_%s_[%s-_length%ss_FFT%ss].npz' % (freq_min, freq_max, ch_title, gps_beg, length, fftlength)
else:
    asd_title = "asd_log_img_%s_[%s-_length%ss_FFT%ss].png" % (ch_title, gps_beg, length, fftlength)
    specgram_title = "specgram_log_img_%s_[%s-_length%ss_FFT%ss].png" % (ch_title, gps_beg, length, fftlength)
    data_title = 'data_log_%s_[%s-_length%ss_FFT%ss].npz' % (ch_title, gps_beg, length, fftlength)

asd_dir_file = img_dirname + asd_title
specgram_dir_file = img_dirname + specgram_title
data_dir_file = data_dirname + data_title


# out put
print("=======================================================================")

print("channel: %s " % ch)
print("start GPS: %s" % gps_beg)
print("end GPS: %s" % gps_end)
print("length: %s s" % length)

if len(sys.argv) == 8:
    print('freq_min: %s Hz' % freq_min)
    print('freq_max: %s Hz' % freq_max)
else:
    print("Range of frequencies are not defined.")
print()

print("output file of ASD: %s" % asd_dir_file)
print("output file of spectrogram: %s" % specgram_dir_file)
print("output file of data: %s" % data_dir_file)
print("cache file: %s" % cache_file)
print()
print(data)

# calculate a Spectrogram by calculating a number of ASDs
sg = data.spectrogram2(fftlength=fftlength, overlap=fftlength/2, window='hann') ** (1 / 2.)

median = sg.percentile(50) 
low = sg.percentile(10)
high = sg.percentile(90)

print(sg)
print()
print("picked up 10%, 50%, and 90% percentile")
print("10%", low.value)
print("50%", median.value)
print("90%", high.value)

###############################
###############################
# saving the data
np.savez(
    data_dir_file, 
    frequency = sg.frequencies.value,
    asd_90p = str(high.value), 
    asd_50p = str(median.value),
    asd_10p = str(low.value)
    )

# load the data
sg_data = np.load(data_dir_file)

##############################
# make plot
# ASD
plot = Plot()

if 'freq_min' in locals():
    ax = plot.add_subplot(
    xscale='log',
    yscale='log',
    xlim=(freq_min, freq_max), 
    xlabel='Frequency [Hz]',
    # ylim=(3e-24, 2e-20),
    ylabel=r'Amplitude Spectrum Density [%s]' % sg.unit
    )
else:
    ax = plot.add_subplot(
        xscale='log',
        yscale='log',
        # xlim=(freq_min, freq_max), 
        xlabel='Frequency [Hz]',
        # ylim=(3e-24, 2e-20),
        ylabel=r'Amplitude Spectrum Density [%s]' % sg.unit
    )
ax.plot_mmm(
    median, low, high,  
    color=color,
    # label='%s - %s' % (gps_beg, gps_end)
    )
ax.set_title('ASD of %s\n %s: %s - %s, length: %s s, FFT: %s s' % (ch, time_type, gps_beg, gps_end, length, fftlength),
             fontsize=14)
# ax.legend(loc='upper left', bbox_to_anchor=(0, 1), fontsize=10)

# plt.axis('square')
# plt.tight_layout()
plt.savefig(asd_dir_file)

##############################
# spectrogram
ymin = sg.df.value
ymax = max(sg.frequencies.value)

plot = sg.imshow(
    norm='log', 
    # vmin=5e-7, 
    # vmax=2e-3
)
ax = plot.gca()
if 'freq_min' in locals():
    if freq_min == 0:
        ax.set_ylim(ymin, freq_max)
    else:
        ax.set_ylim(freq_min, freq_max)
else:
    ax.set_ylim(ymin, ymax)

ax.set_yscale('log')
ax.colorbar(label=r'Amplitude Spectral Density [%s]' % sg.unit)
ax.set_title('spectrogram of %s\n %s: %s - %s, length: %s s, FFT: %s s' % (ch, time_type, gps_beg, gps_end, length, fftlength))

plt.savefig(specgram_dir_file)
plt.show()
print()
print('all done for above data')