#!/usr/bin/env python
# coding: utf-8
from gwpy.timeseries import TimeSeries
from gwpy.time import to_gps
import sys
import warnings
warnings.filterwarnings('ignore')
#warnings.filterwarnings('once')
#warnings.simplefilter('ignore')

# #=======================================
# Channel for temperature of intemediate mass for ETMX
# #=======================================
channel="K1:CRY-TEMPERATURE_EX_PAYLOAD_IM"
# #=======================================

arg = sys.argv
if len(arg) > 1:
    jst_beg = arg[1]
else:
    print("usage : ./get_temp_ETMX.py '2022/04/23 10:37:0 JST'")
    sys.exit(1)

gps_beg = int(to_gps( jst_beg ))
gps_end = gps_beg + 1
gps_beg_head = int(gps_beg/100000)
gps_end_head = int(gps_end/100000)
    
if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.cache" % gps_end_head 
    cache_file="/tmp/%s_%s.cache" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  

data = TimeSeries.read(cache_file, channel, start=gps_beg, end=gps_end, nproc=1)
print("%.1f %s" % (data[0].value, data[0].unit))
#print(data)
