#!/usr/bin/env python
# coding: utf-8
from gwpy.timeseries import TimeSeriesDict
from gwpy.time import to_gps
import sys
import warnings
warnings.filterwarnings('ignore')

# #=======================================
# Channel for temperature of intemediate mass for ETMX
# #=======================================
channels=["K1:CRY-TEMPERATURE_EX_PAYLOAD_IM",
          "K1:CRY-TEMPERATURE_EY_PAYLOAD_IM",
          "K1:CRY-TEMPERATURE_IX_PAYLOAD_IM",
          "K1:CRY-TEMPERATURE_IY_PAYLOAD_IM"]
# #=======================================

arg = sys.argv
if len(arg) > 1:
    jst_beg = arg[1]
else:
    print("usage : ./get_temp.py '2022/04/23 10:37:0 JST'")
    sys.exit(1)

print(jst_beg)

gps_beg = int(to_gps( jst_beg ))
gps_end = gps_beg + 1
gps_beg_head = int(gps_beg/100000)
    
cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head

data = TimeSeriesDict.read(cache_file, channels, start=gps_beg, end=gps_end, nproc=1)
for mirror in ["IX", "IY", "EX", "EY"]:
    ch = "K1:CRY-TEMPERATURE_%s_PAYLOAD_IM" % mirror
    print("%s  %.1f" % (mirror, data[ch].value[0]))
