#!/usr/bin/env python
# coding: utf-8
"""
This is two channels whiten scatter and timeseries plot code
Making by Mayu Murakoshi
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict
from gwpy.plot import Plot
from matplotlib import pyplot as plt
from gwpy.time import from_gps, to_gps
import numpy as np
import sys
import os
import warnings
warnings.filterwarnings('ignore')

# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

print("---------------------------------------")
del sys.argv[0]
print("channel : %s " % sys.argv[0:2])

ch_x = sys.argv[0]
ch_y = sys.argv[1]
ch = sys.argv[0:2]
time_type = sys.argv[2]

ch_x_title = ch_x.replace('K1:', '')
ch_y_title = ch_y.replace('K1:', '')

# getting time
if time_type == "JST":
    glitch_year =sys.argv[3]
    glitch_month =sys.argv[4]
    glitch_day =sys.argv[5]
    glitch_time =sys.argv[6]
    glitch_time_title = glitch_time.replace(':', '')

    glitch_jst_beg = glitch_year + "/" + glitch_month + "/" + glitch_day + " " + glitch_time + " " + time_type
    glitch_jst_title = glitch_year + "-" + glitch_month + "-" + glitch_day + " " + glitch_time_title
    glitch_gps = int(to_gps( glitch_jst_beg ))

elif time_type == "GPS":
    glitch_time =sys.argv[3]
    glitch_gps = int(glitch_time)

# conversion of time
glitch_gps_beg = glitch_gps    # assume the glitch is for one second 
glitch_gps_end = glitch_gps + 1
# glitch_utc_beg = from_gps( glitch_gps_beg )
# glitch_utc_end = from_gps( glitch_gps_end )
glitch_gps_beg_head = int(glitch_gps_beg/100000)
glitch_gps_end_head = int(glitch_gps_end/100000)

glitch_gps_beg_plus = glitch_gps_beg + 32400    # add nine hours to the glitch GPS Time to convert to JST
glitch_gps_end_plus = glitch_gps_end + 32400    # add nine hours to the glitch GPS Time to convert to JST
glitch_jst_beg_plus = from_gps( glitch_gps_beg_plus )   # convert the beginning of the glitch GPS Time to JST
glitch_jst_end_plus = from_gps( glitch_gps_end_plus )   # convert the end of the glitch GPS Time to JST
# ---------------------------------------
back_gps_beg = glitch_gps - 60     # one minute before the glitch
back_gps_end = glitch_gps + 60     # one minute after the glitch
back_utc_beg = from_gps( back_gps_beg )
back_utc_end = from_gps( back_gps_end )
back_gps_beg_head = int(back_gps_beg/100000)
back_gps_end_head = int(back_gps_end/100000)

back_gps_beg_plus = back_gps_beg + 32400     # add nine hours to the background GPS Time to convert to JST
back_gps_end_plus = back_gps_end + 32400     # add nine hours to the background GPS Time to convert to JST
back_jst_beg_plus = from_gps( back_gps_beg_plus )
back_jst_end_plus = from_gps( back_gps_end_plus )

back_jst_beg_title = str(back_jst_beg_plus) + " JST"

if time_type == "JST":  
    print("glitch_time（JST）: %s" % glitch_jst_title)
    print("background_time (UTC) : %s to %s" % (back_utc_beg, back_utc_end))
    print("background_time (JST = UTC+9h) : %s to %s" % (back_jst_beg_plus, back_jst_end_plus))
    dataname1 = "whiten_scatter_img_%s_%s_[glitch %s %s].png" % (ch_x_title, ch_y_title, glitch_jst_title, time_type)
    dataname2 = "whiten_timeseries_img_%s_%s_[glitch %s %s].png" % (ch_x_title, ch_y_title, glitch_jst_title, time_type)
    
elif time_type == "GPS":
    print("glitch_time（GPS Time）: %s" % glitch_gps)
    print("background_time (GPS Time) : %s to %s" % (back_gps_beg, back_gps_end))
    dataname1 = "whiten_scatter_img_%s_%s_[glitch %s %s].png" % (ch_x_title, ch_y_title, glitch_gps, time_type)
    dataname2 = "whiten_timeseries_img_%s_%s_[glitch %s %s].png" % (ch_x_title, ch_y_title, glitch_gps, time_type)

# saving png file
dirname = "whiten_scatter_timeseries_plot_image/"
os.makedirs(dirname, exist_ok=True)
filename1 = dirname + dataname1
filename2 = dirname + dataname2
print("output file1 : %s" % filename1)
print("output file2 : %s" % filename2)
# #========================================

# getting data 
i = 0  

if back_gps_beg_head == back_gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.cache" % back_gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.cache" % back_gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.cache" % back_gps_end_head 
    cache_file="/tmp/%s_%s.cache" % (back_gps_beg, back_gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  
data_x = TimeSeries.read(cache_file, ch_x, start=back_gps_beg, end=back_gps_end, nproc=4, verbose=True)
data_y = TimeSeries.read(cache_file, ch_y, start=back_gps_beg, end=back_gps_end, nproc=4, verbose=True)
# #========================================

# make same data size
if data_x.sample_rate > data_y.sample_rate:
    data_y = data_y.resample(data_x.sample_rate)
else:
    data_x = data_x.resample(data_y.sample_rate)
# #========================================

# crop
data_glitch_x = data_x.crop(start=glitch_gps_beg, end=glitch_gps_end)
data_glitch_y = data_y.crop(start=glitch_gps_beg, end=glitch_gps_end)

# whitening
whiten_data_x = data_x.whiten(4,2)
whiten_data_y = data_y.whiten(4,2)
# #========================================

# Pearson's correlation coefficient
S_xy = sum((data_glitch_x - np.mean(data_glitch_x)) * (data_glitch_y - np.mean(data_glitch_y)))
S_x = np.sqrt(sum((data_glitch_x - np.mean(data_glitch_x)) ** 2))
S_y = np.sqrt(sum((data_glitch_y - np.mean(data_glitch_y)) ** 2))
pearson = S_xy / (S_x * S_y)
# #========================================


##################################
## out put ##
##################################
print("=======================================")
print("cache_file = %s" % cache_file)
print("back_gps_beg = %s" % back_gps_beg)
print("back_gps_end = %s" % back_gps_end)
print("glitch_gps_beg = %s" % glitch_gps_beg)
print("glitch_gps_end = %s" % glitch_gps_end)
print("<original background data>")
print(data_x)
print(data_y)
print("<whitened background data>")
print(whiten_data_x)
print(whiten_data_y)
print("<glitch_data>")
print(data_glitch_x)
print(data_glitch_y)
print("=======================================")
print("Pearson's correlation coefficient of the glitch : %s" % "{:.4f}".format(pearson))
# #========================================

# scatter
plt.scatter(whiten_data_x, whiten_data_y, s=5, color="black", alpha=0.1)
plt.scatter(data_glitch_x, data_glitch_y, label="Pearson's correlation coefficient : %s" % "{:.4f}".format(pearson), s=5, color="magenta", alpha=0.5)
plt.xlabel("%s" % ch_x)
plt.ylabel("%s" % ch_y)

if time_type == "JST":
    # plt.scatter([], [], label="%s to %s" % (back_jst_beg_plus, back_jst_end_plus), color="black")
    # plt.scatter([], [], label="%s to %s" % (glitch_jst_beg_plus, glitch_jst_end_plus), color="magenta")
    plt.title("glitch : %s +1.0 sec" % glitch_jst_beg)
elif time_type == "GPS":
    # plt.scatter([], [], label="%s to %s" % (back_gps_beg, back_gps_end), color="black")
    # plt.scatter([], [], label="%s to %s" % (glitch_gps_beg, glitch_gps_end), color="magenta")
    plt.title("glitch : %s GPS +1.0 sec" % glitch_gps)

plt.axis('square')
plt.legend(loc='upper right', bbox_to_anchor=(1, -0.2))
plt.tight_layout()
plt.savefig(filename1)
# #========================================

# time series
fig = plt.figure() 

ax1 = fig.add_subplot(2, 1, 1)
ax1.plot(whiten_data_x.times - whiten_data_x.t0, whiten_data_x, label='%s' % ch_x, color='orange')
ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(whiten_data_y.times - whiten_data_y.t0, whiten_data_y, label='%s' % ch_y, color='blue')

ax2.set_xlabel("Time [seconds] from %s (%s)" %(back_jst_beg_title, back_gps_beg))
ax1.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax2.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax1.axvline(x=glitch_gps - whiten_data_x.t0.value, linewidth=3, color="black", linestyle = ":")
ax2.axvline(x=glitch_gps - whiten_data_y.t0.value, linewidth=3, color="black", linestyle = ":")

fig.tight_layout()
fig.savefig(filename2)
##################################

plt.show()

