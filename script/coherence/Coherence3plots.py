#!/usr/bin/env python
# coding: utf-8

"""
script to calculate and plot the psd and the coherence of channel 1 and 2. 
Making by Yun-Ying
"""

import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import argparse
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
import scipy
import matplotlib as mpl

# set the value for the plot globally
mpl.rcParams['xtick.labelsize'] = 18  # numbers of x-axis
mpl.rcParams['ytick.labelsize'] = 18  # numbers of y-axis
mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['axes.titlesize'] = 13
mpl.rcParams['legend.fontsize'] = 18
plt.rcParams['font.family'] = 'Times New Roman'
mpl.rcParams['axes.linewidth'] = 2 
myfigsize = (12, 16)
mydpisize = 500

#################################################################################################
# setting all options
plotparser = argparse.ArgumentParser(
    prog = '3PlotforCoherence', 
    description = 'Calculate and plot the psd and the coherence of ch1 and ch2'
    )

requiredNamed = plotparser.add_argument_group('required arguments')
requiredNamed.add_argument('-1','--ch1', required = True, type=str, help = '[str] Name of ch1.')
requiredNamed.add_argument('-2','--ch2', required = True, type=str, help = '[str] Name of ch2.')
requiredNamed.add_argument('-st','--starttime', required = True, type=int, help = '[int] GPS starttime of ch1 and ch2.')
optionalNamed = plotparser.add_argument_group('optional arguments')
optionalNamed.add_argument('-d','--duration', type = float, default = 60, help = '[float] Total time of ch1 and ch2. unit: seconds. Default = 60 s.')
optionalNamed.add_argument('-f','--fftlength', type = float, default = 10, help = '[float] FFT length of ch1 and ch2. unit: seconds. Default = 10 s.')
optionalNamed.add_argument('-o','--overlap', type = float, default = 0.5, help = '[float] Overlap of FFT length of ch1 and ch2. unit: percentage of FFT length. Default = 0.5')
optionalNamed.add_argument('-x','--xscale', choices = ['log', 'linear'], default = 'log', help = '[choice] Type of X scale of the 3 plots. Default: log.')
optionalNamed.add_argument('-y1','--y1scale', choices = ['log', 'linear'], default = 'log', help = '[choice] Type of Y scale of ch1. Default: log.')
optionalNamed.add_argument('-y2','--y2scale', choices = ['log', 'linear'], default = 'log', help = '[choice] Type of Y scale of ch2. Default: log.')
optionalNamed.add_argument('-c','--coherencescale', choices = ['log', 'linear'], default = 'log', help = '[choice] Type of Y scale of coherence. Default: log.')
optionalNamed.add_argument('-xmin','--xmin', type = float, help = '[float] Min of the X scale. Default: 1/totaltime for LogScale and 0 for LinearScale.')
optionalNamed.add_argument('-xmax','--xmax', type = float, help = '[float] Max of the X scale. Default: fs/2 where fs = min(fs1, fs2).')
optionalNamed.add_argument('-y1min','--y1min', type = float, help = '[float] Min of the Y scale of ch1. Default: 0.01*min value in PSD1.')
optionalNamed.add_argument('-y1max','--y1max', type = float, help = '[float] Max of the Y scale of ch1. Default: 100*max value in PSD1.')
optionalNamed.add_argument('-y2min','--y2min', type = float, help = '[float] Min of the Y scale of ch2. Default: 0.01*min value in PSD2.')
optionalNamed.add_argument('-y2max','--y2max', type = float, help = '[float] Max of the Y scale of ch2. Default: 100*max value in PSD2.')
optionalNamed.add_argument('-cmin','--coherencemin', type = float, help = '[float] Min of the Y scale of coherence. Default: 0 for linear and 0.001 for log.')
optionalNamed.add_argument('-cmax','--coherencemax', type = float, help = '[float] Min of the Y scale of coherence. Default: 1')
optionalNamed.add_argument('-fig','--figname',  type = str, help = '[str] Name of figure. Default: Coherence_ch1_ch2.png')
optionalNamed.add_argument('-csig','--coherencesignificance', action = 'store_true', help = 'Add significance, top 0.05, of conherence or not.')
args = plotparser.parse_args()

#################################################################################################
#################################################################################################
# functions required
def signaltoArray(timesignal, Ninterval, Nspacing):
    T = len(timesignal)
    Ntrial = int((T-(Ninterval-Nspacing))/Nspacing)
    signalarray = np.zeros((int(Ntrial), int(Ninterval)))
    for i in range(Ntrial):
        signalarray[i, :] = timesignal[int(Nspacing)*i: int(Nspacing)*i+int(Ninterval)]
    if Ntrial == 1:
        print('interval number = 1, only changes the dimension.')
        print('Originally, shape of signal =', timesignal.shape, 'and dim =', timesignal.ndim)
        print('Now, shape of signal =', signalarray.shape, 'and dim =', signalarray.ndim)
    return(signalarray)

# compute FFT: 
# input  (1) time-signals and (2) samplerate, or 1/dt
# return (1) freq band and (2) mean of fft of the signal, complex form.
def computeFFT(timesignal, samplerate, DSB=0):
    N, T = len(timesignal), len(timesignal[0, :])
    freqsignal = np.zeros((N, T), dtype = 'complex_')
    window = np.hanning(T)
    for i in range(N):
        freqsignal[i, :] = np.fft.fftshift(np.fft.fft(mlab.detrend_linear(timesignal[i])* window)) #NNN
    freqsignal = np.mean(freqsignal, 0)
    freqsignal = freqsignal*(2.0/samplerate/sum(window**2))**0.5 #NNN
    # time = np.arange(T)/samplerate
    freq = 0.5*samplerate/(int(T/2))*(np.arange(int(-T/2),int(T/2)+1))
    if DSB == 1: 
        # including negative frequency
        return(np.array(freq), np.array(freqsignal))
    else: 
        # only positive frequency
        subfreqsignal = np.array(freqsignal[int(len(freqsignal)/2):])
        if T%2 == 0: 
            subfreqsignal = np.append(subfreqsignal, [freqsignal[0]])
        subfreq = np.array(freq[int(len(freq)/2):int(len(freq)/2)+len(subfreqsignal)])
        # subfreqsignal[1:] = 2*subfreqsignal[1:]
        return(np.array(subfreq), np.array(subfreqsignal))

def computePSD(timesignal, samplerate, DSB=0):
    T, N = len(timesignal[0,:]), len(timesignal[:,0])
    psd = np.zeros((N,T), dtype = 'complex_')
    window = np.hanning(T)
    for n in range(N):
        ch1_fft = np.fft.fftshift(np.fft.fft(mlab.detrend_linear(timesignal[n, :])* window)) #NNN
        ch1_fft = ch1_fft*(2.0/samplerate/sum(window**2))**0.5 #NNN
        psd[n,:] = (ch1_fft*np.conjugate(ch1_fft)) #NNN
    psd = np.mean(psd, 0)
    freq = 0.5*samplerate/(int(T/2))*(np.arange(int(-T/2),int(T/2)+1))
    if DSB == 1: 
        # including negative frequency
        return(np.array(freq), np.array(psd))
    else:
        # only positive frequency
        subpsd = np.array(psd[int(len(psd)/2):])
        if T%2 == 0:
            subpsd = np.append(subpsd, [psd[0]])
        subpsd[1:len(subpsd)-1] = 2*subpsd[1:len(subpsd)-1]
        subfreq = np.array(freq[int(len(freq)/2):int(len(freq)/2)+len(subpsd)])
        return(np.array(subfreq), np.array(subpsd))

# compute Coherence (0<r<1): 
# input 2 time-signals, called signal1 and signal2, 
# return the coherence in frequency band of the 2 signals.
def computeCoherence(signal1, samplerate1, signal2, samplerate2, DSB=0):
    ## fxy, Cxy = np.array(signal.coherence(signal1, signal2, samplerate))
    ## return (fxy, Cxy)
    T1, N1 = len(signal1[0,:]), len(signal1[:,0])
    T2, N2 = len(signal2[0,:]), len(signal2[:,0])
    # time = np.arange(T)/samplerate
    freq1 = 0.5*samplerate1/(int(T1/2))*(np.arange(int(-T1/2),int(T1/2)+1))
    freq2 = 0.5*samplerate2/(int(T2/2))*(np.arange(int(-T2/2),int(T2/2)+1))
    freq = np.intersect1d(freq1, freq2)
    samplerate = 1/(freq[-1]-freq[-2])
    N, T = min(N1, N2), len(freq)
    window1 = np.hanning(T1)
    window2 = np.hanning(T2)
    ch1_fft = np.zeros((N,T1), dtype = 'complex_')
    ch2_fft = np.zeros((N,T2), dtype = 'complex_')
    for n in range(N):
        ch1_fft[n, :] = np.fft.fftshift(np.fft.fft(mlab.detrend_linear(signal1[n, :])* window1))
        ch2_fft[n, :] = np.fft.fftshift(np.fft.fft(mlab.detrend_linear(signal2[n, :])* window2))
    ch1_fft = ch1_fft*(2.0/samplerate1/sum(window1**2))**0.5 #NNN
    ch2_fft = ch2_fft*(2.0/samplerate2/sum(window2**2))**0.5 #NNN
    subfsignal1 = np.zeros((N,T), dtype = 'complex_')
    subfsignal2 = np.zeros((N,T), dtype = 'complex_')
    i, j = 0, 0
    for element in range(T1):
        if freq1[element] in freq:
            subfsignal1[:, i] = ch1_fft[:N, element]
            i += 1
    for element in range(T2):
        if freq2[element] in freq:
            subfsignal2[:, j] = ch2_fft[:N, element]
            j += 1
    ch1_fft, ch2_fft = subfsignal1, subfsignal2
    print('Coherence calculation starts!')
    Sxx = np.zeros((N,T), dtype = 'complex_')
    Syy = np.zeros((N,T), dtype = 'complex_')
    Sxy = np.zeros((N,T), dtype = 'complex_')
    # print('N = ', N, 'T = ', T)
    for n in range(N):
        # csd = np.conjugate(ch2_fft)*ch1_fft/len(signal1)/samplerate/T
        Sxx[n,:] = (ch1_fft[n,:]*np.conjugate(ch1_fft[n,:]))/samplerate/T
        Syy[n,:] = (ch2_fft[n,:]*np.conjugate(ch2_fft[n,:]))/samplerate/T
        Sxy[n,:] = (ch1_fft[n,:]*np.conjugate(ch2_fft[n,:]))/samplerate/T
    meanSxx = np.mean(Sxx,0)
    meanSyy = np.mean(Syy,0)
    meanSxy = np.mean(Sxy,0)
    np.seterr(invalid='ignore')
    if 0 in meanSxx: print('>> Note: Sxx has 0.')
    if 0 in meanSyy: print('>> Note: Syy has 0.')
    coherence = np.abs(meanSxy)**2/(meanSxx*meanSyy)
    print('>> Coherence (full frequency) of ch1 and ch2 has', len(coherence), 'points.')
    print('Coherence calculation ends!')
    if DSB == 1: 
        # including negative frequency
        return(np.array(freq), np.array(coherence))
    else:
        # only positive frequency
        subcoherence = np.array(coherence[int(len(coherence)/2):])
        if T%2 == 0:
            subcoherence = np.append(subcoherence, [coherence[0]])
        subfreq = np.array(freq[int(len(freq)/2):int(len(freq)/2)+len(subcoherence)])
        return(np.array(subfreq), np.array(subcoherence))

################################################################################################
#################################################################################################
# reading datas from options
channel1 = args.ch1
channel2 = args.ch2
starttime = args.starttime
totaltime = args.duration
timeinterval = args.fftlength
spacingT = timeinterval*(1-args.overlap)
gps_beg_head = int(starttime/100000)
gps_end_head = int((starttime+totaltime)/100000)
InputSignals = [channel1, channel2]
if args.figname == None:
    args.figname = 'Coherence_of_' + channel1 + '_and_' + channel2 + '.png'
# if '.png' not in str(args.figname):
#     args.figname = str(args.figname) + '.png'
for channels in range(2):
    if gps_beg_head == gps_end_head:
        cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    else:
        cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
        cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head 
        cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)
        with open(cache_file, 'w') as outfile:
            for i in [cache1, cache2]:
                with open(i) as infile:
                    outfile.write(infile.read())  
    InputSignals[channels] = TimeSeries.read(cache_file, InputSignals[channels], start = starttime, end = int(starttime+totaltime), nproc=4, verbose = False)

#################################################################################################
### data organization to required form for calculations
print('========== Finishing all reading signals! ==========')
ch1, ch2 = InputSignals[0], InputSignals[1]
print('Ch1 is', ch1, ',\n and Ch2 is', ch2)
print('Ch1 originally has', len(ch1),'points, and ch2 originally has', len(ch2), 'points.')
data1 = np.array(ch1)
data2 = np.array(ch2) 
dt1 = float(str(ch1.dx).split('s')[0])
dt2 = float(str(ch2.dx).split('s')[0])
fs1, fs2 = 1/dt1, 1/dt2
print('Sample frequency of ch1 =', fs1, 'Hz, and sample frequency of ch2 =', fs2, 'Hz.')
intervalpoints1, spacingpoints1 = int(timeinterval/dt1), int(spacingT/dt1)
intervalpoints2, spacingpoints2 = int(timeinterval/dt2), int(spacingT/dt2)
print('Since time interval =', timeinterval,'s,')
print('in each interval, ch1 has', intervalpoints1,'points, and ch2 has', intervalpoints2, 'points.')
Adata1 = signaltoArray(data1, intervalpoints1, spacingpoints1)
Adata2 = signaltoArray(data2, intervalpoints2, spacingpoints2)
print('dataArray1 has', len(Adata1), 'intervals, with', len(Adata1[0]), 'elements in each interval.')
print('dataArray2 has', len(Adata2), 'intervals, with', len(Adata2[0]), 'elements in each interval.')

if len(Adata1) != len(Adata2):
    print('ERROR: The section number between two channels are not the same!')
    sys.exit(1)
############################################################################
# psd and coherence calculations
print('========== Analyzing the psd of signals! ==========')
[freq1, ch1PSD] = computePSD(Adata1, 1/dt1)
[freq2, ch2PSD] = computePSD(Adata2, 1/dt2)
[freq12, coherence12] = computeCoherence(Adata1, 1/dt1, Adata2, 1/dt2)

significantcohere = scipy.stats.f.ppf(0.95, 2, 2*len(Adata1))
significantcohere = significantcohere/(len(Adata1) - 1 + significantcohere)

############################################################################
############################################################################
# plot settings
# xlabelsize, ylabelsize, xticksize, yticksize, titlesize = 70, 70, 66, 66, 66
# desiredlinewidth = 8
# gridlinewidth = 2
# xlim settings
pxmin, pxmax = args.xmin, args.xmax
if pxmin == None: 
    if args.xscale == 'linear': pxmin = 0
    elif args.xscale == 'log': pxmin = 1/totaltime
if pxmax == None:
    pxmax = min(fs1,fs2)/2

############################################################################
# plots begins
# plot-1
fig = plt.figure(figsize = myfigsize, dpi = mydpisize)
plt.subplot(3, 1, 1)
plt.plot(freq1, abs(ch1PSD))
# plt.title("PSD of $x_1(t)$ :"+str(ch1.channel), fontsize = titlesize)
# plt.xlabel("Frequency (Hz)", fontsize = xlabelsize)
plt.ylabel("Power Spectrum Density ("+str(ch1.unit)+"$^{2}/$Hz)")
plt.xlim([float(pxmin), float(pxmax)])
# pmin, pmax = plt.gca().get_ylim() 
# print(float(pmin), min(abs(ch1PSD))) # -4.029736016683964 9.38448603150378e-15
pmin, pmax = 0.01*min(abs(ch1PSD)), 100*max(abs(ch1PSD))
if args.y1min != None: pmin = args.y1min
if args.y1max != None: pmax = args.y1max
plt.ylim([float(pmin), float(pmax)]) 
plt.xscale(args.xscale); plt.yscale(args.y1scale)
plt.legend([str(ch1.channel)])
plt.grid(True)
# plot-2
plt.subplot(3, 1, 2)
plt.plot(freq2, abs(ch2PSD))
# plt.title("PSD of $x_2(t)$ :"+str(ch2.channel), fontsize = titlesize)
# plt.xlabel("Frequency (Hz)", fontsize = xlabelsize)
plt.ylabel("Power Spectrum Density (" + str(ch2.unit) + "$^{2}/$Hz)")
plt.xlim([pxmin, pxmax])
pmin, pmax = 0.01*min(abs(ch2PSD)), 100*max(abs(ch2PSD))
if args.y2min != None: pmin = args.y2min
if args.y2max != None: pmax = args.y2max
plt.ylim([float(pmin), float(pmax)])
plt.xscale(args.xscale); plt.yscale(args.y2scale)
plt.legend([str(ch2.channel)])
plt.grid(True)
# plot-3
plt.subplot(3, 1, 3)
plt.plot(freq12, abs(coherence12))
if args.coherencesignificance == True: 
    plt.plot(freq12, np.ones(len(freq12))*significantcohere, 'r--')
plt.title("Coherence of "+str(ch1.channel)+" and "+str(ch2.channel))
plt.xlabel("Frequency (Hz)")
plt.ylabel("Coherence")
plt.xlim([pxmin, pxmax])
pmin, pmax = 0, 1
if args.coherencescale == 'log': pmin = 0.001
if args.coherencemin != None: pmin = args.coherencemin
if args.coherencemax != None: pmax = args.coherencemax
plt.ylim([float(pmin), float(pmax)])
plt.xscale(args.xscale); plt.yscale(args.coherencescale)
plt.grid(True)
# plt.show()
# modify textsize and file name
fig.savefig(args.figname)
print('========== Figure saved! No errors! ==========')
