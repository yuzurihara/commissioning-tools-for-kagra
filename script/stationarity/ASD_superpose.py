#!/usr/bin/env python
# coding: utf-8
"""
This code is making a superposing plot of two ASD plots.
Made by Mayu Murakoshi (Aoyama Gakuin Uni).
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
from gwpy.frequencyseries import FrequencySeries
from gwpy.signal import filter_design
from gwpy.astro import inspiral_range
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
import scipy.stats
import math
import sys
import os
import warnings
# warnings.filterwarnings('ignore')
import matplotlib as mpl
import glob

## getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
ch = sys.argv[0]
time_type = sys.argv[1]
print('channel:', ch)
print()

## stationarity time = 2023/5/28 8:00-16:00 UTC (17:00-25:00 JST) 8hrs
ch_list = ["K1:PEM-MIC_PSL_TABLE_PSL1_Z_OUT_DQ",
           "K1:PEM-MIC_PSL_TABLE_PSL2_Z_OUT_DQ",
           "K1:PEM-MIC_PSL_TABLE_PSL3_Z_OUT_DQ",
           "K1:PEM-ACC_PSL_PERI_PSL2_X_OUT_DQ",
           "K1:PEM-ACC_PSL_TABLE_PSL1_Y_OUT_DQ",
           "K1:PEM-ACC_PSL_TABLE_PSL2_X_OUT_DQ",
           "K1:PEM-ACC_PSL_TABLE_PSL3_Z_OUT_DQ"]
sta_utc_beg = "2023/5/28 8:00:00 UTC"
sta_gps_beg = int(to_gps(sta_utc_beg))
sta_gps_end = sta_gps_beg + 28800
sta_utc_end = from_gps(sta_gps_end)
sta_gps_beg_head = int(sta_gps_beg/100000)
sta_gps_end_head = int(sta_gps_end/100000)

i = 0
for j in ch_list:
    if ch == j:
        break
    else:
        i = i+1
if i >= 7:
    print('undefined channel')
    print('stopped')
    sys.exit()

## getting time
time_type == "GPS"
event_gps = int(sys.argv[2])
len_before = int(sys.argv[3])
len_after = int(sys.argv[4])
fftl = sys.argv[5]     # fftlength

gps_beg = event_gps - len_before
gps_end = event_gps + len_after
gps_beg_head = int(gps_beg/100000)
gps_end_head = int(gps_end/100000)
span = len_before + len_after
utc_event = from_gps(event_gps)
utc_beg = from_gps(gps_beg)
utc_end = from_gps(gps_end)
print('-----------------------------------------------')
print('[event time]')
print('event GPS/UTC             :', event_gps, '/', utc_event)
print('start GPS/UTC (event time):', gps_beg, '/', utc_beg)
print('end GPS/UTC (event time)  :', gps_end, '/', utc_end)
print('(from %ss before the event to %ss after the event)' % (len_before, len_after))
# print('span: %s s' % span)
# print('fftlength: %s s' % fftl)
print()

## getting data
# event time
if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head 
    cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)
    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read()) 

print('cache file of event time: ', cache_file)
data = TimeSeries.read(cache_file, ch, start=gps_beg, end=gps_end, nproc=4, verbose=True)
sg = data.spectrogram2(fftlength=fftl, overlap=int(fftl)/2, window='hann') ** (1 / 2.)
print(data)
print()
print(sg)

# stationary time
print()
print('-----------------------------------------------')
print('[stationary time]')
print('start GPS/UTC (stationary time):', sta_gps_beg, '/', sta_utc_beg)
print('end GPS/UTC (stationary time)  :', sta_gps_end, '/', sta_utc_end)
print()

if sta_gps_beg_head == sta_gps_end_head:
    sta_cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % sta_gps_beg_head
else:
    sta_cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % sta_gps_beg_head
    sta_cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % sta_gps_end_head 
    sta_cache_file="/tmp/%s_%s.ffl" % (sta_gps_beg, sta_gps_end)
    with open(sta_cache_file, 'w') as sta_outfile:
        for i in [sta_cache1, sta_cache2]:
            with open(i) as sta_infile:
                sta_outfile.write(sta_infile.read()) 

print('cache file of stationary time: ', sta_cache_file)
sta_data = TimeSeries.read(sta_cache_file, ch, start=sta_gps_beg, end=sta_gps_end, nproc=4, verbose=True)
sta_sg = sta_data.spectrogram2(fftlength=fftl, overlap=int(fftl)/2, window='hann') ** (1 / 2.)
print(sta_data)
print()
print(sta_sg)
print()

## calcurate ASD percentile
# event time
median = sg.percentile(50)
low = sg.percentile(5)
high = sg.percentile(95)
# stationary time
sta_median = sta_sg.percentile(50)
sta_low = sta_sg.percentile(5)
sta_high = sta_sg.percentile(95)

print('-----------------------------------------------')
print("picked up 5%, 50%, and 95% percentile of ASD")
print('[event time]')
print(' 5% :', low)
print('50% :', median)
print('95% :', high)
print('[stationary time]')
print(' 5% :', sta_low)
print('50% :', sta_median)
print('95% :', sta_high)
print()

## normalised spectrogram at event time by 50% percentile at stationary time
normalised = sg.ratio(sta_median)
print('normalised spectrogram at event time by 50% percentile at stationary time :')
print(normalised)
print()

## prepare saving png file
dirname = "ASD_superpose_plot[%s]-%s/" % (ch[3:], event_gps)
pngname1 = "fftl=%s[event=%s_span%ss]_asd_superpose_log.png" %(fftl, event_gps, span)
pngname2 = "fftl=%s[event=%s_span%ss]_asd_superpose_lin1.png" %(fftl, event_gps, span)
pngname3 = "fftl=%s[event=%s_span%ss]_asd_superpose_lin2.png" %(fftl, event_gps, span)
pngname4 = "fftl=%s[event=%s_span%ss]_stg_superpose_log.png" %(fftl, event_gps, span)
pngname5 = "fftl=%s[event=%s_span%ss]_stg_superpose_lin.png" %(fftl, event_gps, span)
pngname6 = "fftl=%s[event=%s_span%ss]_normstg_superpose_log.png" %(fftl, event_gps, span)
pngname7 = "fftl=%s[event=%s_span%ss]_normstg_superpose_lin.png" %(fftl, event_gps, span)
os.makedirs(dirname, exist_ok=True)
filename1 = dirname + pngname1
filename2 = dirname + pngname2
filename3 = dirname + pngname3
filename4 = dirname + pngname4
filename5 = dirname + pngname5
filename6 = dirname + pngname6
filename7 = dirname + pngname7

## getting plot arguments
print('[plot arguments]')
color = sys.argv[6]
freq_min = float(sys.argv[7])
freq_max = float(sys.argv[8])
freq_mid = float(sys.argv[9])

if len(sys.argv) == 14:
    asd_min = float(sys.argv[10])
    asd_max = float(sys.argv[11])
    rela_vmin = float(sys.argv[12])
    rela_vmax = float(sys.argv[13])
    print('min of the ASD scale :', asd_min)
    print('max of the ASD scale :', asd_max)
    print('min of the relative amplitude scale :', rela_vmin)
    print('max of the relative amplitude scale :', rela_vmax)
elif len(sys.argv) == 13:
    if sys.argv[12] == 'asdrange':
        asd_min = float(sys.argv[10])
        asd_max = float(sys.argv[11])
        print('min of the ASD scale :', asd_min)
        print('max of the ASD scale :', asd_max)
        print('min of the relative amplitude scale : not set')
        print('max of the relative amplitude scale : not set')
    elif sys.argv[12] == 'relavrange':
        rela_vmin = float(sys.argv[10])
        rela_vmax = float(sys.argv[11])
        print('min of the ASD scale : not set')
        print('max of the ASD scale : not set')
        print('min of the relative amplitude scale :', rela_vmin)
        print('max of the relative amplitude scale :', rela_vmax)
    else:
        print()
        print('Arguments error')
        sys.exit()
else:
    print("not set both ASD range and relative amplitude range")

if freq_min == 0:
    freq_min_log = sg.df.value
else:
    freq_min_log = freq_min

# =======================================================================================
## ASD log plot
plot = Plot()
if len(sys.argv) >= 12 and 'asd_min' in locals():
    ax = plot.add_subplot(
        xscale='log', 
        xlim=(freq_min_log, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylim=(asd_min, asd_max),
        ylabel='ASD [%s]'% median.unit,
    )
else:
    ax = plot.add_subplot(
        xscale='log', 
        xlim=(freq_min_log, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylabel='ASD [%s]'% median.unit,
    )    
ax.plot_mmm(sta_median, sta_low, sta_high, color=color, label='stationary time (for 8hrs from %s)' % sta_utc_beg)
ax.plot(median, color='black', alpha=0.7, label='event time (for %ss around %s UTC)' % (span, utc_event))
ax.set_title('%s \n span: %s s, fft length: %s s' %(ch, span, fftl),fontsize=16)
plt.legend()
plt.savefig(filename1)

## ASD linear plot 1
plot = Plot()
if len(sys.argv) >= 12 and 'asd_min' in locals():
    ax = plot.add_subplot(
        xscale='linear',
        xlim=(freq_min, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylim=(asd_min, asd_max),
        ylabel='ASD [%s]'% median.unit,
    )
else:
    ax = plot.add_subplot(
        xscale='linear',
        xlim=(freq_min, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylabel='ASD [%s]'% median.unit,
    )    
ax.plot_mmm(sta_median, sta_low, sta_high, color=color, label='stationary time (for 8hrs from %s)' % sta_utc_beg)
ax.plot(median, color='black', alpha=0.7, label='event time (for %ss around %s UTC)' % (span, utc_event))
ax.set_title('%s \n span: %s s, fft length: %s s' %(ch, span, fftl),fontsize=16)
plt.legend()
plt.savefig(filename2)

## ASD linear plot 2
plot = Plot()
plot = plt.figure(figsize=(15, 6))
if len(sys.argv) >= 12 and 'asd_min' in locals():
    ax1 = plot.add_subplot(211,
        xscale='linear',
        xlim=(freq_min, freq_mid),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylim=(asd_min, asd_max),
        ylabel='ASD [%s]'% median.unit,
    )
    ax2 = plot.add_subplot(212,
    xscale='linear',
    xlim=(freq_min, freq_max),
    xlabel='Frequency [Hz]',
    yscale='log',
    ylim=(asd_min, asd_max),
    ylabel='ASD [%s]'% median.unit,
    )
else:
    ax1 = plot.add_subplot(211,
        xscale='linear',
        xlim=(freq_min, freq_mid),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylabel='ASD [%s]'% median.unit,
    )
    ax2 = plot.add_subplot(212,
    xscale='linear',
    xlim=(freq_min, freq_max),
    xlabel='Frequency [Hz]',
    yscale='log',
    ylabel='ASD [%s]'% median.unit,
    )
ax1.plot_mmm(sta_median, sta_low, sta_high, color=color, label='stationary time (for 8hrs from %s)' % sta_utc_beg)
ax1.plot(median, color='black', alpha=0.7, label='event time (for %ss around %s UTC)' % (span, utc_event))
ax1.set_title('%s \n span: %s s, fft length: %s s' %(ch, span, fftl),fontsize=16)

ax2.plot_mmm(sta_median, sta_low, sta_high, color=color, label='stationary time (for 8hrs from %s)' % sta_utc_beg)
ax2.plot(median, color='black', alpha=0.7, label='event time (for %ss around %s UTC)' % (span, utc_event))
# ax2.set_title('%s \n [%s-%s]' %(ch, gps_beg, gps_end),fontsize=16)
plt.legend()
plt.tight_layout()
plt.savefig(filename3)

## spectrogram log plot
if len(sys.argv) >= 12 and 'asd_min' in locals():
    plot = sg.imshow(
        norm='log',
        vmin = asd_min,
        vmax = asd_max,
        # cmap='Spectral_r'
    )
else:
    plot = sg.imshow(
        norm='log',
        # cmap='Spectral_r'
        # vmin=5e-7,
        # vmax=2e-3
    )
ax = plot.gca()
ax.set_yscale('log')
ax.set_ylim(freq_min_log, freq_max)
ax.colorbar(label=r'Amplitude Spectral Density [%s]' % sg.unit)
ax.set_title('%s \n [event=%s(=t0+%ss)] span: %s s, FFT: %s s' %(ch, event_gps, len_before, span, fftl),fontsize=16)
plt.savefig(filename4)

## spectrogram linear plot
if len(sys.argv) >= 12 and 'asd_min' in locals():
    plot = sg.imshow(
        norm='log',
        vmin = asd_min,
        vmax = asd_max,
        # cmap='Spectral_r'
    )
else:
    plot = sg.imshow(
        norm='log',
        # cmap='Spectral_r'
        # vmin=5e-7,
        # vmax=2e-3
    )
ax = plot.gca()
ax.set_yscale('linear')
ax.set_ylim(freq_min, freq_max)
ax.colorbar(label=r'Amplitude Spectral Density [%s]' % sg.unit)
ax.set_title('%s \n [event=%s(=t0+%ss)] span: %s s, FFT: %s s' %(ch, event_gps, len_before, span, fftl),fontsize=16)
plt.savefig(filename5)

## normalised spectrogram log plot
if len(sys.argv) >= 12 and 'rela_vmin' in locals():
    plot = normalised.imshow(
        norm='log',
        vmin = rela_vmin,
        vmax = rela_vmax,
        cmap='Spectral_r'
    )
else:
    plot = normalised.imshow(
        norm='log',
        cmap='Spectral_r'
    )
ax = plot.gca()
ax.set_yscale('log')
ax.set_ylim(freq_min_log, freq_max)
ax.colorbar(label=r'Relative amplitude')
ax.set_title('%s \n Normalised[event=%s(=t0+%ss)] span: %s s, FFT: %s s' %(ch, event_gps, len_before, span, fftl),fontsize=16)
# ax.set_title('%s \n Normalised[event=%s(=t0+%ss)] span: %s s, FFT: %s s vmin:%s, vmax:%s' %(ch, event_gps, len_before, span, fftl, rela_vmin, rela_vmax),fontsize=16)
plt.savefig(filename6)

## normalised spectrogram linear plot
if len(sys.argv) >= 12 and 'rela_vmin' in locals():
    plot = normalised.imshow(
        norm='log',
        vmin = rela_vmin,
        vmax = rela_vmax,
        cmap='Spectral_r'
    )
else:
    plot = normalised.imshow(
        norm='log',
        cmap='Spectral_r'
    )
ax = plot.gca()
ax.set_yscale('linear')
ax.set_ylim(freq_min, freq_max)
ax.colorbar(label=r'Relative amplitude')
ax.set_title('%s \n Normalised[event=%s(=t0+%ss)] span: %s s, FFT: %s s' %(ch, event_gps, len_before, span, fftl),fontsize=16)
# ax.set_title('%s \n Normalised[event=%s(=t0+%ss)] span: %s s, FFT: %s s vmin:%s, vmax:%s' %(ch, event_gps, len_before, span, fftl, rela_vmin, rela_vmax),fontsize=16)
plt.savefig(filename7)
# =======================================================================================

## saving data file
savename = "fftl=%s[event=%s_span%ss]_asd_superpose" %(fftl, event_gps, span)
np.savez(dirname+savename, 
         frequency=median.frequencies, 
         asd_5p=low.value, 
         asd_50p=median.value, 
         asd_95p=high.value,
         sta_asd_5p=sta_low.value, 
         sta_asd_50p=sta_median.value, 
         sta_asd_95p=sta_high.value,
        )

## making Tex file
def write_tex_img(img_path, size=120):
    img_script = '\\begin{figure}[htbp]' '\n'\
                    '\centering' '\n'\
                    '\includegraphics[width=' + str(size) +'mm]{' + img_path + '}' '\n'\
                    '\end{figure}' '\n\n'
    return img_script

def write_tex_header(time):
    header_script = '\n' '\clearpage' '\n'\
                '\section{' + time + '}' '\n\n'
    return header_script

def write_tex_main(tex_list):
    input_script = ''
    for file in tex_list:
        input_script += '\input{' + file + '}\n'

    main_script = '\documentclass{article}' '\n'\
                    '\\usepackage[margin=20mm]{geometry} \n'\
                    '\\usepackage{graphicx} \n'\
                    '\\begin{document}' '\n'\
                    + input_script \
                    + '\end{document}'
    return main_script

dirname_TeX = "ASD_superpose_TeX_%s_-%s/" % (ch[3:], event_gps)
os.makedirs(dirname_TeX, exist_ok=True)

# pdf script for log
with open(dirname_TeX + 'TeX_ASD_superpose_plot_%s-%s_' % (ch[3:], span) + savename + '_log.tex', 'w') as f:
    files = [filename1, filename4, filename6]
    header_script = write_tex_header("fftl=%s[event=%s span%ss]" %(fftl, event_gps, span) + '(' + str(utc_event) + 'UTC )')
    f.write(header_script)    # making header
    for file in files:
        img_script = write_tex_img(file, size=100)
        f.write(img_script)

tex_list = sorted(glob.glob(dirname_TeX + '*_log.tex'))
if 'ASD_superpose_plot_%s-%s' % (ch[3:], span) + '_log.tex' in tex_list:
    tex_list.remove('ASD_superpose_plot_%s-%s' % (ch[3:], span) + '_log.tex')
main_script = write_tex_main(tex_list)
with open('./ASD_superpose_plot_%s-%s' % (ch[3:], span) + '_log.tex', 'w') as f_main:
    f_main.write(main_script)

# pdf script for lin
with open(dirname_TeX + 'TeX_ASD_superpose_plot_%s-%s_' % (ch[3:], span) + savename + '_lin.tex', 'w') as f:
    files = [filename2, filename3, filename5, filename7]
    header_script = write_tex_header("fftl=%s[event=%s span%ss]" %(fftl, event_gps, span) + '(' + str(utc_event) + 'UTC )')
    f.write(header_script)    # making header
    for file in files:
        img_script = write_tex_img(file, size=100)
        f.write(img_script)

tex_list = sorted(glob.glob(dirname_TeX + '*_lin.tex'))
if 'ASD_superpose_plot_%s-%s' % (ch[3:], span) + '_lin.tex' in tex_list:
    tex_list.remove('ASD_superpose_plot_%s-%s' % (ch[3:], span) + '_lin.tex')
main_script = write_tex_main(tex_list)
with open('./ASD_superpose_plot_%s-%s' % (ch[3:], span) + '_lin.tex', 'w') as f_main:
    f_main.write(main_script)

print()
print()
print("all done")
# print("If you want to make PDF file, run 'lualatex TEXFILENAME' in appropriate directory.")
plt.show()