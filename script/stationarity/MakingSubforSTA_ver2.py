#!/usr/bin/env python
# coding: utf-8
"""
# This is making submit file code 
Making by Yun-Ying and Mayu Murakoshi
"""

from gwpy.time import from_gps, to_gps
import numpy as np
import argparse

# input parameter =======================================================================
plotparser = argparse.ArgumentParser(
    prog = 'MakingSubforSTA_ver2', 
    description = 'The file making .sub file for sta_ASD.py, sta_log.py, sta_lin1.py, sta_lin2.py, and ASD_superpose.py.',
    epilog = 'Command example : python MakingSubforSTA_ver2.py -ch K1:PEM-ACC_PSL_PERI_PSL2_X_OUT_DQ -eventjstd 2020/4/12 -eventjsthms 9:00:00 -type JST -py ASD_superpose -lenbefore 30 -lenafter 30 -fftl 60 -col r -freqmin 0 -freqmax 1000'
    )

requiredNamed = plotparser.add_argument_group('required arguments')
requiredNamed.add_argument('-ch','--channel', required = True, type=str, help = '[str] Name of the channel.')

prerequiredNamed_Superpose = plotparser.add_argument_group('pre-required arguments (only for ASD_superpose.py)')
prerequiredNamed_Superpose.add_argument('-eventjstd','--eventjsttimeday', type=str, help = '[str] JST day that GW event candidates detected e.g., 2020/4/12')
prerequiredNamed_Superpose.add_argument('-eventjsthms','--eventjsttimehms', type=str, help = '[str] JST hours, minutes, and seconds that GW event candidates detected e.g., 9:00:00')
prerequiredNamed_Superpose.add_argument('-eventgps','--eventgpstime', type=int, help = '[int] GPS time that GW event candidates detected')

prerequiredNamed_TimeType = plotparser.add_argument_group('pre-required arguments (Select time type from GPS time or JST excepting for ASD_superpose.py.)')
prerequiredNamed_TimeType.add_argument('-stjstd','--startjsttimeday', type=str, help = '[str] start JST day of ch. e.g., 2020/4/12')
prerequiredNamed_TimeType.add_argument('-stjsthms','--startjsttimehms', type=str, help = '[str] start JST hours, minutes, and seconds of ch. e.g., 9:00:00')
prerequiredNamed_TimeType.add_argument('-stgps','--startgpstime', type=int, help = '[int] start GPS time of ch.')

optionalNamed = plotparser.add_argument_group('optional arguments')
optionalNamed.add_argument('-type','--timetype', choices = ['JST', 'GPS'], help = '[choice] Which type of start time.')
optionalNamed.add_argument('-py','--pytorun', choices = ['sta_ASD', 'sta_log', 'sta_lin1', 'sta_lin2', 'ASD_superpose'], default = 'sta_ASD', help = '[choice] Which .py to run. Default: sta_ASD')
optionalNamed.add_argument('-d','--duration', type = int, default = 60, help = '[int] Total time of ch excepting for ASD_superpose.py. unit: seconds. Default = 60 s.')
optionalNamed.add_argument('-s','--span', type = int, default = 60, help = '[int] Time used to calculating one figure excepting for ASD_superpose.py. unit: seconds. Default = 60 s.')
optionalNamed.add_argument('-lenbefore','--lengthbeforeevent', type=int, default = 30, help = '[int] time length before GW event candidates only for ASD_superpose.py. unit: seconds. Default = 30 s.')
optionalNamed.add_argument('-lenafter','--lengthafterevent', type=int, default = 30, help = '[int] time length after GW event candidates only for ASD_superpose.py. unit: seconds. Default = 30 s.')
optionalNamed.add_argument('-fftl','--fftlength', type = int, default = 10, help = '[int] FFT length of ch. unit: seconds. Default = 10 s.')
optionalNamed.add_argument('-col','--color', type=str, default = 'r', help = '[str] The color of the ASD figures. Default: red.')
optionalNamed.add_argument('-freqmin','--freqmin', type = float, default = 0, help = '[float] Min of the frequency scale. Default: 0.')
optionalNamed.add_argument('-freqmax','--freqmax', type = float, default = 1024, help = '[float] Max of the frequency scale. with maximum 1024. Default: 1024.')
optionalNamed.add_argument('-freqmid','--freqmid', type = float, default = 100, help = '[float] Max of the frequency scale of one of the ASD linear plot 2. Need only when pytorun is "sta_ASD" or "sta_lin2". Default: 100.')
optionalNamed.add_argument('-asdmin','--asdmin', type = float, help = '[float] Min of the ASD scale.')
optionalNamed.add_argument('-asdmax','--asdmax', type = float, help = '[float] Max of the ASD scale.')
optionalNamed.add_argument('-relavmin','--relativevmin', type = float, help = '[float] Min of the relative amplitude scale only for ASD_superpose.py.')
optionalNamed.add_argument('-relavmax','--relativevmax', type = float, help = '[float] Max of the relative amplitude scale only for ASD_superpose.py.')
args = plotparser.parse_args()

ch = args.channel

event_day = str(args.eventjsttimeday)
event_hms = str(args.eventjsttimehms)
event_gps = args.eventgpstime
len_before = args.lengthbeforeevent
len_after = args.lengthafterevent

time_type = str(args.timetype)
time_beg_day = str(args.startjsttimeday)
time_beg_hms = str(args.startjsttimehms)
gps_beg = args.startgpstime

span = args.span
duration = args.duration
fftl = args.fftlength
color = args.color

asd_min = args.asdmin
asd_max = args.asdmax
rela_vmin = args.relativevmin
rela_vmax = args.relativevmax
pythonfile = args.pytorun
freq_min = args.freqmin
freq_max = args.freqmax
freq_mid = args.freqmid
# ======================================================================================

# convert gps time
# only for ASD_superpose.py
if pythonfile == "ASD_superpose":
    if time_type == "JST":
        event_jst = event_day + " " + event_hms + " " + time_type
        event_gps = int(to_gps( event_jst ))
# excepting for ASD_superpose.py
else:
    if time_type == "JST":
        jst_beg = time_beg_day + " " + time_beg_hms + " " + time_type
        gps_beg = int(to_gps( jst_beg ))
    gps_end = gps_beg + span

# output input parame
print("this submit file for...", pythonfile)
print("channel  :", ch)
if pythonfile == "ASD_superpose":
    if time_type == "JST":
        print("event_jst  :", event_jst)
    print("event_gps :", event_gps)
    print("length before the event : %s s" % len_before)
    print("length after the event : %s s" % len_after)
else:
    if time_type == "JST":
        print("jst_beg  :", jst_beg)
    print("gps_beg  :", gps_beg)
    print("span     : %s s" % span)
    print("duration : %s s" % duration)

# making submit file
f = open('%s.sub' % pythonfile, 'w')

datalist = [
    'Universe = Vanilla\n', 
    'request_memory = 10 GB\n',
    '\n',
    'Executable = %s.py\n' % pythonfile,
    'Log = %s.log\n' % pythonfile,
    'Output = %s.out\n'% pythonfile,
    'Error = %s.err\n'% pythonfile,
    'Getenv = True\n',
    ]
f.writelines(datalist)

N = duration / span
time_type = "GPS"

# only for ASD_superpose.py
if pythonfile == "ASD_superpose":
    if type(asd_max) == float and type(rela_vmax) == float:
        datalist = [
        '\n',
        'Arguments = %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, event_gps, len_before, len_after, fftl, color, freq_min, freq_max, freq_mid, asd_min, asd_max, rela_vmin, rela_vmax),
        'Queue'
        ]
    elif type(asd_max) == float and type(rela_vmax) != float:
        datalist = [
        '\n',
        'Arguments = %s %s %s %s %s %s %s %s %s %s %s %s asdrange\n' %(ch, time_type, event_gps, len_before, len_after, fftl, color, freq_min, freq_max, freq_mid, asd_min, asd_max),
        'Queue'
        ]
    elif type(asd_max) != float and type(rela_vmax) == float:
        datalist = [
        '\n',
        'Arguments = %s %s %s %s %s %s %s %s %s %s %s %s relavrange\n' %(ch, time_type, event_gps, len_before, len_after, fftl, color, freq_min, freq_max, freq_mid, rela_vmin, rela_vmax),
        'Queue'
        ]
    else:
        datalist = [
        '\n',
        'Arguments = %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, event_gps, len_before, len_after, fftl, color, freq_min, freq_max, freq_mid),
        'Queue'
        ]
    f.writelines(datalist)
# excepting for ASD_superpose.py
else:
    for n in range(int(N)):
        # print(n)
        if pythonfile == "sta_log":
            if type(asd_max) == float:
                datalist = [
                    '\n',
                    'Arguments = %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, asd_min, asd_max),
                    'Queue'
                    ]
            else:
                datalist = [
                    '\n',
                    'Arguments = %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max),
                    'Queue'
                    ]
            f.writelines(datalist)
        
        elif pythonfile == "sta_lin1":
            if type(asd_max) == float:
                datalist = [
                    '\n',
                    'Arguments = %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, asd_min, asd_max),
                    'Queue'
                    ]
            else:
                datalist = [
                    '\n',
                    'Arguments = %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max),
                    'Queue'
                    ]
            f.writelines(datalist)
        
        elif pythonfile == "sta_lin2":
            if type(asd_max) == float:
                datalist = [
                    '\n',
                    'Arguments = %s %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid, asd_min, asd_max),
                    'Queue'
                    ]
            else:
                datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid),
                'Queue'
                ]
            f.writelines(datalist)
        
        elif pythonfile == "sta_ASD":
            if type(asd_max) == float:
                datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid, asd_min, asd_max),
                'Queue'
                ]
            else:
                datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid),
                'Queue'
                ]
            f.writelines(datalist)
        
        gps_beg = gps_end
        gps_end = gps_beg + span

f.close()

print()
print("run 'condor_submit %s.sub'" % pythonfile)