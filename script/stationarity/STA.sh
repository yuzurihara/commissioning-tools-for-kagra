#!/bin/bash
# usage: MakingSubforSTA [-h] -ch CH [-stjstd STARTJSTTIMEDAY] [-stjsthms STARTJSTTIMEHMS] [-stgps STARTGPSTIME] [-type {JST,GPS}]
#                        [-py {sta_ASD,sta_log,sta_lin1,sta_lin2}] [-d DURATION] [-s SPAN] [-fftl FFTLENGTH] [-col COLOR] [-freqmin FREQMIN] [-freqmax FREQMAX]
#                        [-freqmid FREQMID] [-asdmin ASDMIN] [-asdmax ASDMAX]

python MakingSubforSTA.py -ch K1:VIS-IMMT2_TM_OPLEV_TILT_SUM_OUT_DQ -stjstd 2023/1/1 -stjsthms 0:00:00 -type JST -py sta_ASD -d 86400 -s 86400 -freqmax 200
echo "making submit-file for STA"
condor_submit sta_ASD.sub
echo "submit job"

condor_q