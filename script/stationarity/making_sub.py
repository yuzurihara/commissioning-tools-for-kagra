#!/usr/bin/env python
# coding: utf-8
from gwpy.time import from_gps, to_gps
import numpy as np

# locked during O3GK (red): 2020/4/12 9:00-17:00 JST
# unlocked during O3GK (blue): 2020/4/13 9:00-17:00 JST
# recently (green): 2022/8/21 20:00-8/22 4:00 JST

# =======================================================================================
# input parameter =======================================================================
# ch = "K1:PEM-ACC_EXA_TABLE_TX_Z_OUT_DQ"
ch = "K1:PEM-ACC_EXA_TABLE_RX_Z_OUT_DQ"
# ch = "K1:PEM-ACC_EYA_TABLE_TX_Z_OUT_DQ"
# ch = "K1:PEM-ACC_EYA_TABLE_RX_Z_OUT_DQ"
# ch = "K1:PEM-MIC_EXA_BOOTH_EXA_Z_OUT_DQ"
# ch = "K1:PEM-MIC_EYA_BOOTH_EYA_Z_OUT_DQ"
## ch = "K1:PEM-MIC_TMSX_TABLE_TMS_Z_OUT_DQ"
## ch = "K1:PEM-MIC_TMSY_TABLE_TMS_Z_OUT_DQ"

# Select 1 time type from GPS or JST.
time_type = "GPS"
gps_beg = 1334480780

# time_type = "JST"
# time_beg_y = "2020"
# time_beg_m = "4"
# time_beg_d = "12"
# time_beg_hms = "9:00:00"

# span [s]: Time used to calculating one figure.
span = 60 #s
# duration [s]: Total time used to calculating all figures.
duration = 60 #s

fftl = 60   # fftlength
# color = 'b'  # optional / Default ASD plot color is red.

# Amplitude Spectral Density [m/Hz^(1/2)s^2]
# Do not have to define range of ASD. However, if you do, you must define both of them.
# asd_min = 1e-5 # ASD plot min in log scale
# asd_max = 5e-1 # ASD plot max in log scale
# Do not have to define range of frequency. However, if you do, you must define all of them.
# freq_min = 0.0   # Default is 0.0 Hz.
# freq_max = 1000  # Default is 1024 Hz. / Max is 1024 Hz.
# freq_mid = 100  # Need only when pythonfile is "sta_ASD" or "sta_lin2". / Default is 100 Hz.

# Select 1 pythonfile from 1-"sta_ASD", 2-"sta_log", 3-"sta_lin1" or 4-"sta_lin2".
# 1
pythonfile = "sta_ASD" #combine with sta_log, sta_lin1 ans sta_lin2

# 2
# pythonfile = "sta_log" #横軸がlog scale

# 3
# pythonfile = "sta_lin1" #横軸がlinier scale

# 4
# pythonfile = "sta_lin2" #横軸がlinier scale & 比較したい２つの周波数領域を同時plot
# =======================================================================================
# =======================================================================================

# convert gps time
if time_type == "JST":
    jst_beg = time_beg_y + "/" + time_beg_m + "/" + time_beg_d + " " + time_beg_hms + " " + time_type
    gps_beg = int(to_gps( jst_beg ))
gps_end = gps_beg + span

# frequency
if 'freq_min' in locals():
    pass
else:
    freq_min = 0.0
    freq_max = 1024
    freq_mid = 100

# color
if 'color' in locals():
    pass
else:
    color = 'r'

# output input parame
print("this submit file for...", pythonfile)
print("channel:", ch)
if time_type == "JST":
    print("jst_beg:", jst_beg)
print("gps_beg:", gps_beg)
print("span   : %s s" % span)
print("duration : %s s" % duration)

# making submit file
f = open('%s.sub' % pythonfile, 'w')

datalist = [
    'Universe = Vanilla\n', 
    'request_memory = 10 GB\n',
    '\n',
    'Executable = %s.py\n' % pythonfile,
    'Log = %s.log\n' % pythonfile,
    'Output = %s.out\n'% pythonfile,
    'Error = %s.err\n'% pythonfile,
    'Getenv = True\n',
    ]
f.writelines(datalist)

N = duration / span
time_type = "GPS"
for n in range(int(N)):
    # print(n)
    if pythonfile == "sta_log":
        if 'asd_min' in locals():
            datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, asd_min, asd_max),
                'Queue'
                ]
        else:
            datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max),
                'Queue'
                ]
        f.writelines(datalist)
    
    elif pythonfile == "sta_lin1":
        if 'asd_min' in locals():
            datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, asd_min, asd_max),
                'Queue'
                ]
        else:
            datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max),
                'Queue'
                ]
        f.writelines(datalist)
    
    elif pythonfile == "sta_lin2":
        if 'asd_min' in locals():
            datalist = [
                '\n',
                'Arguments = %s %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid, asd_min, asd_max),
                'Queue'
                ]
        else:
            datalist = [
            '\n',
            'Arguments = %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid),
            'Queue'
            ]
        f.writelines(datalist)
    
    elif pythonfile == "sta_ASD":
        if 'asd_min' in locals():
            datalist = [
            '\n',
            'Arguments = %s %s %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid, asd_min, asd_max),
            'Queue'
            ]
        else:
            datalist = [
            '\n',
            'Arguments = %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color, freq_min, freq_max, freq_mid),
            'Queue'
            ]
        f.writelines(datalist)
    
    gps_beg = gps_end
    gps_end = gps_beg + span

f.close()

print()
print("use 'condor_submit %s.sub'" % pythonfile)