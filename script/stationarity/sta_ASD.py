#!/usr/bin/env python
# coding: utf-8
"""
This is making some ASD plots code.
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
from gwpy.signal import filter_design
from gwpy.astro import inspiral_range
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
import scipy.stats
import math
import sys
import os
import warnings
# warnings.filterwarnings('ignore')
import matplotlib as mpl
import glob

# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
ch = sys.argv[0]
time_type = sys.argv[1]
print('channel:', ch)

# getting time
time_type == "GPS"
time_beg = sys.argv[2]
time_end = sys.argv[3]
fftl = sys.argv[4] # fftlength
gps_beg = int(time_beg)
gps_end = int(time_end)
gps_beg_head = int(gps_beg/100000)
gps_end_head = int(gps_end/100000)
span = gps_end - gps_beg
print('start GPS:', gps_beg)
print('end GPS:', gps_end)
print('span: %s s' % span)
print('fftlength: %s s' % fftl)

# getting data
if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head 
    cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  
print('cache file: ', cache_file)

data = TimeSeries.read(cache_file, ch, start=gps_beg, end=gps_end, nproc=4, verbose=True)
print(data)
print()

# calurate ASD
sg = data.spectrogram2(fftlength=fftl, overlap=int(fftl)/2, window='hann') ** (1 / 2.)

# calurate ASD percentile
median = sg.percentile(50)
low = sg.percentile(5)
high = sg.percentile(95)
print("picked up 5%, 50%, and 95% percentile ASD")
print(low)
print(median)
print(high)

# prepare saving png file
dirname = "ASD_plot[%s]-%s/" % (ch[3:], span)
pngname1 = "fftl=%s[%s-%s_span%ss]_asd_log.png" %(fftl, gps_beg, gps_end, span)
pngname2 = "fftl=%s[%s-%s_span%ss]_stg_log.png" %(fftl, gps_beg, gps_end, span)
pngname3 = "fftl=%s[%s-%s_span%ss]_asd_lin1.png" %(fftl, gps_beg, gps_end, span)
pngname4 = "fftl=%s[%s-%s_span%ss]_stg_lin.png" %(fftl, gps_beg, gps_end, span)
pngname5 = "fftl=%s[%s-%s_span%ss]_asd_lin2.png" %(fftl, gps_beg, gps_end, span)
os.makedirs(dirname, exist_ok=True)
filename1 = dirname + pngname1
filename2 = dirname + pngname2
filename3 = dirname + pngname3
filename4 = dirname + pngname4
filename5 = dirname + pngname5

# getting plot parameters
color = sys.argv[5]
freq_min = float(sys.argv[6])
freq_max = float(sys.argv[7])
freq_mid = float(sys.argv[8])

if len(sys.argv) == 11:
    asd_min = float(sys.argv[9])
    asd_max = float(sys.argv[10])

if freq_min == 0:
    freq_min_log = sg.df.value
else:
    freq_min_log = freq_min

# ASD log plot
plot = Plot()

if len(sys.argv) == 11:
    ax = plot.add_subplot(
        xscale='log', 
        xlim=(freq_min_log, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylim=(asd_min, asd_max),
        ylabel='ASD [%s]'% median.unit,
    )
else:
    ax = plot.add_subplot(
        xscale='log', 
        xlim=(freq_min_log, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylabel='ASD [%s]'% median.unit,
    )    
ax.plot_mmm(median, low, high, color=color)
ax.set_title('%s \n [%s-%s] span: %s s, FFT: %s s' %(ch, gps_beg, gps_end, span, fftl),fontsize=16)
plt.savefig(filename1)

# spectrogram_log
if len(sys.argv) == 11:
    plot = sg.imshow(
        norm='log',
        vmin = asd_min,
        vmax = asd_max
    )
else:
    plot = sg.imshow(
        norm='log',
        # vmin=5e-7,
        # vmax=2e-3
    )
ax = plot.gca()
ax.set_yscale('log')
ax.set_ylim(freq_min_log, freq_max)
ax.colorbar(label=r'Amplitude Spectral Density [%s]' % sg.unit)
ax.set_title('%s \n [%s-%s] span: %s s, FFT: %s s' %(ch, gps_beg, gps_end, span, fftl),fontsize=16)
plt.savefig(filename2)

# ASD linear plot 1
plot = Plot()

if len(sys.argv) == 11:
    ax = plot.add_subplot(
        xscale='linear',
        xlim=(freq_min, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylim=(asd_min, asd_max),
        ylabel='ASD [%s]'% median.unit,
    )
else:
    ax = plot.add_subplot(
        xscale='linear',
        xlim=(freq_min, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylabel='ASD [%s]'% median.unit,
    )    
ax.plot_mmm(median, low, high, color=color)
ax.set_title('%s \n [%s-%s] span: %s s, FFT: %s s' %(ch, gps_beg, gps_end, span, fftl),fontsize=16)
plt.savefig(filename3)

# spectrogram_linear
if len(sys.argv) == 11:
    plot = sg.imshow(
        norm='log',
        vmin = asd_min,
        vmax = asd_max
    )
else:
    plot = sg.imshow(
        norm='log',
        # vmin=5e-7,
        # vmax=2e-3
    )
ax = plot.gca()
ax.set_yscale('linear')
ax.set_ylim(freq_min, freq_max)
ax.colorbar(label=r'Amplitude Spectral Density [%s]' % sg.unit)
ax.set_title('%s \n [%s-%s] span: %s s, FFT: %s s' %(ch, gps_beg, gps_end, span, fftl),fontsize=16)
plt.savefig(filename4)

# ASD linear plot 2
plot = Plot()
plot = plt.figure(figsize=(15, 6))

if len(sys.argv) == 11:
    ax1 = plot.add_subplot(211,
        xscale='linear',
        xlim=(freq_min, freq_mid),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylim=(asd_min, asd_max),
        ylabel='ASD [%s]'% median.unit,
    )
    ax2 = plot.add_subplot(212,
    xscale='linear',
    xlim=(freq_min, freq_max),
    xlabel='Frequency [Hz]',
    yscale='log',
    ylim=(asd_min, asd_max),
    ylabel='ASD [%s]'% median.unit,
    )
else:
    ax1 = plot.add_subplot(211,
        xscale='linear',
        xlim=(freq_min, freq_mid),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylabel='ASD [%s]'% median.unit,
    )
    ax2 = plot.add_subplot(212,
    xscale='linear',
    xlim=(freq_min, freq_max),
    xlabel='Frequency [Hz]',
    yscale='log',
    ylabel='ASD [%s]'% median.unit,
    )

ax1.plot_mmm(median, low, high, color=color)
ax1.set_title('%s \n [%s-%s] span: %s s, FFT: %s s' %(ch, gps_beg, gps_end, span, fftl),fontsize=16)

ax2.plot_mmm(median, low, high, color=color)
# ax2.set_title('%s \n [%s-%s]' %(ch, gps_beg, gps_end),fontsize=16)
plt.tight_layout()
plt.savefig(filename5)

# saving data file
savename = "fftl=%s[%s-%s_span%ss]" %(fftl, gps_beg, gps_end, span)
np.savez(dirname+savename, 
         frequency=median.frequencies, 
         asd_5p=low.value, 
         asd_50p=median.value, 
         asd_95p=high.value
        )

# making Tex file
def write_tex_img(img_path, size=120):
    img_script = '\\begin{figure}[htbp]' '\n'\
                    '\centering' '\n'\
                    '\includegraphics[width=' + str(size) +'mm]{' + img_path + '}' '\n'\
                    '\end{figure}' '\n\n'
    return img_script

def write_tex_header(time):
    header_script = '\n' '\clearpage' '\n'\
                '\section{' + time + '}' '\n\n'
    return header_script

def write_tex_main(tex_list):
    input_script = ''
    for file in tex_list:
        input_script += '\input{' + file + '}\n'

    main_script = '\documentclass{article}' '\n'\
                    '\\usepackage[margin=20mm]{geometry} \n'\
                    '\\usepackage{graphicx} \n'\
                    '\\begin{document}' '\n'\
                    + input_script \
                    + '\end{document}'
    return main_script

dirname_TeX = "TeX_list_%s-%s/" % (ch[3:], span)
os.makedirs(dirname_TeX, exist_ok=True)

# pdf script for log ######################################
with open(dirname_TeX + 'TeX_ASD_plot_%s-%s_' % (ch[3:], span) + savename + '_log.tex', 'w') as f:
    files = [filename1, filename2]
    time_beg_jst = from_gps(time_beg)
    header_script = write_tex_header("fftl=%s[%s-%s span%ss]" %(fftl, gps_beg, gps_end, span) + '(' + str(time_beg_jst) + ')')
    f.write(header_script)    # making header
    for file in files:
        img_script = write_tex_img(file)
        f.write(img_script)

tex_list = sorted(glob.glob(dirname_TeX + '*_log.tex'))
if 'ASD_plot_%s-%s' % (ch[3:], span) + '_log.tex' in tex_list:
    tex_list.remove('ASD_plot_%s-%s' % (ch[3:], span) + '_log.tex')
main_script = write_tex_main(tex_list)
with open('./ASD_plot_%s-%s' % (ch[3:], span) + '_log.tex', 'w') as f_main:
    f_main.write(main_script)

# pdf script for line ######################################
with open(dirname_TeX + 'TeX_ASD_plot_%s-%s_' % (ch[3:], span) + savename + '_line.tex', 'w') as f:
    files = [filename3, filename4, filename5]
    time_beg_jst = from_gps(time_beg)
    header_script = write_tex_header("fftl=%s[%s-%s span%ss]" %(fftl, gps_beg, gps_end, span) + '(' + str(time_beg_jst) + ')')
    f.write(header_script)    # making header
    for file in files:
        img_script = write_tex_img(file, size=100)
        f.write(img_script)

tex_list = sorted(glob.glob(dirname_TeX + '*_line.tex'))
if 'ASD_plot_%s-%s' % (ch[3:], span) + '_line.tex' in tex_list:
    tex_list.remove('ASD_plot_%s-%s' % (ch[3:], span) + '_line.tex')
main_script = write_tex_main(tex_list)
with open('./ASD_plot_%s-%s' % (ch[3:], span) + '_line.tex', 'w') as f_main:
    f_main.write(main_script)

print()
print("all done")
plt.show()