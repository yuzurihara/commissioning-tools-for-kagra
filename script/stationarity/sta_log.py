#!/usr/bin/env python
# coding: utf-8
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
from gwpy.signal import filter_design
from gwpy.astro import inspiral_range
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
import scipy.stats
import math
import sys
import os
import warnings
# warnings.filterwarnings('ignore')
import matplotlib as mpl

# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
ch = sys.argv[0]
time_type = sys.argv[1]
print('channel:', ch)

# getting time
time_type == "GPS"
time_beg = sys.argv[2]
time_end = sys.argv[3]
fftl = sys.argv[4]   # fftlength
gps_beg = int(time_beg)
gps_end = int(time_end)
gps_beg_head = int(gps_beg/100000)
gps_end_head = int(gps_end/100000)
span = gps_end -gps_beg
print('start GPS:', gps_beg)
print('end GPS:', gps_end)
print('span: %s s' % span)
print('fftlength: %s s' % fftl)

# getting data
if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head 
    cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  
print('cache file: ', cache_file)

data = TimeSeries.read(cache_file, ch, start=gps_beg, end=gps_end, nproc=4, verbose=True)
print(data)
print()

# calurate ASD
sg = data.spectrogram2(fftlength=fftl, overlap=int(fftl)/2, window='hann') ** (1 / 2.)

# calurate ASD percentile
median = sg.percentile(50)
low = sg.percentile(5)
high = sg.percentile(95)
print("picked up 5%, 50%, and 95% percentile ASD")
print(low)
print(median)
print(high)

# prepare saving png file
dirname = "%s_%s/" % (ch[3:], span)
pngname1 = "fftl=%s[%s-%s_span%ss]_asd_log.png" %(fftl, gps_beg, gps_end, span)
pngname2 = "fftl=%s[%s-%s_span%ss]_stg.png" %(fftl, gps_beg, gps_end, span)
os.makedirs(dirname, exist_ok=True)
filename1 = dirname + pngname1
filename2 = dirname + pngname2

# getting plot parameters
color = sys.argv[5]
freq_min = float(sys.argv[6])
freq_max = float(sys.argv[7])

if len(sys.argv) == 10:
    asd_min = float(sys.argv[8])
    asd_max = float(sys.argv[9])

if freq_min == 0:
    freq_min_log = sg.df.value
else:
    freq_min_log = freq_min

# ASD log plot
plot = Plot()

if len(sys.argv) == 10:
    ax = plot.add_subplot(
        xscale='log', 
        xlim=(freq_min_log, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylim=(asd_min, asd_max),
        ylabel='ASD [%s]'% median.unit,
    )
else:
    ax = plot.add_subplot(
        xscale='log', 
        xlim=(freq_min_log, freq_max),
        xlabel='Frequency [Hz]',
        yscale='log',
        ylabel='ASD [%s]'% median.unit,
    )    
ax.plot_mmm(median, low, high, color=color)
ax.set_title('%s \n [%s-%s] span: %s s, FFT: %s s' %(ch, gps_beg, gps_end, span, fftl),fontsize=16)
plt.savefig(filename1)

# spectrogram_log
if len(sys.argv) == 10:
    plot = sg.imshow(
        norm='log',
        vmin = asd_min,
        vmax = asd_max
    )
else:
    plot = sg.imshow(
        norm='log',
        # vmin=5e-7,
        # vmax=2e-3
    )
ax = plot.gca()
ax.set_yscale('log')
ax.set_ylim(freq_min_log, freq_max)
ax.colorbar(label=r'Amplitude Spectral Density [%s]' % sg.unit)
ax.set_title('%s \n [%s-%s] span: %s s, FFT: %s s' %(ch, gps_beg, gps_end, span, fftl),fontsize=16)
plt.savefig(filename2)

# saving data file
savename = "fftl=%s[%s-%s_span%ss]" %(fftl, gps_beg, gps_end, span)
np.savez(dirname+savename, 
         frequency=median.frequencies, 
         asd_5p=low.value, 
         asd_50p=median.value, 
         asd_95p=high.value
        )

print()
print("all done")
plt.show()