#!/usr/bin/env python
# coding: utf-8
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, StateTimeSeries
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
from itertools import product
from scipy import stats
# from lmfit.models import LorentzianModel
from gwpy.astro import inspiral_range, range_timeseries, inspiral_range_psd
import matplotlib.dates as mdates
from csv import writer
import os
import matplotlib as mpl

# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
gps_beg = int(sys.argv[0])
gps_end = int(sys.argv[1])

mpl.rcParams['xtick.labelsize'] = 20
mpl.rcParams['ytick.labelsize'] = 20
mpl.rcParams["axes.labelsize"] = 20
mpl.rcParams["axes.titlesize"] = 20
mpl.rcParams["legend.fontsize"] = 20
mpl.rcParams['axes.linewidth'] = 2
mpl.rcParams['figure.facecolor'] = "white"
mpl.rcParams['mathtext.fontset'] = 'stix'
mpl.rcParams['font.family'] = 'STIXGeneral'

channel = [
    "K1:VIS-ETMX_TM_COILDRV_VMON_H1_OUT_DQ", 
    "K1:VIS-ETMX_TM_COILDRV_VMON_H2_OUT_DQ", 
    "K1:VIS-ETMX_TM_COILDRV_VMON_H3_OUT_DQ", 
    "K1:VIS-ETMX_TM_COILDRV_VMON_H4_OUT_DQ",
    "K1:LSC-MICH_IN1_DQ",
    "K1:LSC-PRCL_IN1_DQ",
    "K1:VIS-ETMY_TM_COILDRV_VMON_H1_OUT_DQ", 
    "K1:VIS-ETMY_TM_COILDRV_VMON_H2_OUT_DQ", 
    "K1:VIS-ETMY_TM_COILDRV_VMON_H3_OUT_DQ", 
    "K1:VIS-ETMY_TM_COILDRV_VMON_H4_OUT_DQ",
    "K1:CAL-CS_PROC_DARM_DISPLACEMENT_DQ",
    ]
freq_list = [20, 25, 30, 35, 40, 45, 160, 165, 170, 175, 180, 185]

channel_OMC = [
    "K1:OMC-TRANS_DC_A_IN1_DQ",
    "K1:OMC-TRANS_DC_B_IN1_DQ",
    "K1:OMC-TRANS_DC_SUM_OUT_DQ"
]

nproc=4
fftlength = 8
fmin_range=20

def read_data(gps_beg, gps_end, channel):
    gps_dir1 = int(gps_beg / 100000)
    gps_dir2 = int(gps_end / 100000)
    print(gps_beg, gps_end, gps_dir1, gps_dir2)
    dur = gps_end - gps_beg

    cache_file="%s_%s.ffl" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for j in range(gps_dir1, gps_dir2+1):
            # cache1="/home/devel/cache/Cache_GPS/%s.ffl" % j
            cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % j
            with open(cache1) as infile:
                outfile.write(infile.read())

    input1 = TimeSeries.read(cache_file, channel, start=gps_beg, end=gps_end, nproc=nproc, verbose=True)
    os.remove(cache_file)
    return input1

def plot_highpass(input1, gps_beg, gps_end, fmin, fmax):

    input1h = input1.bandpass(fmin, fmax, ftype='butter')

    fig = Plot(figsize=(12, 5))
    ax = fig.add_subplot(xscale='auto-gps')
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')
    ax.plot(input1h, label="%s UTC" % t0)
    ax.set_epoch(gps_beg)
    ax.set_ylabel('DARM displacement [m]')
    ax.set_title('%s, bandpass filter (%d, %d) Hz' % (channel, fmin, fmax))
    ax.legend()
    fig.savefig("%d_%d_fmin%d_fmax%d.png" % (gps_beg, gps_end, fmin, fmax))
    fig.show()

def plot_highpass3(input1, plot_beg, plot_end, freq_list, num_ch, thereshold=0.5):#特定のチャンネルでさまざまな周波数フィルターをかけたplot
    fig = Plot(figsize=(12, 30), sharex=True)

    for i in range(len(freq_list)):
        ax = fig.add_subplot(len(freq_list), 1, i+1, xscale='auto-gps')
        input1h = input1.bandpass(freq_list[i], freq_list[i]+5, ftype='butter')
        input2 = input1h.crop(plot_beg+1, plot_end-1)

        if i==0:
            t0=from_gps(input2.t0.value).strftime('%Y-%m-%d %H:%M:%S')
            ax.set_title('%s' % (channel[num_ch])) 
        
        ax.plot(input2, label="bandpass (%d, %d) Hz" %(freq_list[i], freq_list[i]+5), color="magenta")
        ax.legend(loc="upper right")
        # ax.set_ylabel('DARM displacement [m]')
        ax.set_ylim(min(input2.value), max(input2.value))
        print(min(input2.value), max(input2.value))
        ax.set_epoch(plot_beg)
    
    plt.tight_layout()
    fig.subplots_adjust(hspace=0.5) 
    fig.savefig(channel[num_ch]+"/%d_%d_[%s_%sHz].png" % (plot_beg, plot_beg, min(freq_list), max(freq_list)))
    fig.show()

def plot_highpass4(channel, plot_beg, plot_end, freq, thereshold=0.5):#特定の周波数でさまざまなチャンネルをplot[6x1]
    fig = Plot(figsize=(12, 30), sharex=True)

    for i in range(len(channel)):
        ax = fig.add_subplot(len(channel), 1, i+1, xscale='auto-gps')
        input1 = read_data(gps_beg, gps_end, channel[i])
        input1h = input1.bandpass(freq, freq+5, ftype='butter')
        input2 = input1h.crop(plot_beg+1, plot_end-1)

        if i==0:
            ax.set_title("bandpass (%d, %d) Hz" %(freq, freq+5)) 
        
        ax.plot(input2, label='%s' % (channel[i]), color="magenta")
        ax.legend(loc="upper right")
        # ax.set_ylabel('DARM displacement [m]')
        ax.set_ylim(min(input2.value), max(input2.value))
        print(min(input2.value), max(input2.value))
        ax.set_epoch(plot_beg)
    
    plt.tight_layout()
    fig.subplots_adjust(hspace=0.5) 
    fig.savefig("png/[%s-%sHz]_%d_%d.png" % (freq, freq+5, plot_beg, plot_beg))
    fig.show()

def plot_highpass5(channel, plot_beg, plot_end, freq, thereshold=0.5):#特定の周波数でさまざまなチャンネルをplot[6x2]
    fig = Plot(figsize=(16, 20), sharex=True)
    num_plots = min(len(channel), 12)

    for i in range(num_plots):
        row = i % 6 + 1  #1-6の行番号
        col = i // 6 + 1  #1-2の列番号
        ax = fig.add_subplot(6, 2, (row -1)*2+col, xscale='auto-gps')
        input1 = read_data(gps_beg, gps_end, channel[i])
        input1h = input1.bandpass(freq, freq+5, ftype='butter')
        input2 = input1h.crop(plot_beg+1, plot_end-1)

        if i==0:
            ax.set_title("bandpass (%d, %d) Hz" %(freq, freq+5)) 
        
        ax.plot(input2, label='%s' % (channel[i]), color="magenta")
        ax.legend(loc="upper right", fontsize=10)
        # ax.set_ylabel('DARM displacement [m]')
        ax.set_ylim(min(input2.value), max(input2.value))
        print(min(input2.value), max(input2.value))
        ax.set_epoch(plot_beg)
        ax.tick_params(axis='both', labelsize=10)
    
    plt.tight_layout()
    fig.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05, hspace=0.4, wspace=0.3)  # 余白調整 
    fig.savefig("png_[6x4]/[1301]/[%s-%sHz]_%d_%d.png" % (freq, freq+5, plot_beg, plot_beg), dpi=300)
    fig.show()

def plot_highpass6(channel, plot_beg, plot_end, freq, freq_list, thereshold=0.5):#特定の周波数でさまざまなチャンネル[最大12]と，特定のチャンネルでさまざまな周波数フィルタ[最大12]をかけたplot[6x4]
    fig = Plot(figsize=(30, 24), sharex=True)

    for i in range(min(len(channel), 12)):
        row = i % 6 + 1  #1-6の行番号
        col = i // 6 + 1  #1-2の列番号
        print(row, col)
        ax = fig.add_subplot(6, 4, (row -1)*4+col, xscale='auto-gps')
        input1 = read_data(gps_beg, gps_end, channel[i])
        input1h = input1.bandpass(freq, freq+5, ftype='butter')
        input2 = input1h.crop(plot_beg+2, plot_end-2)

        if i==0:
            ax.set_title("bandpass (%d, %d) Hz" %(freq, freq+5)) 
        
        ax.plot(input2, label='%s' % (channel[i]), color="magenta")
        ax.legend(loc="upper right", fontsize=10)
        # ax.set_ylabel('DARM displacement [m]')
        # print(min(input2.value), max(input2.value))
        ax.set_ylim(min(input2.value), max(input2.value))
        ax.set_epoch(plot_beg)
        ax.tick_params(axis='both', labelsize=10)
    
    for i in range(min(len(freq_list),12)):
        row = i % 6 + 1  #1-6の行番号
        col = i // 6 + 3  #3-4の列番号
        print(row, col)
        ax = fig.add_subplot(6, 4, (row-1)*4+col, xscale='auto-gps')
        input1 = read_data(gps_beg, gps_end, channel[-1]) #channleの最後の要素で固定．
        input1h = input1.bandpass(freq_list[i], freq_list[i]+5, ftype='butter')
        input2 = input1h.crop(plot_beg+2, plot_end-2)

        if i==0 or i==6:
            ax.set_title('%s' % (channel[-1]))
        
        ax.plot(input2, label="bandpass (%d, %d) Hz" %(freq_list[i], freq_list[i]+5), color="magenta")
        ax.legend(loc="upper right", fontsize=10)
        # ax.set_ylabel('DARM displacement [m]')
        # print(min(input2.value), max(input2.value))
        ax.set_ylim(min(input2.value), max(input2.value))
        ax.set_epoch(plot_beg)
        ax.tick_params(axis='both', labelsize=10)

    plt.tight_layout()
    fig.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05, hspace=0.4, wspace=0.3)  # 余白調整 
    fig.savefig("png_[6x4]/%d_%d.png" % (plot_beg, plot_end), dpi=300)
    fig.show()

def plot_data(channel, plot_beg, plot_end):
    fig = Plot()
    for i in range(len(channel)):
        ax = fig.add_subplot(len(channel), 1, i+1, xscale='auto-gps')
        input1 = read_data(gps_beg, gps_end, channel[i])
        input2 = input1.crop(plot_beg+2, plot_end-2)
        ax.plot(input2, label='%s' % (channel[i]), color="magenta")
        ax.legend(loc="upper right", fontsize=10)
        ax.set_ylim(-40000, 40000)
        ax.set_epoch(plot_beg)
        ax.tick_params(axis='both', labelsize=10)
    
    plt.tight_layout()
    # fig.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05, hspace=0.4, wspace=0.3)  # 余白調整 
    fig.savefig("png_OMC/%d_%d.png" % (plot_beg, plot_end), dpi=300)
    fig.show()



# ====
print("-- START --")
# for m in range(len(channel)):
#     if os.path.isdir(channel[m]):
#         pass
#     else:
#         os.makedirs(channel[m])

# input1 = read_data(gps_beg, gps_end, channel[m])

for i in range(100):
    print("for %s" % i)
    plot_beg = gps_beg + i*5000
    plot_end = plot_beg + 5000
    print(plot_beg, plot_end)

    if (plot_end > gps_end):
        print("plot_end > gps_end")
        gps_end1 = gps_end
    if (plot_beg > gps_end):
        print("plot_beg > gps_end")
        break
    
    # plot_highpass3(input1, plot_beg, plot_end, freq, m) #highpassかけた時系列データplot
    # plot_highpass5(channel, plot_beg, plot_end, 38) #特定の周波数でさまざまなチャンネルをplot
    plot_highpass6(channel, plot_beg, plot_end, 38, freq_list) #特定の周波数でさまざまなチャンネルをplot
    plot_data(channel_OMC, plot_beg, plot_end)

print("-- STOP --")