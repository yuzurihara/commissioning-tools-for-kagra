#!/usr/bin/env python
# coding: utf-8
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, StateTimeSeries
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
from itertools import product
from scipy import stats
# from lmfit.models import LorentzianModel
from gwpy.astro import inspiral_range, range_timeseries, inspiral_range_psd
import matplotlib.dates as mdates
from csv import writer
import os
import matplotlib as mpl

# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
gps_beg = int(sys.argv[0])
gps_end = int(sys.argv[1])

mpl.rcParams['xtick.labelsize'] = 20
mpl.rcParams['ytick.labelsize'] = 20
mpl.rcParams["axes.labelsize"] = 20
mpl.rcParams["axes.titlesize"] = 20
mpl.rcParams["legend.fontsize"] = 20
mpl.rcParams['axes.linewidth'] = 2
mpl.rcParams['figure.facecolor'] = "white"
mpl.rcParams['mathtext.fontset'] = 'stix'
mpl.rcParams['font.family'] = 'STIXGeneral'

nproc=4

channels = [
    "K1:LSC-DARM_IN1_DQ",    
]

channel = "K1:CAL-CS_PROC_DARM_DISPLACEMENT_DQ"

fftlength = 8

fmin_range=20


def read_data(gps_beg, gps_end):
    gps_dir1 = int(gps_beg / 100000)
    gps_dir2 = int(gps_end / 100000)
    print(gps_beg, gps_end, gps_dir1, gps_dir2)
    dur = gps_end - gps_beg

    cache_file="%s_%s.ffl" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for j in range(gps_dir1, gps_dir2+1):
            # cache1="/home/devel/cache/Cache_GPS/%s.ffl" % j
            cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % j
            with open(cache1) as infile:
                outfile.write(infile.read())

    input1 = TimeSeries.read(cache_file, channel, start=gps_beg, end=gps_end, nproc=nproc, verbose=True)
    os.remove(cache_file)
    return input1

def plot_highpass(input1, gps_beg, gps_end, fmin, fmax):

    input1h = input1.bandpass(fmin, fmax, ftype='butter')

    fig = Plot(figsize=(12, 5))
    ax = fig.add_subplot(xscale='auto-gps')
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')
    ax.plot(input1h, label="%s UTC" % t0)
    ax.set_epoch(gps_beg)
    ax.set_ylabel('DARM displacement [m]')
    ax.set_title('%s, bandpass filter (%d, %d) Hz' % (channel, fmin, fmax))
    ax.legend()
    fig.savefig("%d_%d_fmin%d_fmax%d.png" % (gps_beg, gps_end, fmin, fmax))
    fig.show()

def plot_highpass1(input1, gps_beg, gps_end, threshold=0.5):

    fig = Plot(figsize=(12, 30), sharex=True)
    ax = fig.add_subplot(6, 1, 1, xscale='auto-gps')
    fmin=160; fmax=165    
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')
    ax.set_title('%s, %s UTC' % (channel, t0))    
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(6, 1, 2, xscale='auto-gps')
    fmin=165; fmax=170
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')    
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(6, 1, 3, xscale='auto-gps')
    fmin=170; fmax=175
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')        
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(6, 1, 4, xscale='auto-gps')
    fmin=175; fmax=180
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')            
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(6, 1, 5, xscale='auto-gps')
    fmin=180; fmax=185
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')                
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')    
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(6, 1, 6, xscale='auto-gps')
    fmin=185; fmax=190
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')                    
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')    
    ax.set_epoch(gps_beg)
    
    fig.savefig("png_5000/%d_%d_violin.png" % (gps_beg, gps_end))
    fig.show()

def plot_highpass2(input1, gps_beg, gps_end, threshold=0.5):

    fig = Plot(figsize=(12, 30), sharex=True)
    ax = fig.add_subplot(7, 1, 1, xscale='auto-gps')
    fmin=15; fmax=20    
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')
    ax.set_title('%s, %s UTC' % (channel, t0))    
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_ylim(min(input1h.value), max(input1h.value)) 
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(7, 1, 2, xscale='auto-gps')
    fmin=20; fmax=25
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')    
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_ylim(min(input1h.value), max(input1h.value)) 
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(7, 1, 3, xscale='auto-gps')
    fmin=25; fmax=30
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')        
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_ylim(min(input1h.value), max(input1h.value)) 
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(7, 1, 4, xscale='auto-gps')
    fmin=30; fmax=35
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')            
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_ylim(min(input1h.value), max(input1h.value)) 
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(7, 1, 5, xscale='auto-gps')
    fmin=35; fmax=40
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')                
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')
    ax.set_ylim(min(input1h.value), max(input1h.value))     
    ax.set_epoch(gps_beg)
    
    ax = fig.add_subplot(7, 1, 6, xscale='auto-gps')
    fmin=40; fmax=45
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')                    
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]') 
    ax.set_ylim(min(input1h.value), max(input1h.value))   
    ax.set_epoch(gps_beg)

    ax = fig.add_subplot(7, 1, 7, xscale='auto-gps')
    fmin=45; fmax=50
    input1h = input1.bandpass(fmin, fmax, ftype='butter')   
    t0=from_gps(input1h.t0.value).strftime('%Y-%m-%d %H:%M:%S')                    
    if max(abs(input1h.value)) > threshold:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="magenta")
    else:
        ax.plot(input1h, label="bandpass (%d, %d) Hz" %(fmin, fmax), color="gray", alpha=0.8)
    ax.legend(loc="upper right")
    ax.set_ylabel('DARM displacement [m]')  
    ax.set_ylim(min(input1h.value), max(input1h.value))   
    ax.set_epoch(gps_beg)
    
    fig.savefig("png_5000/%d_%d_15_50Hz.png" % (gps_beg, gps_end))
    fig.show()

# ====
print("-- START --")
input1 = read_data(gps_beg, gps_end)

for i in range(100):
    print("for %s" % i)
    gps_beg1 = gps_beg + i*5000
    gps_end1 = gps_beg1 + 5000
    print(gps_beg1, gps_end1)

    if (gps_end1 > gps_end):
        print("gps_end1 > gps_end")
        gps_end1 = gps_end
    if (gps_beg1 > gps_end):
        print("gps_beg1 > gps_end")
        break
    
    print("001")
    input2 = input1.crop(gps_beg1, gps_end1)
    print("010")
    plot_highpass2(input2, gps_beg1, gps_end1) #15-50Hz
    print("100")
    plot_highpass1(input2, gps_beg1, gps_end1) #160-190Hz

print("-- STOP --")