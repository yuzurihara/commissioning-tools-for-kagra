#!/usr/bin/env python
# coding: utf-8
"""
This is two channels timeseries overlay plot code.
plot data is normalized by maximum difference between data and mean to compere two channels.
Making by Kaori Obayashi
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
from gwpy.signal import filter_design
from gwpy.astro import inspiral_range
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
import scipy.stats
import math
import sys
import os
import warnings
# warnings.filterwarnings('ignore')
import matplotlib as mpl
fig = plt.figure()

# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
ch_x = sys.argv[0]
ch_y = sys.argv[1]
chs = sys.argv[0:2]
time_type = sys.argv[2]

# getting time
if time_type == "JST":
    time_beg_y = sys.argv[3]
    time_beg_m = sys.argv[4]
    time_beg_d = sys.argv[5]
    time_beg_hms = sys.argv[6]
    time_end_y = sys.argv[7]
    time_end_m = sys.argv[8]
    time_end_d = sys.argv[9]
    time_end_hms = sys.argv[10]

    jst_beg = time_beg_y + "/" + time_beg_m + "/" + time_beg_d + " " + time_beg_hms + " " + time_type
    jst_end = time_end_y + "/" + time_end_m + "/" + time_end_d + " " + time_end_hms + " " + time_type
    gps_beg = int(to_gps( jst_beg ))
    gps_end = int(to_gps( jst_end ))
    gps_beg_head = int(gps_beg/100000)
    gps_end_head = int(gps_end/100000)

    data_beg = jst_beg
    data_end = jst_end

elif time_type == "GPS":
    time_beg = sys.argv[3]
    time_end = sys.argv[4]

    gps_beg = int(time_beg)
    gps_end = int(time_end)
    gps_beg_head = int(gps_beg/100000)
    gps_end_head = int(gps_end/100000)

    data_beg = gps_beg
    data_end = gps_end

# saving png file
dirname = "timeseries_ol_plot_image/"
dataname = "top_img_%s&%s_{%s_to_%s}.png" % (ch_x[3:], ch_y[3:], gps_beg, gps_end)
os.makedirs(dirname, exist_ok=True)
filename = dirname + dataname
print("output file：%s" % filename)

# #========================================
# getting data 
i = 0  

if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.cache" % gps_end_head 
    cache_file="/tmp/%s_%s.cache" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  

datax = TimeSeries.read(cache_file, ch_x, start=gps_beg, end=gps_end, nproc=4, verbose=True)
datay = TimeSeries.read(cache_file, ch_y, start=gps_beg, end=gps_end, nproc=4, verbose=True)

# normalized and plot
datax_norm = (datax - datax.mean())/(np.abs(datax - datax.mean())).max()
datay_norm = (datay - datay.mean())/(np.abs(datay - datay.mean())).max()
print("**********************:")
print(datax)
print("x.mean:", datax.mean())
print("x.max:", datax.mean())
print("x.min:", datax.mean())
print("(x-xmean).max", (datax - datax.mean()).max())
print("abs(x-xmean).max", (np.abs(datax - datax.mean())).max())
print("***")
print(datay)
print("y.mean:", datay.mean())
print("y.max:", datay.mean())
print("y.min:", datay.mean())
print("(y-ymean).max", (datay - datay.mean()).max())
print("abs(y-ymean).max", (np.abs(datay - datay.mean())).max())

plot = datax_norm.plot(label='%s'% ch_x, alpha=0.5)

ax = plot.gca()
ax.plot(datay_norm, label='%s'% ch_y, alpha=0.5)

ax.legend(frameon=False,bbox_to_anchor=(1.,1.), loc = 'lower right' , ncol =2)
plt.show()
plt.savefig(filename)