#!/usr/bin/env python
# coding: utf-8
"""
# This is making submit file for log code
# Making by Mayu Murakoshi
"""
from gwpy.time import from_gps, to_gps
import os

# =========================================
# =========================================
ch = 'K1:PEM-ACC_EXA_TABLE_RX_Z_OUT_DQ'
# ch = 'K1:PEM-ACC_EYA_TABLE_RX_Z_OUT_DQ'

## JST
start_year = '2020'
start_month = '04'
start_date = '12'
start_hms = '09:00:00'

## one day = 86400, eight hours = 28800, ten minutes = 600
length = 600
span = 600

fftlength = 60

## range of frequency in float (max is 1024 Hz)
## No need to define both, but you must not define only one.
# freq_min = 0.
# freq_max = 1000.

## blue: 2020/4/13 9:00-17:00 JST
## red: 2020/4/12 9:00-17:00 JST
## green: 2022/8/21 20:00-8/22 4:00 JST
# plot_color = 'blue'
plot_color = 'red'
# plot_color = 'green'

date = 221003
dirname = '/home/mayu.murakoshi/test/PEM_sensor/%s/%s' % (ch, date)
filename = 'PEM_sensor_log'
# =========================================
# =========================================

# convert time
start_time = start_year + "/" + start_month + "/" + start_date + " " + start_hms + " " + "JST"
start_gps = int(to_gps(start_time))
stop_gps = start_gps + length
start_utc = from_gps(start_gps)

print("start GPS: %s" % start_gps)
print("start UTC: %s" % start_utc)
print("length: %s s" % length)

# division number
N = int(length/span)
print("division number: %s" % N)

# 上書きor新規作成モードでファイルを開き、文字列を書き込み(mode='w')
os.makedirs(dirname, exist_ok=True)

with open('/home/mayu.murakoshi/test/PEM_sensor/%s.sub' % filename, 'w') as f:
    f.write("# submit file for runing acc_test.py\n")
    f.write("# Argments are [channel] [start_gps] [end_gps] [fftlength] [plot_color] [dirname] [freq_min] [freq_max]\n")
    f.write("\n")
    f.write("Universe = Vanilla\n")
    f.write("\n")
    f.write("Log = %s/%s.log\n" % (dirname, filename))
    f.write("Executable = /home/mayu.murakoshi/test/PEM_sensor/%s.py\n" % filename)
    f.write("request_memory = 10 GB\n")
    f.write("\n")
    f.write("Output = %s/%s.out\n" % (dirname, filename))
    f.write("Error = %s/%s.err\n" % (dirname, filename))
    f.write("Getenv = True\n")

num = 1

# check def of the range of frequency
if 'freq_min' in locals():
    print('freq_min: %s Hz' % freq_min)
    print('freq_max: %s Hz' % freq_max)
    print()
    for start_gps in range (start_gps, stop_gps, span):
        end_gps = start_gps + span
        print('[%s]' % num)
        print("st: %s" % start_gps)
        print("en: %s" % end_gps)
        print()
        num = num +1
        # ファイルの末尾に追記される。
        with open('/home/mayu.murakoshi/test/PEM_sensor/%s.sub' % filename, 'a') as f:
            f.write("\n")
            f.write("Arguments = %s %s %s %s %s %s %s %s\n" %(ch, start_gps, end_gps, fftlength, plot_color, dirname, freq_min, freq_max))
            f.write("Queue\n")

else:
    print('freq_min and freq_max are not defined.')
    print()
    for start_gps in range (start_gps, stop_gps, span):
        end_gps = start_gps + span
        print('[%s]' % num)
        print("st: %s" % start_gps)
        print("en: %s" % end_gps)
        print()
        num = num +1
        # ファイルの末尾に追記される。
        with open('/home/mayu.murakoshi/test/PEM_sensor/%s.sub' % filename, 'a') as f:
            f.write("\n")
            f.write("Arguments = %s %s %s %s %s %s\n" %(ch, start_gps, end_gps, fftlength, plot_color, dirname))
            f.write("Queue\n")

