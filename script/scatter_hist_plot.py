#!/usr/bin/env python
# coding: utf-8
"""
This is two channels scatter and histogram plot code
Making by Kaori Obayashi
 v2：updated input arguments for GPS time.
 v3：updated adjust sampling rate.
 v4：improved code to make it easier to see.
 v5：changed file name of plot image.：not include ":"
 v6：changed plot transparency
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
from gwpy.signal import filter_design
from gwpy.astro import inspiral_range
from gwpy.plot import Plot
from matplotlib import pyplot as plt
import numpy as np
import sys, traceback
from gwpy.time import from_gps, to_gps
from datetime import datetime, timedelta
import scipy.stats
import math
import sys
import os
import warnings
# warnings.filterwarnings('ignore')
import matplotlib as mpl

# formatting
config = {  
    "xtick.labelsize": 16,
    "ytick.labelsize": 16,
    "axes.labelsize":  20,
    "axes.titlesize":  20,
    "legend.fontsize": 25,
    "axes.linewidth":  2,
    "mathtext.fontset": 'stix',
    "font.family": 'STIXGeneral' 
    }

# define scatter_hist
def scatter_hist(data1, data2, ax, ax_histx, ax_histy, color):
    ax_histx.tick_params(axis="x", labelbottom=False) #nothing x-label 
    ax_histy.tick_params(axis="y", labelleft=False) #nothin y-label
    ax.scatter(data1, data2, color=color, s=3, alpha=0.5) #scatter plot
    ax.set_xlabel(data1.name)
    ax.set_ylabel(data2.name)
    
    bins = 50
    
    ax_histx.hist(data1, bins=bins, alpha=0.4, color=color) #making x-histogram
    ax_histx.set_yscale('log')
    ax_histy.hist(data2, bins=bins,  alpha=0.4, orientation='horizontal', color=color) #making y-histogram
    ax_histy.set_xscale('log')

# define scatter_hist_init
def scatter_hist_init(left, width, bottom, height, spacing,figx, figy):
    fig_scatter = [left, bottom, width, height] #define scatter
    fig_histx = [left, bottom + height + spacing, width, 0.28] #define upper histogram
    fig_histy = [left + width + spacing, bottom, 0.28, height] #define right histogram

    global fig, ax, ax_histx, ax_histy 

    fig = plt.figure(figsize=(figx, figy)) 
    ax = fig.add_axes(fig_scatter) 
    ax_histx = fig.add_axes(fig_histx, sharex=ax) 
    ax_histy = fig.add_axes(fig_histy, sharey=ax) 

    return fig, ax, ax_histx, ax_histy

# define Pearson's correlation coefficient function
def pearson_corr(x, y):
    x_diff = x - np.mean(x)
    y_diff = y - np.mean(y)
    return np.dot(x_diff, y_diff) / (np.sqrt(sum(x_diff ** 2)) * np.sqrt(sum(y_diff ** 2)))

# #=======================================
# getting arguments
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

del sys.argv[0]
ch_x = sys.argv[0]
ch_y = sys.argv[1]
chs = sys.argv[0:2]
time_type = sys.argv[2]

# getting time
if time_type == "JST":
    time_beg_y = sys.argv[3]
    time_beg_m = sys.argv[4]
    time_beg_d = sys.argv[5]
    time_beg_hms = sys.argv[6]
    time_end_y = sys.argv[7]
    time_end_m = sys.argv[8]
    time_end_d = sys.argv[9]
    time_end_hms = sys.argv[10]

    jst_beg = time_beg_y + "/" + time_beg_m + "/" + time_beg_d + " " + time_beg_hms + " " + time_type
    jst_end = time_end_y + "/" + time_end_m + "/" + time_end_d + " " + time_end_hms + " " + time_type
    gps_beg = int(to_gps( jst_beg ))
    gps_end = int(to_gps( jst_end ))
    gps_beg_head = int(gps_beg/100000)
    gps_end_head = int(gps_end/100000)

    data_beg = jst_beg
    data_end = jst_end

elif time_type == "GPS":
    time_beg = sys.argv[3]
    time_end = sys.argv[4]

    gps_beg = int(time_beg)
    gps_end = int(time_end)
    gps_beg_head = int(gps_beg/100000)
    gps_end_head = int(gps_end/100000)

    data_beg = gps_beg
    data_end = gps_end

# plot color
color=["b", "r", "g", "m", "c", "pink"]

# saving png file
dirname = "scatter_hist_plot_image/"
dataname = "shp_img_%s&%s_{%s_to_%s}.png" % (ch_x[3:], ch_y[3:], gps_beg, gps_end)
os.makedirs(dirname, exist_ok=True)
filename = dirname + dataname
print("output file：%s" % filename)

# #========================================
# getting data 
i = 0  

if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.cache" % gps_end_head 
    cache_file="/tmp/%s_%s.cache" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read())  

data = TimeSeriesDict.read(cache_file, chs, start=gps_beg, end=gps_end, nproc=4, verbose=True)
print(data)

# make same data size
if data[ch_x].sample_rate > data[ch_y].sample_rate:
    data[ch_y] = data[ch_y].resample(data[ch_x].sample_rate)

else:
    data[ch_x] = data[ch_x].resample(data[ch_y].sample_rate)

####################### 
# out put
scatter_hist_init(0.1, 0.59, 0.1, 0.59, 0.02, 12, 12)   
scatter_hist(data[ch_x], data[ch_y], ax, ax_histx, ax_histy, color[i])
Pearson = pearson_corr(data[ch_x], data[ch_y])
ax.scatter([], [], label="%s to %s" % (data_beg, data_end), color=color[i])#dummy plot
ax.scatter([], [], label="pearson corr is %s" % Pearson, alpha=0)# pearson description

print("---------------------------------------")
print("channel：%s " % chs)
print("beg_time（%s）：%s" % (time_type, data_beg))
print("end_time（%s）：%s" % (time_type, data_end))
print("pearson corr：%s" % Pearson)      
 
for ch in chs:
    print("std of %s = %e" % (ch, np.std(data[ch]).value))
    print("ave of %s = %e" % (ch, np.average(data[ch]).value))

fig.legend(loc="upper right")
plt.rcParams.update(config)
plt.savefig(filename)
plt.show()
