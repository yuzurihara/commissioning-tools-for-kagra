#!/usr/bin/env python
# coding: utf-8
from gwpy.time import from_gps, to_gps
import numpy as np

# input parameter =======================================================================
ch = "K1:PEM-ACC_EXA_TABLE_TX_Z_OUT_DQ"
# ch = "K1:PEM-MIC_EYA_BOOTH_EYA_Z_OUT_DQ"

time_type = "JST"
time_beg_y = "2020"
time_beg_m = "04"
time_beg_d = "13"
time_beg_hms = "09:00:00"

span = 10*60 #s
length = 8*60*60 #s

fftl = 60
color = 'b' #20200412=r, 20200413=b, 20220921=g

pythonfile = "sta_log" #横軸がlog scale

# pythonfile = "sta_lin1" #横軸がlinier scale
# freq_min = 0.0
# freq_max = 1024

# pythonfile = "sta_lin2" #横軸がlinier scale & 比較したい２つの周波数領域を同時plot
# freq_min = 0.0
# freq_mid = 100
# freq_max = 1024
# ======================================================================================
# convert gps time
jst_beg = time_beg_y + "/" + time_beg_m + "/" + time_beg_d + " " + time_beg_hms + " " + time_type
gps_beg = int(to_gps( jst_beg ))
gps_end = gps_beg + span
time_type = "GPS"

# output input parame
print("this submit file for...", pythonfile)
print("jst_beg：", jst_beg)
print("gps_beg：", gps_beg)
print("span   ：", span)
print("length ：", length)

# making submit file
f = open('%s.sub' % pythonfile, 'w')

datalist = [
    'Universe = Vanilla\n', 
    'Executable = %s.py\n' % pythonfile,
    'request_memory = 10 GB\n',
    'Output = %s.out\n'% pythonfile,
    'Error = %s.err\n'% pythonfile,
    'Getenv = True\n',
    ]
f.writelines(datalist)

N = length / span
for n in range(int(N)):
    # print(n)
    if pythonfile == "sta_log":
        datalist = [
            '\n',
            'Arguments = %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, color),
            'Queue'
            ]
        f.writelines(datalist)
    
    elif pythonfile == "sta_lin1":
        datalist = [
            '\n',
            'Arguments = %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, freq_min, freq_max, color),
            'Queue'
            ]
        f.writelines(datalist)
    
    elif pythonfile == "sta_lin2":
        datalist = [
            '\n',
            'Arguments = %s %s %s %s %s %s %s %s %s\n' %(ch, time_type, gps_beg, gps_end, fftl, freq_min, freq_mid, freq_max, color),
            'Queue'
            ]
        f.writelines(datalist)
    

    # print(gps_beg)
    # print(gps_end)
    gps_beg = gps_end
    gps_end = gps_beg + span

f.close()