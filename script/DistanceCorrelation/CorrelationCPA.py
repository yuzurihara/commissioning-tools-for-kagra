#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import matplotlib.pyplot as plt
import argparse
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
import matplotlib as mpl
# import dcor

# set the value for the plot globally
mpl.rcParams['xtick.labelsize'] = 18  # numbers of x-axis
mpl.rcParams['ytick.labelsize'] = 18  # numbers of y-axis
mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['axes.titlesize'] = 13
mpl.rcParams['legend.fontsize'] = 18
plt.rcParams['font.family'] = 'Times New Roman'
mpl.rcParams['axes.linewidth'] = 2
myfigsize1 = (12, 16)
myfigsize2 = (12, 8)
mydpisize = 500
chfinder_r = 0.05
chfinder_order = 1 # order must be 1 or more
chfinder_smooth = 7 # smooth must be 3 or more.
cf_peakL, cf_peakH = 10, None
cf_number = 100
cf_trialL, cf_trialM, cf_trialH = 10, 50, 90 

#################################################################################################
# setting all options
plotparser = argparse.ArgumentParser(
    prog = 'CorrelationCPA',
    description = 'Changing point analysis for distance correlation of ch1 and ch2'
    )

requiredNamed = plotparser.add_argument_group('required arguments')
requiredNamed.add_argument('-1','--ch1', required = True, type = str, help = '[str] Name of ch1.')
requiredNamed.add_argument('-2','--ch2', required = True, type = str, help = '[str] Name of ch2.')
requiredNamed.add_argument('-st','--starttime', required = True, type = int, help = '[int] GPS starttime of ch1 and ch2.')
optionalNamed = plotparser.add_argument_group('optional arguments')
optionalNamed.add_argument('-d','--duration', type = float, default = 3600, help = '[float] Total time of ch1 and ch2. unit: seconds. Default = 3600 s.')
optionalNamed.add_argument('-sh','--shortsection', type = float, default = 5, help = '[float] Section time to calculate every point of correlation. unit: seconds. Default = 5 s.')
optionalNamed.add_argument('-o','--overlap', type = float, default = 0.5, help = '[float] Overlap of each section of ch1 and ch2. unit: percentage of section length. Default = 0.5')
optionalNamed.add_argument('-x','--xscale', choices = ['log', 'linear'], default = 'linear', help = '[choice] Type of X scale. Default: linear.')
optionalNamed.add_argument('-c','--correlationscale', choices = ['log', 'linear'], default = 'linear', help = '[choice] Type of Y scale of correlation. Default: linear.')
optionalNamed.add_argument('-xmin','--xmin', type = float, help = '[float] Min of the X scale. unit: seconds. Default: 0.')
optionalNamed.add_argument('-xmax','--xmax', type = float, help = '[float] Max of the X scale. unit: seconds. Default: Total time of the channels.')
# optionalNamed.add_argument('-ymin','--ymin', type = float, help = '[float] Min of the Y scale of ch1 and ch2.')
# optionalNamed.add_argument('-ymax','--ymax', type = float, help = '[float] Max of the Y scale of ch1 and ch2.')
optionalNamed.add_argument('-cmin','--cmin', type = float, help = '[float] Min of the Y scale of correlation. Default: 0.01*min value for LogScale and -1 for LinearScale.')
optionalNamed.add_argument('-cmax','--cmax', type = float, help = '[float] Max of the Y scale of correlation. Default: 1')
optionalNamed.add_argument('-fig','--figname',  type = str, help = '[str] Name of figure. Default: CorrelationCPA_of_ch1_ch2.png')
# optionalNamed.add_argument('-csig','--coherencesignificance', action = 'store_true', help = 'Add significance, top 0.05, of conherence or not.')
args = plotparser.parse_args()

#################################################################################################
#################################################################################################
# functions required
def signaltoArray(timesignal, Ninterval, Nspacing):
    T = len(timesignal)
    Ntrial = int((T-(Ninterval-Nspacing))/Nspacing)
    signalarray = np.zeros((int(Ntrial), int(Ninterval)))
    for i in range(Ntrial):
        signalarray[i, :] = timesignal[int(Nspacing)*i: int(Nspacing)*i+int(Ninterval)]
    if Ntrial == 1:
        print('interval number = 1, only changes the dimension.')
        print('Originally, shape of signal =', timesignal.shape, 'and dim =', timesignal.ndim)
        print('Now, shape of signal =', signalarray.shape, 'and dim =', signalarray.ndim)
    return(signalarray)

def distance_correlation(signal1, dt1, signal2, dt2):
    T1, T2 = len(signal1[0]), len(signal2[0])
    N1, N2 = len(signal1), len(signal2)
    time1, time2 = np.arange(T1)*float(dt1), np.arange(T2)*float(dt2)
    time = np.intersect1d(time1, time2)
    T, N = len(time), min(N1, N2)
    # print('N1 =', N1, ', N2 =', N2, ', and N =', N )
    if T1 != T:
        subsig1 = np.zeros((N, T))
        t = 0
        for moment in range(T1):
            if time1[moment] in time:
                subsig1[:, t] = signal1[:N, moment]
                t += 1
        signal1 = subsig1
        print('>>> Since samplerate of ch1 and ch2 are different, only useful samples from signal1 are chosen!')
    if T2 != T:
        t = 0
        subsig2 = np.zeros((N, T))
        for moment in range(T2):
            if time2[moment] in time:
                subsig2[:, t] = signal2[:N, moment]
                t += 1
        signal2 = subsig2
        print('>>> Since samplerate of ch1 and ch2 are different, only useful samples from signal2 are chosen!')
    discorXY = np.zeros(N)
    for count in range(N): # every count is a time section
        discorXY[count] = dcor.distance_correlation(signal1[count], signal2[count])
    return discorXY

def linear_correlation(signal1, dt1, signal2, dt2):
    T1, T2 = len(signal1[0]), len(signal2[0])
    N1, N2 = len(signal1), len(signal2)
    time1, time2 = np.arange(T1)*float(dt1), np.arange(T2)*float(dt2)
    time = np.intersect1d(time1, time2)
    T, N= len(time), min(N1, N2)
    print('N1 =', N1, ', N2 =', N2, ', and N =', N )
    if T1 != T:
        subsig1 = np.zeros((N, T))
        t = 0
        for moment in range(T1):
            if time1[moment] in time:
                subsig1[:, t] = signal1[:N, moment]
                t += 1
        signal1 = subsig1
        print('>>> Since samplerate of ch1 and ch2 are different, only useful samples from signal1 are chosen!')
        # print('>>> signal1 = ', signal1)
    if T2 != T:
        t = 0
        subsig2 = np.zeros((N, T))
        for moment in range(T2):
            if time2[moment] in time:
                subsig2[:, t] = signal2[:N, moment]
                t += 1
        signal2 = subsig2
        print('>>> Since samplerate of ch1 and ch2 are different, only useful samples from signal2 are chosen!')
        # print('>>> signal2 = ', signal2)
    lincorXY = np.zeros(N)
    for count in range(N): # every count is a time section
        asig, bsig = signal1[count], signal2[count]
        S_xy_glitch = sum((asig - np.mean(asig)) * (bsig - np.mean(bsig)))
        S_x_glitch = np.sqrt(sum((asig - np.mean(asig)) ** 2))
        S_y_glitch = np.sqrt(sum((bsig - np.mean(bsig)) ** 2))
        lincorXY[count] = S_xy_glitch / (S_x_glitch * S_y_glitch)
    return lincorXY

#################################################################################################
# reading datas from options
channel1 = args.ch1
channel2 = args.ch2
starttime = args.starttime
totaltime = args.duration
timeinterval = args.shortsection
spacingT = timeinterval*(1-args.overlap)
gps_beg_head = int(starttime/100000)
gps_end_head = int((starttime+totaltime)/100000)
InputSignals = [channel1, channel2]
if args.figname == None:
    args.figname = 'CorrelationCPA_of_' + channel1 + '_and_' + channel2 + '.png'
# if '.png' not in str(args.figname):
#     args.figname = str(args.figname) + '.png'

for channels in range(2):
    if gps_beg_head == gps_end_head:
        cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    else:
        cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
        cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head
        cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)
        with open(cache_file, 'w') as outfile:
            for i in [cache1, cache2]:
                with open(i) as infile:
                    outfile.write(infile.read())
    InputSignals[channels] = TimeSeries.read(cache_file, InputSignals[channels], start = starttime, end = int(starttime+totaltime), nproc=4, verbose = False)

#################################################################################################
### data organization to required form for calculations
print('========== Finishing all reading signals! ==========')
ch1, ch2 = InputSignals[0], InputSignals[1]
print('Ch1 is', ch1, ',\n and Ch2 is', ch2)
print('Ch1 originally has', len(ch1),'points, and ch2 originally has', len(ch2), 'points.')
data1 = np.array(ch1)
data2 = np.array(ch2)
dt1 = float(str(ch1.dx).split('s')[0])
dt2 = float(str(ch2.dx).split('s')[0])
fs1, fs2 = 1/dt1, 1/dt2
print('Sample frequency of ch1 =', fs1, 'Hz, and sample frequency of ch2 =', fs2, 'Hz.')
ch1time = np.arange(len(data1))*dt1
ch2time = np.arange(len(data2))*dt2
intervalpoints1, spacingpoints1 = int(timeinterval/dt1), int(spacingT/dt1)
intervalpoints2, spacingpoints2 = int(timeinterval/dt2), int(spacingT/dt2)
print('Since time interval =', timeinterval,'s,')
print('in each interval, ch1 has', intervalpoints1,'points, and ch2 has', intervalpoints2, 'points.')
Adata1 = signaltoArray(data1, intervalpoints1, spacingpoints1)
Adata2 = signaltoArray(data2, intervalpoints2, spacingpoints2)
print('dataArray1 has', len(Adata1), 'intervals, with', len(Adata1[0]), 'elements in each interval.')
print('dataArray2 has', len(Adata2), 'intervals, with', len(Adata2[0]), 'elements in each interval.')
correlationtime = np.arange(0, totaltime-timeinterval+spacingT, spacingT)
print('Since time interval =', timeinterval,'s, and spacing =', spacingT, 's,')
print('time for calculating correlation = ', correlationtime, ', with', len(correlationtime), 'elements.')
print('Check: interval number should equal to the length of the correlation')

if len(Adata1) != len(Adata2):
    print('ERROR: The section number between two channels are not the same!')
    sys.exit(1)

################################################################################
# calculate distance correlation by definition
import dcor
print('Calculations of distance correlation starts!')
discor = distance_correlation(Adata1, dt1, Adata2, dt2)
print('Calculations of distance correlation ends!')
print('Calculations of linear correlation starts!')
lincor = linear_correlation(Adata1, dt1, Adata2, dt2)
print('Calculations of linear correlation ends!')

################################################################################
# # detection
print('Calculations of changefinder starts!')
import changefinder
from scipy.signal import find_peaks
# change finder for distance correlation
chfindresult = np.zeros((cf_number, len(discor)))
for i in range(cf_number):
    chfind = changefinder.ChangeFinder(r=chfinder_r, order=chfinder_order, smooth=chfinder_smooth)
    chfindsubresult = []
    for distancecor in discor:
        score1 = chfind.update(distancecor)
        chfindsubresult.append(score1)
    chfindresult[i] = chfindsubresult
# print(chfindresult)
chfindresultL = np.percentile(chfindresult, cf_trialL,axis = 0)
chfindresultM = np.percentile(chfindresult, cf_trialM,axis = 0)
chfindresultH = np.percentile(chfindresult, cf_trialH,axis = 0)
print('Calculations of changefinder ends!')
# # find peaks
Peakindices = find_peaks(chfindresultM, prominence=(cf_peakL, cf_peakH))
Peakindices = Peakindices[0]
# print(Peakindices, ', with length =', len(Peakindices))
############################################################################
# plots begins
# plot-1-1
fig = plt.figure(figsize = myfigsize1, dpi = mydpisize)
plt.subplot(3, 1, 1)
plt.plot(ch1time, data1)
plt.plot(ch2time, data2)
plt.ylabel("ch1 (" + str(ch1.unit) + ") and ch2 (" + str(ch2.unit) + ")")
pxmin, pxmax = 0, max(max(ch1time), max(ch2time))
if args.xmin != None: pxmin = args.xmin
if args.xmax != None: pxmin = args.xmax
plt.xlim([float(pxmin), float(pxmax)])
plt.xscale(args.xscale)
# pymin, pymax = 0.01*min(abs(ch1PSD)), 100*max(abs(ch1PSD))
# if args.ymin != None: pymin = args.ymin
# if args.ymax != None: pymax = args.ymax
# plt.ylim([float(pmin), float(pmax)])
# plt.yscale(args.yscale)
plt.legend([str(ch1.channel), str(ch2.channel)])
plt.grid(True)

# plot-1-2
ax1 = plt.subplot(3, 1, 2)
ax2 = ax1.twinx()
ax1.plot(correlationtime, lincor, '-', label = 'linear correlation')
ax1.plot(correlationtime, discor, '-', label = 'distance correlation')
# ax2.plot(correlationtime, chfindresultM, 'g-', label = 'change finder')
# ax1.set_xlabel("Time (s)")
ax1.set_ylabel('Correlation')
# ax2.set_ylabel('Results of change finder with '+str(cf_number)+' trials ('+str(cf_trialM)+'%)', color='g')
plt.xlim([pxmin, pxmax])
cmin, cmax = -1, 1
if args.correlationscale == 'log': rmin = 0.001*min(min(discor),min(lincor))
if args.cmin != None: cmin = args.cmin
if args.cmax != None: cmax = args.cmax
ax1.set_ylim([float(cmin), float(cmax)])
# ax2.set_ylim([0, 20])
plt.xscale(args.xscale); ax1.set_yscale(args.correlationscale)
ax1.legend(['linear correlation', 'distance correlation'])
# ax2.legend(['changing finder, r ='+str(chfinder_r)+', order ='+str(chfinder_order) + ', smooth = '+str(chfinder_smooth)])
# for ele in Peakindices:
#     ax2.plot(ele*spacingT, chfindresultM[ele], 'o', color = 'green')
plt.grid(True)
# modify textsize and file name
# fig.savefig(args.figname)

# plot-1-3, trails for changing point analysis of distance correlation
# fig, ax = plt.subplots(figsize = myfigsize2, dpi = mydpisize)
ax = plt.subplot(3, 1, 3)
ax.plot(correlationtime, chfindresultL)
ax.plot(correlationtime, chfindresultM)
ax.plot(correlationtime, chfindresultH)
plt.title('changing finder, r ='+str(chfinder_r)+', order ='+str(chfinder_order) + 
    ', smooth = '+str(chfinder_smooth) + ', with ' +str(cf_number) + ' trials.', fontsize = 18)
ax.legend([str(cf_trialL)+'%', str(cf_trialM)+'%', str(cf_trialH)+'%'])
for ele in Peakindices:
    # ax.plot(ele*spacingT, chfindresultM[ele], 'o', color = 'red')
    ax.axvspan(ele*spacingT-totaltime/100, ele*spacingT+totaltime/100, alpha=0.1, color='red') 
ax.set_xlabel("Time (s)")
ax.set_ylabel('Changing point results of distance correlation')
fig.savefig(args.figname)
print('========== Figure saved! No errors! ==========')
