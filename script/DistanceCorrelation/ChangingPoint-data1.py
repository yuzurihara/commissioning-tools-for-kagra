#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import matplotlib.pyplot as plt
import argparse
from gwpy.timeseries import TimeSeries, TimeSeriesDict, StateVector, StateVectorDict, TimeSeriesList
import matplotlib as mpl
# import dcor

# set the value for the plot globally
mpl.rcParams['xtick.labelsize'] = 18  # numbers of x-axis
mpl.rcParams['ytick.labelsize'] = 18  # numbers of y-axis
mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['axes.titlesize'] = 13
mpl.rcParams['legend.fontsize'] = 18
plt.rcParams['font.family'] = 'Times New Roman'
mpl.rcParams['axes.linewidth'] = 2
myfigsize1 = (12, 16)
myfigsize2 = (12, 8)
mydpisize = 500
# cf_peakL, cf_peakH = 10, None
# cf_trialL, cf_trialM, cf_trialH = 10, 50, 90

#################################################################################################
# setting all options
plotparser = argparse.ArgumentParser(
    prog = 'ChangingPoint-data1',
    description = 'Changing point analysis of 1 trial for orignal/ whiten/ second-trend data. Note (r, order, smooth) is better adjusted with the length of the data.'
    )

requiredNamed = plotparser.add_argument_group('required arguments')
requiredNamed.add_argument('-1','--ch1', required = True, type = str, help = '[str] Name of ch1.')
requiredNamed.add_argument('-st','--starttime', required = True, type = int, help = '[int] GPS starttime of ch1 and ch2.')
optionalNamed = plotparser.add_argument_group('optional arguments')
optionalNamed.add_argument('-d','--duration', type = float, default = 3600, help = '[float] Total time of ch1 and ch2. unit: seconds. Default = 3600 s.')
# optionalNamed.add_argument('-sh','--shortsection', type = float, default = 5, help = '[float] Section time to calculate every point of correlation. unit: seconds. Default = 5 s.')
# optionalNamed.add_argument('-o','--overlap', type = float, default = 0.5, help = '[float] Overlap of each section of ch1 and ch2. unit: percentage of section length. Default = 0.5')
optionalNamed.add_argument('-ros','--rordersmooth',  type = str, default = '0.1,1,100', help = '[str] "r,order,smooth" for changefinder. Please use comma for separation and avoid spaces in command lines. Note that order must be an positive integer and that smooth must be an integer bigger than order and 3.')
optionalNamed.add_argument('-wh','--whiten', action = 'store_true', help = 'Whiten the data or not.')
optionalNamed.add_argument('-2trd','--secondtrend', action = 'store_true', help = 'Use the second trend data (average with sampling frequency 1 Hz) or not.')
optionalNamed.add_argument('-top','--toppeak', type = int, default = 3, help = '[int] The positive number of maximun peaks wished to be marked. Default = 3 peaks.')
optionalNamed.add_argument('-szero','--setzero', type = float, default = 0.1, help = '[float] The timelength wished to be neglected from the begining. unit: percentage of the total time length. Default = 0.1')
optionalNamed.add_argument('-x','--xscale', choices = ['log', 'linear'], default = 'linear', help = '[choice] Type of X scale. Default: linear.')
optionalNamed.add_argument('-xmin','--xmin', type = float, help = '[float] Min of the X scale. unit: seconds. Default: 0.')
optionalNamed.add_argument('-xmax','--xmax', type = float, help = '[float] Max of the X scale. unit: seconds. Default: Total time of the channels.')
# optionalNamed.add_argument('-ymin','--ymin', type = float, help = '[float] Min of the Y scale of ch1 and ch2.')
# optionalNamed.add_argument('-ymax','--ymax', type = float, help = '[float] Max of the Y scale of ch1 and ch2.')
optionalNamed.add_argument('-fig','--figname',  type = str, help = '[str] Name of figure. Default: ChangePoint_Data(Origin/Whiten)_ch1.png')
args = plotparser.parse_args()

#################################################################################################
# reading datas from options
ros = args.rordersmooth.split(',')
if len(ros) != 3:
    sys.exit('Form of the input parameters is not correct. Input r,order,smooth. Please use comma for separation and avoid spaces.') 
elif float(ros[0]) < 0 and float(ros[0])> 1:
    sys.exit('setting r is not correct. 0 < r < 1')
elif int(ros[1]) < 1 :
    sys.exit('setting order is not correct. Order must be positive integer.')
elif int(ros[2]) < 3 or int(ros[2]) < int(ros[1]):
    sys.exit('setting smooth is not correct. Smooth must be positive integer larger than order and 3.')
chfinder_r = float(ros[0])
chfinder_order = int(ros[1]) # order must be 1 or more
chfinder_smooth = int(ros[2]) # smooth must be 3 or more.
print('(r = '+str(chfinder_r)+', order = '+str(chfinder_order)+', smooth = '+str(chfinder_smooth)+')')
topNpeak = int(args.toppeak)
set0 = float(args.setzero)
channel1 = args.ch1
starttime = args.starttime
totaltime = args.duration
# timeinterval = args.shortsection
# spacingT = timeinterval*(1-args.overlap)
gps_beg_head = int(starttime/100000)
gps_end_head = int((starttime+totaltime)/100000)
# InputSignals = [channel1]
if args.figname == None:
    if args.whiten == True:
        args.figname = 'ChangePoint_Data(Whiten)_' + channel1 + '.png'
    else:
        args.figname = 'ChangePoint_Data(Origin)_' + channel1 + '.png'

if args.secondtrend == True:
    if gps_beg_head == gps_end_head:
        cache_file="/home/detchar/cache/CacheSecond_GPS/%s.ffl" % gps_beg_head
    else:
        cache1="/home/detchar/cache/CacheSecond_GPS/%s.ffl" % gps_beg_head
        cache2="/home/detchar/cache/CacheSecond_GPS/%s.ffl" % gps_end_head
        cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)
        with open(cache_file, 'w') as outfile:
            for i in [cache1, cache2]:
                with open(i) as infile:
                    outfile.write(infile.read())
else:
    if gps_beg_head == gps_end_head:
        cache_file="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
    else:
        cache1="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_beg_head
        cache2="/home/detchar/cache/Cache_GPS/%s.ffl" % gps_end_head
        cache_file="/tmp/%s_%s.ffl" % (gps_beg, gps_end)
        with open(cache_file, 'w') as outfile:
            for i in [cache1, cache2]:
                with open(i) as infile:
                    outfile.write(infile.read())
InputSignals = TimeSeries.read(cache_file, channel1, start = starttime, end = int(starttime+totaltime), nproc=4, verbose = False)
    
#################################################################################################
### data organization to required form for calculations
print('========== Finishing all reading signals! ==========')
ch1 = InputSignals
if args.whiten == True :
    data1 = np.array(ch1.whiten(4,2))
else: 
    data1 = np.array(ch1) 
print('Ch1 is', ch1, ',')
print('Ch1 originally has', len(ch1),'points.')
dt1 = float(str(ch1.dx).split('s')[0])
fs1 = 1/dt1
print('Sample frequency of ch1 =', fs1, 'Hz.')
ch1time = np.arange(len(data1))*dt1
# down sampling
# fs1s = fs1 # change to down sample frequency
# data1s = np.transpose(data1.reshape((int(totaltime*fs1s), int(fs1/fs1s))))
# data1s = data1s[0] # choose the first point in a second.
# ch1stime = np.arange(len(data1s))*(1/fs1s)
# dt1s = ch1stime[1]
# print('Sample frequency of data =', fs1s, 'Hz.')
# print('After down sampling, data has', len(data1s),'points.')
# down sampling finished

################################################################################
# # detection
print('Calculations of changefinder starts!')
import changefinder
from scipy.signal import find_peaks

# change finder for original data
chfindresult1 = np.zeros(len(data1)) # chfindresult1 = np.zeros(len(data1s))
chfindresult1_0 = np.zeros(len(data1))
chfind = changefinder.ChangeFinder(r=chfinder_r, order=chfinder_order, smooth=chfinder_smooth)
for j, data in enumerate(data1): # for j, data in enumerate(data1s):
    chfindresult1[j] = chfind.update(data)
    chfindresult1_0[j] = chfind.update(data)
# find peaks
# chfindresult1_0 = chfindresult1.copy()
for i in range(int(set0*totaltime*fs1)):
    chfindresult1_0[i] = 0
    # print(chfindresult1_0[i], chfindresult1[i])
Peakindices = find_peaks(chfindresult1_0)
Peakindices = Peakindices[0]
peakvalues = [chfindresult1[i] for i in Peakindices]
if len(peakvalues) < topNpeak:
    PeakindicesN = chfindresult1_0.argsort()[-len(peakvalues):][::-1]
else:
    PeakindicesN = chfindresult1_0.argsort()[-topNpeak:][::-1]
############################################################################
# plots begins
# plot-1, trails for changing point analysis of original data
fig = plt.figure(figsize = myfigsize1, dpi = mydpisize)
plt.subplot(2, 1, 1)
if args.whiten == True:
    plt.title(str(channel1) + ' (Whiten), GPS starttime = ' + str(starttime), fontsize = 18)
else: 
    plt.title(str(channel1) + ', GPS starttime = ' + str(starttime), fontsize = 18)
plt.plot(ch1time, data1)
# plt.plot(ch1stime, data1s)
# plt.legend(['origin data ('+str(fs1)+' Hz)', 'down sampling ('+str(fs1s)+' Hz)'])
ax = plt.subplot(2, 1, 2)
ax.plot(ch1time, chfindresult1)
# ax.set_ylim([0, 100])
for ele in PeakindicesN:
    # ax.plot(ele*spacingT, chfindresultM[ele], 'o', color = 'red')
    ax.axvspan(ele*dt1-totaltime/100, ele*dt1+totaltime/100, alpha=0.05, color='red') 
plt.title('changing finder, r ='+str(chfinder_r)+', order ='+str(chfinder_order) + 
   ', smooth = '+str(chfinder_smooth), fontsize = 18)
# ax.legend([str(cf_trialL)+'%', str(cf_trialM)+'%', str(cf_trialH)+'%'])
ax.set_xlabel("Time (s)")
fig.savefig(args.figname)


# fig = plt.figure(figsize = myfigsize1, dpi = mydpisize)
# plt.subplot(4, 1, 1)
# if args.whiten == True:
#     plt.title(str(channel1) + ' (Whiten), GPS starttime = ' + str(starttime), fontsize = 18)
# else: 
#     plt.title(str(channel1) + ', GPS starttime = ' + str(starttime), fontsize = 18)
# plt.plot(ch1time, data1)
# ax = plt.subplot(4, 1, 2)
# ax.plot(ch1time, chfindresult1L)
# ax.legend([str(cf_trialL)+'%'])
# plt.title('changing finder, r ='+str(chfinder_r)+', order ='+str(chfinder_order) + 
#    ', smooth = '+str(chfinder_smooth) + ', with ' +str(cf_number) + ' trials.', fontsize = 18)
# ax = plt.subplot(4, 1, 3)
# ax.plot(ch1time, chfindresult1M)
# ax.legend([str(cf_trialM)+'%'])
# ax = plt.subplot(4, 1, 4)
# ax.plot(ch1time, chfindresult1H)
# ax.legend([str(cf_trialH)+'%'])
# # ax.set_ylim([0, 100])
# # for ele in PeakindicesN:
# #     # ax.plot(ele*spacingT, chfindresultM[ele], 'o', color = 'red')
# #     ax.axvspan(ele*dt1-totaltime/100, ele*dt1+totaltime/100, alpha=0.05, color='red') 
# # plt.title('changing finder, r ='+str(chfinder_r)+', order ='+str(chfinder_order) + 
# #    ', smooth = '+str(chfinder_smooth) + ', with ' +str(cf_number) + ' trials.', fontsize = 18)
# # ax.legend([str(cf_trialL)+'%', str(cf_trialM)+'%', str(cf_trialH)+'%'])
# ax.set_xlabel("Time (s)")
# fig.savefig('test1_'+args.figname)

# fig = plt.figure(figsize = myfigsize1, dpi = mydpisize)
# plt.subplot(2, 1, 1)
# if args.whiten == True:
#     plt.title(str(channel1) + ' (Whiten), GPS starttime = ' + str(starttime), fontsize = 18)
# else: 
#     plt.title(str(channel1) + ', GPS starttime = ' + str(starttime), fontsize = 18)
# plt.plot(ch1time, data1)
# ax = plt.subplot(2, 1, 2)
# ax.plot(ch1time, chfindresult1H)
# ax.plot(ch1time, chfindresult1M)
# ax.plot(ch1time, chfindresult1L)
# # ax.set_ylim([0, 100])
# # for ele in PeakindicesN:
#     # ax.plot(ele*spacingT, chfindresultM[ele], 'o', color = 'red')
#     # ax.axvspan(ele*dt1-totaltime/100, ele*dt1+totaltime/100, alpha=0.05, color='red') 
# plt.title('changing finder, r ='+str(chfinder_r)+', order ='+str(chfinder_order) + 
#    ', smooth = '+str(chfinder_smooth) + ', with ' +str(cf_number) + ' trials.', fontsize = 18)
# ax.legend([str(cf_trialH)+'%', str(cf_trialM)+'%', str(cf_trialL)+'%'])
# ax.set_xlabel("Time (s)")
# fig.savefig('test2_'+args.figname)

print('========== Figure saved! No errors! ==========')

