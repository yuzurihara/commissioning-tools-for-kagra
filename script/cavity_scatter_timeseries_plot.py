#!/usr/bin/env python
# coding: utf-8
"""
This is IRX/IRY and IX/IY/EX/EY channels scatter and timeseries plot code
Making by Mayu Murakoshi
"""
from gwpy.timeseries import TimeSeries, TimeSeriesDict
from gwpy.plot import Plot
from matplotlib import pyplot as plt
from gwpy.time import from_gps, to_gps
import numpy as np
import sys
import os
import warnings
warnings.filterwarnings('ignore')

# getting channel
if __name__ == "__main__":
    print(sys.argv)
    print(len(sys.argv))
    print(type(sys.argv))

print("---------------------------------------")
del sys.argv[0]
print("IR channel : %s " % sys.argv[0])

IR = sys.argv[0]
IR_tytle = IR.replace('K1:', '')

IX_p = "K1:VIS-ITMX_TM_WIT_P_DQ"
IX_y = "K1:VIS-ITMX_TM_WIT_Y_DQ"
EX_p = "K1:VIS-ETMX_TM_WIT_P_DQ"
EX_y = "K1:VIS-ETMX_TM_WIT_Y_DQ"
IY_p = "K1:VIS-ITMY_TM_WIT_P_DQ"
IY_y = "K1:VIS-ITMY_TM_WIT_Y_DQ"
EY_p = "K1:VIS-ETMY_TM_WIT_P_DQ"
EY_y = "K1:VIS-ETMY_TM_WIT_Y_DQ"

chs = [IR, IX_p, IX_y, EX_p, EX_y, IY_p, IY_y, EY_p, EY_y]


# getting time
time_type = sys.argv[1]

if time_type == "JST":
    jst_ymd = sys.argv[2]
    jst_hms = sys.argv[3]
    span = int(sys.argv[4])

    jst_ymd_title = jst_ymd.replace('/', '-')
    jst_hms_title = jst_hms.replace(':', '')

    jst = jst_ymd + " " + jst_hms + " " + time_type
    jst_title = jst_ymd_title + "-" + jst_hms_title
    gps = int(to_gps( jst ))

elif time_type == "GPS":
    time = sys.argv[2]
    span = int(sys.argv[3])
    # time = 1366350533   # 2023/4/24 14:48:35 JST ##################################### memo
    # time = 1364347848   # 2023/4/1 1:30:30 UTC +-30s
    # time = 1364350848   # 2023/4/1 2:20:30 UTC +-30s    x=5が下がってる
    # time = 1366483898   # 2023/4/25 18:51:20 UTCが下がってる
    # time = 1366480343   # 2023/4/25 17:52:05 UTCが下がってる   x=32が下がってる
    gps = int(time)

halfspan = int(span/2)


# conversion of time
gps_beg = gps - halfspan    # assume the glitch is for ten second 
gps_end = gps + halfspan
# utc_beg = from_gps( gps_beg )
# utc_end = from_gps( gps_end )
gps_beg_head = int(gps_beg/100000)
gps_end_head = int(gps_end/100000)
# gps_beg_plus = gps_beg + 32400    # add nine hours to the glitch GPS Time to convert to JST
# gps_end_plus = gps_end + 32400    # add nine hours to the glitch GPS Time to convert to JST
# jst_plus = from_gps( gps_beg_plus )   # convert the beginning of the glitch GPS Time to JST
# jst_end_plus = from_gps( gps_end_plus )   # convert the end of the glitch GPS Time to JST


# saving png file
if time_type == "JST":  
    print("time（JST）: %s +- %s s" % (jst, halfspan))
    dataname1 = "cavity_scatter_img_%s_[%s%s+-%ss].png" % (IR_tytle, jst_title, time_type, halfspan)
    dataname2 = "oplev_scatter_img_%s_[%s%s+-%ss].png" % (IR_tytle, jst_title, time_type, halfspan)
    dataname3 = "oplev_timeseries_img_%s_[%s%s+-%ss].png" % (IR_tytle, jst_title, time_type, halfspan)
    dataname4 = "cavity_timeseries_img_%s_[%s%s+-%ss].png" % (IR_tytle, jst_title, time_type, halfspan)
    
elif time_type == "GPS":
    print("time（GPS Time）: %s" % gps)
    dataname1 = "cavity_scatter_img_%s_[%s%s+-%ss].png" % (IR_tytle, gps, time_type, halfspan)
    dataname2 = "oplev_scatter_img_%s_[%s%s+-%ss].png" % (IR_tytle, gps, time_type, halfspan)
    dataname3 = "oplev_timeseries_img_%s_[%s%s+-%ss].png" % (IR_tytle, gps, time_type, halfspan)
    dataname4 = "cavity_timeseries_img_%s_[%s%s+-%ss].png" % (IR_tytle, gps, time_type, halfspan)

dirname = "cavity_scatter_timeseries_plot_image_for%ss/" % (span)

os.makedirs(dirname, exist_ok=True)
filename1 = dirname + dataname1
filename2 = dirname + dataname2
filename3 = dirname + dataname3
filename4 = dirname + dataname4
print()
print("output file1 : %s" % filename1)
print("output file2 : %s" % filename2)
print("output file3 : %s" % filename3)
print("output file4 : %s" % filename4)
# #============================================================

# getting data 
i = 0
if gps_beg_head == gps_end_head:
    cache_file="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
else:
    cache1="/home/detchar/cache/Cache_GPS/%s.cache" % gps_beg_head
    cache2="/home/detchar/cache/Cache_GPS/%s.cache" % gps_end_head 
    cache_file="/tmp/%s_%s.cache" % (gps_beg, gps_end)

    with open(cache_file, 'w') as outfile:
        for i in [cache1, cache2]:
            with open(i) as infile:
                outfile.write(infile.read()) 
# data = TimeSeriesDict.read(cache_file, chs, start=gps_beg, end=gps_end, verbose=True)
data = TimeSeriesDict.read(cache_file, chs, start=gps_beg, end=gps_end, nproc=16, verbose=True)


# make same data size
print()
print("<change sample rate>")
for ch in chs:
    print(ch)
    if data[IR].sample_rate.value != data[ch].sample_rate.value:
        print(data[ch].sample_rate, "-->")
        data[ch] = data[ch].resample(data[IR].sample_rate)
    print(data[ch].sample_rate)


# cavity mode
DHARD_p = (data[IX_p]-np.mean(data[IX_p])) - (data[EX_p]-np.mean(data[EX_p])) - (data[IY_p]-np.mean(data[IY_p])) + (data[EY_p]-np.mean(data[EY_p]))
CHARD_p = (data[IX_p]-np.mean(data[IX_p])) - (data[EX_p]-np.mean(data[EX_p])) - (data[IY_p]-np.mean(data[IY_p])) - (data[EY_p]-np.mean(data[EY_p]))
DSOFT_p = (data[IX_p]-np.mean(data[IX_p])) + (data[EX_p]-np.mean(data[EX_p])) - (data[IY_p]-np.mean(data[IY_p])) - (data[EY_p]-np.mean(data[EY_p]))
CSOFT_p = (data[IX_p]-np.mean(data[IX_p])) + (data[EX_p]-np.mean(data[EX_p])) + (data[IY_p]-np.mean(data[IY_p])) + (data[EY_p]-np.mean(data[EY_p]))
DHARD_y = (data[IX_y]-np.mean(data[IX_y])) + (data[EX_y]-np.mean(data[EX_y])) + (data[IY_y]-np.mean(data[IY_y])) + (data[EY_y]-np.mean(data[EY_y]))
CHARD_y = (data[IX_y]-np.mean(data[IX_y])) + (data[EX_y]-np.mean(data[EX_y])) - (data[IY_y]-np.mean(data[IY_y])) - (data[EY_y]-np.mean(data[EY_y]))
DSOFT_y = (data[IX_y]-np.mean(data[IX_y])) - (data[EX_y]-np.mean(data[EX_y])) + (data[IY_y]-np.mean(data[IY_y])) - (data[EY_y]-np.mean(data[EY_y]))
CSOFT_y = (data[IX_y]-np.mean(data[IX_y])) - (data[EX_y]-np.mean(data[EX_y])) - (data[IY_y]-np.mean(data[IY_y])) + (data[EY_y]-np.mean(data[EY_y]))

print()
print("data[IR]", data[IR])
print("DHARD_p", DHARD_p)

print("----------------------------------------------------------------------")
print("<data>")
print("cache_file = %s" % cache_file)
print("gps_beg = %s" % gps_beg)
print("gps_end = %s" % gps_end)
print(data)
print("======================================================================")
# #============================================================
# #============================================================

################ out put ################
# ---------------------------------------
# scatter 1
fig = plt.figure(figsize = (20,10))

ax1 = fig.add_subplot(2, 4, 1)
ax2 = fig.add_subplot(2, 4, 2)
ax3 = fig.add_subplot(2, 4, 3)
ax4 = fig.add_subplot(2, 4, 4)
ax5 = fig.add_subplot(2, 4, 5)
ax6 = fig.add_subplot(2, 4, 6)
ax7 = fig.add_subplot(2, 4, 7)
ax8 = fig.add_subplot(2, 4, 8)

ax1.scatter(data[IR], DHARD_p, s=5, color="lightblue")
ax1.set_xlabel("%s" % IR)
ax1.set_ylabel("DHARD_p")
ax2.scatter(data[IR], CHARD_p, s=5, color="salmon")
ax2.set_xlabel("%s" % IR)
ax2.set_ylabel("CHARD_p")
ax3.scatter(data[IR], DSOFT_p, s=5, color="lightgreen")
ax3.set_xlabel("%s" % IR)
ax3.set_ylabel("DSOFT_p")
ax4.scatter(data[IR], CSOFT_p, s=5, color="y")
ax4.set_xlabel("%s" % IR)
ax4.set_ylabel("CSOFT_p")
ax5.scatter(data[IR], DHARD_y, s=5, color="m")
ax5.set_xlabel("%s" % IR)
ax5.set_ylabel("DHARD_y")
ax6.scatter(data[IR], CHARD_y, s=5, color="c")
ax6.set_xlabel("%s" % IR)
ax6.set_ylabel("CHARD_y")
ax7.scatter(data[IR], DSOFT_y, s=5, color="purple")
ax7.set_xlabel("%s" % IR)
ax7.set_ylabel("DSOFT_y")
ax8.scatter(data[IR], CSOFT_y, s=5, color="brown")
ax8.set_xlabel("%s" % IR)
ax8.set_ylabel("CSOFT_y")

if time_type == "JST":  
    fig.suptitle("%s \n %s +-%s sec" % (IR, jst, halfspan), fontsize=25)
elif time_type == "GPS":
    fig.suptitle("%s \n %s GPS +-%s sec" % (IR, gps, halfspan), fontsize=25)

fig.subplots_adjust(wspace=0.4, hspace=0.4)
fig.savefig(filename1)

# ---------------------------------------
# scatter 2
fig = plt.figure(figsize = (20,10))

ax1 = fig.add_subplot(2, 4, 1)
ax2 = fig.add_subplot(2, 4, 2)
ax3 = fig.add_subplot(2, 4, 3)
ax4 = fig.add_subplot(2, 4, 4)
ax5 = fig.add_subplot(2, 4, 5)
ax6 = fig.add_subplot(2, 4, 6)
ax7 = fig.add_subplot(2, 4, 7)
ax8 = fig.add_subplot(2, 4, 8)

ax1.scatter(data[IR], data[IX_p], s=5, color="lightblue")
ax1.set_xlabel("%s" % IR)
ax1.set_ylabel(IX_p)
ax2.scatter(data[IR], data[IX_y], s=5, color="salmon")
ax2.set_xlabel("%s" % IR)
ax2.set_ylabel(IX_y)
ax3.scatter(data[IR], data[EX_p], s=5, color="lightgreen")
ax3.set_xlabel("%s" % IR)
ax3.set_ylabel(EX_p)
ax4.scatter(data[IR], data[EX_y], s=5, color="y")
ax4.set_xlabel("%s" % IR)
ax4.set_ylabel(EX_y)
ax5.scatter(data[IR], data[IY_p], s=5, color="m")
ax5.set_xlabel("%s" % IR)
ax5.set_ylabel(IY_p)
ax6.scatter(data[IR], data[IY_y], s=5, color="c")
ax6.set_xlabel("%s" % IR)
ax6.set_ylabel(IY_y)
ax7.scatter(data[IR], data[EY_p], s=5, color="purple")
ax7.set_xlabel("%s" % IR)
ax7.set_ylabel(EY_p)
ax8.scatter(data[IR], data[EY_y], s=5, color="brown")
ax8.set_xlabel("%s" % IR)
ax8.set_ylabel(EY_y)

if time_type == "JST":  
    fig.suptitle("%s \n %s +-%s sec" % (IR, jst, halfspan), fontsize=25)
elif time_type == "GPS":
    fig.suptitle("%s \n %s GPS +-%s sec" % (IR, gps, halfspan), fontsize=25)

fig.subplots_adjust(wspace=0.4, hspace=0.4)
fig.savefig(filename2)

# -------------------------------
# # time series 1
fig = plt.figure(figsize = (10,16))

ax1 = fig.add_subplot(9, 1, 1)
ax2 = fig.add_subplot(9, 1, 2)
ax3 = fig.add_subplot(9, 1, 3)
ax4 = fig.add_subplot(9, 1, 4)
ax5 = fig.add_subplot(9, 1, 5)
ax6 = fig.add_subplot(9, 1, 6)
ax7 = fig.add_subplot(9, 1, 7)
ax8 = fig.add_subplot(9, 1, 8)
ax9 = fig.add_subplot(9, 1, 9)

ax1.plot(data[IR].times-data[IR].t0, data[IR], label='%s' % IR, color='orange')
ax2.plot(data[IX_p].times-data[IX_p].t0, data[IX_p], label='%s' % IX_p, color='b')
ax3.plot(data[IX_y].times-data[IX_y].t0, data[IX_y], label='%s' % IX_y, color='r')
ax4.plot(data[EX_p].times-data[EX_p].t0, data[EX_p], label='%s' % EX_p, color='g')
ax5.plot(data[EX_y].times-data[EX_y].t0, data[EX_y], label='%s' % EX_y, color='y')
ax6.plot(data[IY_p].times-data[IY_p].t0, data[IY_p], label='%s' % IY_p, color='m')
ax7.plot(data[IY_y].times-data[IY_y].t0, data[IY_y], label='%s' % IY_y, color='c')
ax8.plot(data[EY_p].times-data[EY_p].t0, data[EY_p], label='%s' % EY_p, color='purple')
ax9.plot(data[EY_y].times-data[EY_y].t0, data[EY_y], label='%s' % EY_y, color='brown')

ax9.set_xlabel("Time [s] (t0 = %s GPS)" %(gps_beg), fontsize=20)

ax1.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax2.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax3.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax4.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax5.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax6.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax7.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax8.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax9.legend(loc='lower right', bbox_to_anchor=(1, 1))

ax1.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax2.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax3.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax4.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax5.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax6.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax7.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax8.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax9.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")

plt.subplots_adjust(hspace=1.3)
fig.savefig(filename3)

# ---------------------------------------
# # time series 2
fig = plt.figure(figsize = (10,16))

ax1 = fig.add_subplot(9, 1, 1)
ax2 = fig.add_subplot(9, 1, 2)
ax3 = fig.add_subplot(9, 1, 3)
ax4 = fig.add_subplot(9, 1, 4)
ax5 = fig.add_subplot(9, 1, 5)
ax6 = fig.add_subplot(9, 1, 6)
ax7 = fig.add_subplot(9, 1, 7)
ax8 = fig.add_subplot(9, 1, 8)
ax9 = fig.add_subplot(9, 1, 9)

ax1.plot(data[IR].times-data[IR].t0, data[IR], label='%s' % IR, color='orange')
ax2.plot(data[IX_p].times-data[IX_p].t0, DHARD_p, label='DHARD_p', color='b')
ax3.plot(data[IX_y].times-data[IX_y].t0, CHARD_p, label='CHARD_p', color='r')
ax4.plot(data[EX_p].times-data[EX_p].t0, DSOFT_p, label='DSOFT_p', color='g')
ax5.plot(data[EX_y].times-data[EX_y].t0, CSOFT_p, label='CSOFT_p', color='y')
ax6.plot(data[IY_p].times-data[IY_p].t0, DHARD_y, label='DHARD_y', color='m')
ax7.plot(data[IY_y].times-data[IY_y].t0, CHARD_y, label='CHARD_y', color='c')
ax8.plot(data[EY_p].times-data[EY_p].t0, DSOFT_y, label='DSOFT_y', color='purple')
ax9.plot(data[EY_y].times-data[EY_y].t0, CSOFT_y, label='CSOFT_y', color='brown')

ax9.set_xlabel("Time [s] (t0 = %s GPS)" %(gps_beg), fontsize=20)

ax1.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax2.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax3.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax4.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax5.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax6.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax7.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax8.legend(loc='lower right', bbox_to_anchor=(1, 1))
ax9.legend(loc='lower right', bbox_to_anchor=(1, 1))

ax1.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax2.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax3.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax4.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax5.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax6.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax7.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax8.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")
ax9.axvline(x=gps-data[IR].t0.value, linewidth=3, color="black", linestyle = ":")

plt.subplots_adjust(hspace=1.3)
fig.savefig(filename4)

# #######################################

plt.show()
