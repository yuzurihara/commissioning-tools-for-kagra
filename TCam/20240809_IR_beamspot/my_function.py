import numpy as np
import pandas as pd
import cv2
import os
import shutil
from scipy.optimize import curve_fit

# create new directory
def renew_dir(dirname):
    if os.path.isdir(dirname):
        print("old output files are moved to", dirname[:-1]+"_old/ and refresh",dirname )
        if not os.path.exists(dirname[:-1]+"_old/"): os.mkdir(dirname[:-1]+"_old/")# MAkE SAVEDIR
        shutil.rmtree(dirname[:-1]+"_old/")
        os.rename(dirname, dirname[:-1]+"_old/")
    else:
        print("make ", dirname)
    os.mkdir(dirname)
    
# Gaussian (clipped)
def ClippedGaussian(x, AMP, WID, CNT, BGD):
    y = AMP * np.exp(-(x-CNT)*(x-CNT)/(2*WID*WID)) + BGD
    y = np.clip(y, None, 255)
    return y

def CircleMask(img, crow, ccol, radius):
    # マスクをかける
    mask = np.zeros_like(img)
    cv2.circle(mask, (crow, ccol), radius, (255,255,255), thickness=-1)
    masked_img = cv2.bitwise_and(img, mask)
    masked_img[masked_img < 1] = False
    return masked_img

def get_fitting_data(img, img_ftr, ax_slice, x_slice):
    if   ax_slice == 0:
        ydata     =     img[int(x_slice),:]
        ydata_ftr = img_ftr[int(x_slice),:]
    elif ax_slice == 1:
        ydata     =     img[:,int(x_slice)]
        ydata_ftr = img_ftr[:,int(x_slice)]
    xdata = np.arange(len(ydata))
    index = np.ones(len(ydata), dtype=bool)
    index[ydata > ydata_ftr] = False                     # ydata > ydata_ftr の部分のデータを落とす
    return xdata, ydata, index
 
def get_peak(xdata_fit, ydata_fit):
    max_idx  = xdata_fit[np.argmax(ydata_fit)]
    ave_idx1 = xdata_fit[np.where( ydata_fit > np.average(ydata_fit))][0]
    ave_idx2 = xdata_fit[np.where( ydata_fit > np.average(ydata_fit))][-1]
    
    #フィットする範囲
    CNT0 = max_idx
    WID0 = ave_idx2-ave_idx1
    AMP0 = np.max(ydata_fit)
    BGD0 = np.average(ydata_fit)

    #初期値を決める#####################################
    #中心の初期値を求めるために配列のmaxカウントの位置を抜き出す
#    print('fit range:(min, max, initial center) = ({:d}, {:d}, {:d})'.format(ave_idx1, ave_idx2, max_idx))

    p0 = [AMP0, WID0, CNT0, BGD0] #初期値
    bounds=([AMP0/10., WID0/10., ave_idx1, 0], [AMP0*10, WID0*2, ave_idx2, BGD0*2]) # パラメーターの下限値と上限値
    popt, pcov = curve_fit(ClippedGaussian, xdata_fit, ydata_fit, p0=p0, bounds=bounds)
    return popt, pcov

def get_average(fit_results):
    FIT_POS = [r[0] for r in fit_results]
    FIT_CTR = [r[5] for r in fit_results]
    ERR_CTR = [r[6] for r in fit_results]
 
    df2 = pd.DataFrame(list(zip(FIT_POS, FIT_CTR)), columns = ['X_LINE','FIT_CTR'])
    para3 = 1.2
    
    # # # 四分位数、四分位範囲
    q1  = df2['FIT_CTR'].quantile(.25)
    q3  = df2['FIT_CTR'].quantile(.75)
    iqr = q3 - q1
    limit_low  = q1 - iqr * para3
    limit_high = q3 + iqr * para3
    df3 = df2.query('@limit_low < FIT_CTR < @limit_high')
    ave = np.average(df3['FIT_CTR'])
    var = np.sqrt(np.var(df3['FIT_CTR'])) 
    med = np.median(df3['FIT_CTR'])
    return ave, med, var

def twoD_Gaussian(XY, amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
  x,y =  XY[0:2]
  xo  =  float(xo)
  yo  =  float(yo)
  a   =  (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
  b   = -(np.sin(2*theta)) /(4*sigma_x**2) + (np.sin(2*theta)) /(4*sigma_y**2)
  c   =  (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
  g   =  offset + amplitude*np.exp(-(a*((x-xo)**2)+2*b*(x-xo)*(y-yo)+c*((y-yo)**2)))
  return g.ravel()

def twoD_HigPass(img):
  f = np.fft.fft2(img)                        # 2Dフーリエ変換
  f_shift = np.fft.fftshift(f)                # 直流成分を画像中心に移動させるためN/2シフトさせる
  mag = 20 * np.log(np.abs(f_shift))          # 振幅成分を計算
   
  # 周波数領域にマスクをかける
  rows, cols = img.shape                      # 画像サイズを取得
  crow, ccol = int(rows / 2), int(cols / 2)   # 画像中心を計算
  mask  = 50                                  # マスクのサイズ
  f_shift[crow-mask:crow+mask, ccol-mask:ccol+mask] = 0 # high-pass
   
  # 2D逆フーリエ変換によりフィルタリング後の画像を得る
  f_ishift = np.fft.ifftshift(f_shift)         # シフト分を元に戻す
  img_back = np.fft.ifft2(f_ishift)            # 逆フーリエ変換
  img_back = np.abs(img_back)                  # 実部を計算する
  return img_back

def twoD_LowPass(img,n_mask):
  f = np.fft.fft2(img)                         # 2Dフーリエ変換
  f_shift = np.fft.fftshift(f)                 # 直流成分を画像中心に移動させるためN/2シフトさせる
  mag = 20 * np.log(np.abs(f_shift))           # 振幅成分を計算
   
#   # 周波数領域にマスクをかける
#   rows, cols = img.shape                       # 画像サイズを取得
#   crow, ccol = int(rows / 2), int(cols / 2)    # 画像中心を計算
#   rmask = 10                                   # マスクのサイズ
#   cmask = 10                                   # マスクのサイズ 
# #  f_shift[crow:ccol]                       = 0 # 直流成分を落とす
#   f_shift[0:crow-cmask,0:ccol-rmask]       = 0 # 高周波成分を落とす
#   f_shift[0:crow-cmask,ccol+rmask:cols]    = 0 # 高周波成分を落とす
#   f_shift[crow+cmask:rows,0:ccol-rmask]    = 0 # 高周波成分を落とす
#   f_shift[crow+cmask:rows,ccol+rmask:cols] = 0 # 高周波成分を落とす

  # 周波数領域にマスクをかける
  rows, cols = img.shape                       # 画像サイズを取得
  crow, ccol = int(rows / 2), int(cols / 2)    # 画像中心を計算
  mask = np.zeros_like(img)
  sigma_x = n_mask
  sigma_y = n_mask
  for x in range(0,rows):
      for y in range(0,cols):
#           if (x- crow)**2 +(y- ccol)**2 <R**2:
#               mask[x,y]=
          mask[x,y] = np.exp(-(x-crow)**2/(2*sigma_x**2)-(y-ccol)**2/(2*sigma_y**2))

  f_shift=f_shift*mask
   
  # 2D逆フーリエ変換によりフィルタリング後の画像を得る
  f_ishift = np.fft.ifftshift(f_shift)        # シフト分を元に戻す
  img_back = np.fft.ifft2(f_ishift)           # 逆フーリエ変換
  img_back = np.abs(img_back)                 # 実部を計算する
  return img_back


