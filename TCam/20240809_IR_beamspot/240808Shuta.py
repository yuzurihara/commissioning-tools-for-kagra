import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy import odr # for fitting
import os, glob, sys
import my_function as mf
from hdaf_filter import hdaf, input_output
import math 


ITMX_r_pre= 630; ITMX_mirror_center_X=1819.5; ITMX_mirror_center_Y=1400.5
ITMY_r_pre= 640; ITMY_mirror_center_X=2254.6; ITMY_mirror_center_Y=1346.5
ETMX_r_pre=1243; ETMX_mirror_center_X=1947.8; ETMX_mirror_center_Y=1410.4
ETMY_r_pre=1248; ETMY_mirror_center_X=2216.5; ETMY_mirror_center_Y=1029.9

# data directory
datadir      = '../data/240809/Original0809/'
savedir      = '../data/240809/MyFit0809/'
# Parameters to be given
############### parameters to obtain the weight centers ############
# n_thresh    : thereshold for binarization
# n_area      : lower limit contour area calculating the Weight center
############### parameters to obtain the fitting centers ############
# refine_estimate : calculate the fitting center for True
# mask_radius     : radius for the mask circle around the weith center
# n_slice         : parameters to obtain the fitted centers 
# data_quality    : minimum AD count to be used for the fitting 

mf.renew_dir(savedir)

filename= sorted(glob.glob(os.path.join(datadir, '*.png')))
for f in filename:
    if          "ITMX" in f:
        pix2mm = 110/ITMX_r_pre; mirror_center_X=ITMX_mirror_center_X; mirror_center_Y=ITMX_mirror_center_Y
        n_thresh, n_area, refine_estimate, mask_radius, n_slice, data_quality = 220, 1e4, 1, 300, 350, 100
    elif        "ITMY" in f:                         
        pix2mm = 110/ITMY_r_pre; mirror_center_X=ITMY_mirror_center_X; mirror_center_Y=ITMY_mirror_center_Y
        n_thresh, n_area, refine_estimate, mask_radius, n_slice, data_quality = 220, 1e4, 1, 500, 400, 100
    elif        "ETMX" in f:                         
        pix2mm = 110/ETMX_r_pre; mirror_center_X=ETMX_mirror_center_X; mirror_center_Y=ETMX_mirror_center_Y
        n_thresh, n_area, refine_estimate, mask_radius, n_slice, data_quality = 220, 1e4, 1, 500, 400, 100
    elif        "ETMY" in f:                         
        pix2mm = 110/ETMY_r_pre; mirror_center_X=ETMY_mirror_center_X; mirror_center_Y=ETMY_mirror_center_Y
        n_thresh, n_area, refine_estimate, mask_radius, n_slice, data_quality = 220, 1e4, 1, 450, 450, 100
        
    # 画像を読み込む
    img_full   = cv2.imread(f, cv2.IMREAD_UNCHANGED)
    # グレースケールにする
    img_gray = cv2.cvtColor(img_full, cv2.COLOR_BGR2GRAY)
    # グレースケール画像を二値化
    ret,img = cv2.threshold(img_gray,n_thresh,255,cv2.THRESH_BINARY)
    # 輪郭情報の取得
    contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    
    # 輪郭ごとの処理
    for i, contour in enumerate(contours):
        # 輪郭の面積 > n_areaのものについて重心を求める
        if np.abs(cv2.contourArea(contour, True)) > n_area:
            # 重心の計算
            m = cv2.moments(contour)
            x,y= m['m10']/m['m00'] , m['m01']/m['m00']
            # 重心位置に x印を書く
            cv2.drawMarker(img_full, position=(round(x), round(y)), color=(0  ,0  ,255), markerType=1, markerSize=32, thickness=8, line_type=cv2.LINE_4)
            # contourの輪郭を描画（青色で描画）
            cv2.polylines(img_full, contour, True, (255, 0, 0), 10)
#             #### contourだけの画像が欲しい時
#             img_blank = np.ones_like(img) * 255
#             img_contour_only = cv2.drawContours(img_blank, contour, -1, (0,0,0), 3)
#             cv2.imwrite(savedir+os.path.splitext(os.path.split(f)[1])[0]+'_moment'+str(i)+'.jpg',img_contour_only) 
    # 重心結果の保存
    cv2.imwrite(savedir+os.path.splitext(os.path.split(f)[1])[0]+'_moment.jpg',img_full)
    
    # 面積が最大の輪郭を見つける
    max_area = 0
    max_contour = None

    for contour in contours:
        area = cv2.contourArea(contour)
        if area > max_area:
            max_area = area
            max_contour = contour

    ############## fitting #####################
    if refine_estimate == True:            
        # 最大の面積の輪郭の重心座標
        M = cv2.moments(max_contour)
        if M["m00"] != 0:
            cXY = [round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"])]
        else:
            cXY = [0, 0]  # 重心が計算できない場合（面積が0の場合）

        x_diff=(cXY[0]-mirror_center_X)*pix2mm
        y_diff=(cXY[1]-mirror_center_Y)*pix2mm
        cv2.putText(img_full, "diff from mirror center to beam (weight) center [mm]= ({:.1f}, {:.1f})".format(x_diff, y_diff), (100, int(img_full.shape[0]*0.93)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)
#        print("File Name: "+os.path.splitext(os.path.split(f)[1])[0]+", Weight Center = ({:.2f}, {:.2f})".format(x, y)+", Area Sice = ", np.abs(cv2.contourArea(contour, True)))
#        print("File Name: "+os.path.splitext(os.path.split(f)[1])[0]+", diff from mirror center to beam center [mm]= ({:.1f}, {:.1f})".format(x_diff, y_diff)+", Area Sice = ", np.abs(cv2.contourArea(contour, True)))

        # 重心の周りに画像をマスク
        img_full_masked = mf.CircleMask(img_full, cXY[0], cXY[1], mask_radius)
        cv2.imwrite(savedir+os.path.splitext(os.path.split(f)[1])[0]+'_masked.jpg',img_full_masked)
        # 重心の周りに画像をマスク, グレースケール
        img_masked      = mf.CircleMask(img_gray, cXY[0], cXY[1], mask_radius)
        # adaf.pyを用いて，low-pass filterをかける．
        img             = np.array(img_masked, dtype=float) # converting np.array
        obj             = hdaf.filt(img)
        img_ftr         = obj.apply_filter('low_pass', 80)
        cv2.imwrite(savedir+os.path.splitext(os.path.split(f)[1])[0]+'_low-pass.jpg',img_ftr)
        c_ave = []; c_med = []; c_err = []
        
        for ax_slice in range(2):
            fit_results = []
            x_slice = np.linspace(0, img.shape[ax_slice]-1, n_slice)      
            for i_slice in range(n_slice):
                
                xdata, ydata, index = mf.get_fitting_data(img,img_ftr,ax_slice,x_slice[i_slice])    
                #    cv2.imwrite(datadir+os.path.splitext(os.path.split(f)[1])[0]+'_filtered.jpg',img_ftr)
                
                xdata_fit = xdata[index]
                ydata_fit = ydata[index]
                
                if np.max(ydata_fit) < data_quality:
                #    print('no IR beam spot seen in this slice {:d}'.format(int(x_slice[i_slice])))
                    continue
            
                popt, pcov = mf.get_peak(xdata_fit, ydata_fit)
            
                result = []
                result.append(x_slice[i_slice])
                for i in range(len(popt)):
                    result.append(popt[i])
                    result.append(np.diag(pcov)[i])
                fit_results.append(result)

            c_ave.append(mf.get_average(fit_results)[0])
            c_med.append(mf.get_average(fit_results)[1])    
            c_err.append(mf.get_average(fit_results)[2])
        x_diff=(c_ave[0]-mirror_center_X)*pix2mm; x_diff_err=c_err[0]*pix2mm
        y_diff=(c_ave[1]-mirror_center_Y)*pix2mm; y_diff_err=c_err[1]*pix2mm
#        print("File Name: "+os.path.splitext(os.path.split(f)[1])[0]+", Fitted Center = (x_average,y_average)=({:.2f}, {:.2f}), (x_median,y_median)=({:.2f}, {:.2f}), (x_error,y_error)=({:.2f}, {:.2f})".format(c_ave[0], c_ave[1],c_med[0], c_med[1], c_err[0], c_err[1]))
#        print("File Name: "+os.path.splitext(os.path.split(f)[1])[0]+", diff from mirror center to beam center [mm]= ({:.1f} +/- {:.1f}, {:.1f} +/- {:.1f})".format(x_diff, x_diff_err, y_diff, y_diff_err))

        cv2.putText(img_full, "diff from mirror center to beam (fitted)  center [mm]= ({:.1f} +/- {:.1f}, {:.1f} +/- {:.1f})".format(x_diff, x_diff_err, y_diff, y_diff_err), (100, int(img_full.shape[0]*0.95)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)
        cv2.drawMarker(img_full, position=(int(c_ave[0]), int(c_ave[1])), color=(0  ,255  ,0  ), markerType=3, markerSize=32, thickness=8, line_type=cv2.LINE_4)
        # text in left bottom
        # cv2.putText(img_full, 'Taken at %s (%s)' % (date, mirror_name), (100, int(height*0.93)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)    
        # cv2.putText(img_full, 'gain = %s, T = %s [s]' % (gain, T), (100, int(height*0.95)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)
#         cv2.putText(img_full, "center = ({:.1f} +/- {:.1f}, {:.1f} +/- {:.1f})".format((float(c_ave[0])-center_pix_x)*pix2mm, float(c_err[0])*pix2mm, (float(c_ave[1])-center_pix_y)*pix2mm, float(c_err[1])*pix2mm)+" from center", (100, img_full.shape[1]/5), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)
                       
        cv2.imwrite(savedir+os.path.splitext(os.path.split(f)[1])[0]+'_fitted.jpg',img_full)
    
 