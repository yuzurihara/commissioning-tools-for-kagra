#!/usr/bin/env python
# coding: utf-8

####################################################
#
# 2022/8/.. fitting and finding center of mirros (ITMX, ETMX, ...)
# developped by S.J.Tanaka, Toriyama, Shita, Yuzurihara,
#
####################################################
#
# update history
#
#
#
####################################################

import cv2
import numpy as np
from matplotlib import pyplot as plt
import os
import sys
import datetime
import glob
#import shutil
import pandas as pd
import math

import subprocess

import TCam_beam_center_lib as mf

from csv import writer

import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

mpl.rcParams['xtick.labelsize'] = 40
mpl.rcParams['ytick.labelsize'] = 40
mpl.rcParams["axes.labelsize"] = 40
mpl.rcParams["axes.titlesize"] = 40
mpl.rcParams["legend.fontsize"] = 40

mpl.rcParams['axes.linewidth'] = 10

######################################
# Calibration factor for each mirro
###################################### 
# 2022/08/22~
# [pixel]
# ETMX_r_pre = 1246
# ETMY_r_pre = 1248
# ITMX_r_pre = 630
# ITMY_r_pre = 640
mirror_r_pre = 110 # [mm]

#
# return calibration factor pixel to mm
#
def calibration_factor_pixel2mm(mirror_name, r_mirror):
    factor = 0.0
    if mirror_name == "ETMX":
        factor = mirror_r_pre / r_mirror
    elif mirror_name == "ETMY":
        factor = mirror_r_pre / r_mirror
    elif mirror_name == "ITMX":
        factor = mirror_r_pre / r_mirror
    elif mirror_name == "ITMY":
        factor = mirror_r_pre / r_mirror
    else:
        print("%s is not supported." % mirror_name)
    return factor

#
# extract gain and exposure time from configuration file of TCam
#
def get_gain_T(config_file):
    # tmp = figname.split(".")[0].split("_")[3:6]
    # date = "-".join(tmp)

    # config_file = dirname + "/" + figname.split(".")[0] + ".txt"
    #print(config_file)
    
    with open(config_file, 'r') as outfile:
        lines = outfile.readlines()
        outfile.close()

        gain = 0
        T = 0
        lines = [line.strip() for line in lines]        
        for line in lines:
            if "controls_[3]_value_milliseconds" in line:
                T = line.split(" ")[1]

            if "controls_[2]_value" in line:
                gain = line.split(" ")[1]
    gain = math.ceil(int(gain))
    T    = math.ceil(float(T))
    return gain, T


    

def fit_image(outdirname, outdirname_sym, dirname, dir_base_fit,
              figname, mirror_name, gain, T, x_mirror, y_mirror, r_mirror,
              x_err_mirror, y_err_mirror, n_slice = 250, threshold_center_error = 2, flag_output=False):

    print("n_slice = %d" % n_slice)
    
    n_tile_horizon  = 8 # how many tiles we put horizontally
    fname="%s/%s" % (dirname, figname)
    
    img_full = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
    height, width, _ = img_full.shape    
    img_gray = cv2.cvtColor(img_full, cv2.COLOR_BGR2GRAY)
    img      = np.array(img_gray, dtype=float)

    # image filtering method
    img_ftr = mf.twoD_LowPass(img)
    if flag_output:
        fname="%s/lp_%s" % (outdirname, figname)
        cv2.imwrite(fname, img_ftr)
    
    char_axis = ["y", "x"]
    directions = ['yoko', 'tate']    
    return_values = np.zeros(4)
    center_values_slice = {}
    center_values_ave = {}
    center_values_err = {}
    
    for ax_slice in range(2):
        cut_direction = directions[ax_slice]        
        fit_results = []

        # 鏡の内側のみをフィットに使う(pcalをフィットに使わないため)
        r_half = r_mirror/3.0
        if char_axis[ax_slice] == "x": # x軸方向の中心を決めたいので、縦方向にimageを輪切りにする
            x_slice = np.linspace(int(x_mirror-r_half), int(x_mirror+r_half), n_slice)
        if char_axis[ax_slice] == "y": # y軸方向の中心を決めたいので、横方向にimageを輪切りにする
            x_slice = np.linspace(int(y_mirror-r_half), int(y_mirror+r_half), n_slice)
        #print(x_slice[0], x_slice[-1])
            
        n_tile_vertical = math.ceil(len(x_slice) / n_tile_horizon)
            
        fig = plt.figure(figsize=[100, n_tile_vertical*10])      
            
        for i_slice in (range(n_slice)):

            ii_slice = int(x_slice[i_slice])
            #print("ii_slice = %f" % ii_slice)
            if cut_direction == 'yoko': # y = y_0 で切った時のデータをフィットする。横に切る
                # (y_mirror-r_half), int(y_mirror+r_half),
                x_beg = int(x_mirror - r_half)
                x_end = int(x_mirror + r_half)
                ydata     = img[ii_slice, x_beg:x_end]
                ydata_ftr = img_ftr[ii_slice, x_beg:x_end]
                #ydata     = img[ii_slice, :]
                #ydata_ftr = img_ftr[ii_slice, :]
                #xdata = np.arange(len(ydata)) + x_beg
                xdata = np.arange(len(ydata))
                fit_color = 'pink'

                index1 = np.ones(len(ydata), dtype=bool)
                
                # only for ETMY
                if mirror_name == "ETMY" and y_mirror <= ii_slice <= height:
                    index1[( x_mirror < xdata ) & (xdata < width)] = False
                
            elif cut_direction == 'tate':  # x = x_0 で切った時のデータをフィットする。縦に切る
                x_beg = int(y_mirror - r_half)
                x_end = int(y_mirror + r_half)
                ydata     = img[x_beg:x_end, ii_slice]
                ydata_ftr = img_ftr[x_beg:x_end, ii_slice]
                #ydata     = img[:, ii_slice]
                #ydata_ftr = img_ftr[:, ii_slice]
                #xdata = np.arange(len(ydata)) + x_beg
                xdata = np.arange(len(ydata))
                fit_color = 'cyan'

                index1 = np.ones(len(ydata), dtype=bool)
                
                # only for ETMY                

                if mirror_name == "ETMY" and x_mirror <= ii_slice <= width:
                    index1[( y_mirror < xdata ) & (xdata < height)] = False

            #print(x_beg, x_end)
            #xdata = np.arange(len(ydata))
            xdata = np.arange(len(ydata)) + x_beg
            # xdata_fit = xdata[index1] # ETMYでなければindexはすべてTrueなので影響なし
            # ydata_fit = ydata[index1]
            # ydata_ftr = ydata_ftr[index1]                        

            # ydata > ydata_ftr の部分のデータを落とす            
            #index2 = np.ones(len(ydata_fit), dtype=bool)
            #index2[ydata_fit > ydata_ftr] = False
            index2 = np.ones(len(ydata), dtype=bool)
            index2[ydata > ydata_ftr] = False
            
            #xdata_fit = xdata_fit[index2 & index1]
            #ydata_fit = ydata_fit[index2 & index1]

            xdata_fit = xdata[index2 & index1]
            ydata_fit = ydata[index2 & index1]

           
            #print(len(xdata_fit), len(ydata_fit))
            if len(ydata_fit) > 1:
                popt, pcov = mf.get_peak(xdata_fit, ydata_fit)
                center_err = np.diag(pcov)[2]
            else:
                center_err = 1e+5
            #print(popt)

                
            plt.subplot(n_tile_vertical, n_tile_horizon, i_slice+1)        
            
            plt.plot(xdata_fit, ydata_fit, label="%s=%d" % (char_axis[ax_slice], ii_slice), c="black", marker="x",
                     markersize=10, markeredgewidth=4, linewidth=0)                             
            #print(center_err)
            if(center_err < threshold_center_error):
                result = []
                result.append(x_slice[i_slice]) # これが今のsliceの値
                for i in range(len(popt)):
                    result.append(popt[i]) # 最初のフィットパラメーター [amplitude, width, center, background]
                    result.append(np.diag(pcov)[i]) # 標準偏差 std error
                fit_results.append(result)

                plt.plot(np.array(xdata), mf.ClippedGaussian(xdata, *popt), c=fit_color, linewidth=4)
                plt.vlines(popt[2], 0, 255, "red", label="center = %d" % popt[2], linestyles='dashed', linewidth=4) # vlines        
            plt.legend()                

        fig.patch.set_facecolor('white')
        tmp = "fit_%s_%s.png" % (figname.split(".")[0], cut_direction)
        fname="%s/%s" % (outdirname, tmp)
        plt.savefig(fname, bbox_inches='tight', pad_inches=0.5)        
        
        
        x, dx = mf.get_average(fit_results)
        print("%s = %.2f +/- %.2f" % (char_axis[ax_slice], x, dx))
        return_values[2*ax_slice] = x
        return_values[2*ax_slice+1] = dx

        # fit_results = [slice, amplitude, amplitude_error, width, width_error, center, center_error, bias, bias_error]
        center_values_slice["%s" % char_axis[ax_slice]] = [r[0] for r in fit_results]
        center_values_ave["%s" % char_axis[ax_slice]]   = [r[5] for r in fit_results]
        center_values_err["%s" % char_axis[ax_slice]]   = [r[6] for r in fit_results]
        
    
    x     = return_values[0]
    x_err = return_values[1]
    y     = return_values[2]
    y_err = return_values[3]


    fname="%s/%s" % (dirname, figname)
    img_estimated = cv2.imread(fname, cv2.IMREAD_UNCHANGED)    

    #
    # mirror center of circle with yellow
    # color=(255, 0, 255), # magenta
    # color=(0, 255, 255) # yellow    
    # color=(0, 0, 255) # lime
    #
    center=(int(x_mirror), int(y_mirror))
    img_estimated =cv2.circle(img_estimated, center=center, radius=16, color=(0, 255, 255), # yellow
                              thickness=-1, lineType=cv2.LINE_4, shift=0)
    # large circle for mirror edge with magenta
    img_estimated =cv2.circle(img_estimated, center=center, radius=int(r_mirror), color=(0, 255, 255), # yellow
                              thickness=3, lineType=cv2.LINE_4, shift=0)
    
    #
    # add reticle
    #
    img_estimated =cv2.line(img_estimated, (0, center[1]), (width, center[1]), color=(0, 255, 255), # yellow
           thickness=3, lineType=cv2.LINE_4, shift=0)
    img_estimated =cv2.line(img_estimated, (center[0], 0), (center[0], height), color=(0, 255, 255), # yellow
           thickness=3, lineType=cv2.LINE_4, shift=0)

    reticle_size = 128
    for i in range(-3, 4):
        img_estimated =cv2.line(img_estimated, (center[0] - reticle_size, center[1]+int(r_mirror*i/3.0)), (center[0] + reticle_size, center[1]+int(r_mirror*i/3.0)), color=(0, 255, 255), # yellow
               thickness=3, lineType=cv2.LINE_4, shift=0)
        img_estimated =cv2.line(img_estimated, (center[0]+int(r_mirror*i/3.0), center[1] - reticle_size), (center[0]+int(r_mirror*i/3.0), center[1] + reticle_size), color=(0, 255, 255), # yellow
               thickness=3, lineType=cv2.LINE_4, shift=0)    
    
    
    # beam center of circle with magenta
    center=(int(x), int(y))
    img_estimated =cv2.circle(img_estimated, center=(int(x), int(y)), radius=32, color=(255, 0, 255),
           thickness=-1, lineType=cv2.LINE_4, shift=0)
    
    
    #
    # extract date when TCam took the figure
    #
    year = figname.split("_")[3]
    mm   = figname.split("_")[4][0:2]
    dd   = figname.split("_")[4][2:4]
    hour = figname.split("_")[5][0:2]
    minute = figname.split("_")[5][2:4]
    second = figname.split("_")[5][4:6]
    date = year + "/" + mm + "/" + dd + " " + hour + ":" + minute   

    # difference from mirror center
    calibration = calibration_factor_pixel2mm(mirror_name, r_mirror)
    diff_x_mm = (x - x_mirror) * calibration
    diff_y_mm = (y - y_mirror) * calibration
    
    
    # text in left bottom (magenta)
    cv2.putText(img_estimated, 'Taken at %s (%s)' % (date, mirror_name),
                (100, int(height*0.89)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)
    cv2.putText(img_estimated, 'gain = %s, T = %s [ms]' % (gain, T),
                (100, int(height*0.91)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)    
    cv2.putText(img_estimated, 'Beam center = (%.1f +/- %.1f, %.1f +/- %.1f)' % (x, x_err, y, y_err),
                (100, int(height*0.93)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)
    cv2.putText(img_estimated, 'diff from mirror center to beam center [mm] = (%+.1f, %+.1f)' % (diff_x_mm, diff_y_mm),
                (100, int(height*0.95)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)

    # text in left bottom (yellow)
    cv2.putText(img_estimated, 'mirror center = (%.1f +/- %.1f, %.1f +/- %.1f), radius = %.1f +/- %.1f' % (x_mirror, x_err_mirror, y_mirror, y_err_mirror, r_mirror, 0),
                (100, int(height*0.97)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)    
    
    # save fig as png
    tmp = figname.split(".")[0] + "_beam_center.png"
    fname="%s/%s" % (outdirname, tmp)
    cv2.imwrite(fname, img_estimated)


    
    #
    # make error bar plot of fitting
    #
    xslice  = center_values_ave["y"]
    yslice  = center_values_slice["y"]
    for i in range(len(xslice)):
        center = (int(xslice[i]), int(yslice[i]))
        img_estimated =cv2.circle(img_estimated, center=center, radius=8, color=(255, 0, 0), # red
                                  thickness=-1, lineType=cv2.LINE_4, shift=0)
    xslice  = center_values_slice["x"]
    yslice  = center_values_ave["x"]
    for i in range(len(xslice)):
        center = (int(xslice[i]), int(yslice[i]))
        img_estimated =cv2.circle(img_estimated, center=center, radius=8, color=(255, 0, 0), # red
                                  thickness=-1, lineType=cv2.LINE_4, shift=0)

    if mirror_name == "ETMY":
        img_estimated = cv2.rectangle(img_estimated, (int(x_mirror), int(y_mirror)), (width, height-200), color=(128, 128, 128), # lime
                     thickness=-1, lineType=cv2.LINE_4, shift=0)        
        
    # save fig as png
    fname="%s/scatter_%s" % (outdirname, figname)
    cv2.imwrite(fname, img_estimated)    
    

    
    #
    # make intermediate plots
    #
#     fname="%s/%s" % (dirname, figname)
#     img_estimated = cv2.imread(fname, cv2.IMREAD_UNCHANGED)    

#     calibration = calibration_factor_pixel2mm(mirror_name, r_mirror)
#     diff_x_mm = (x - x_mirror) * calibration
#     diff_y_mm = (y - y_mirror) * calibration
        
#     # text in left bottom
#     cv2.putText(img_estimated, 'Taken at %s (%s)' % (date, mirror_name), (100, int(height*0.89)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)    
#     cv2.putText(img_estimated, 'gain = %s, T = %s [ms]' % (gain, T), (100, int(height*0.91)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)    
    
#     cv2.putText(img_estimated, 'mirror center = (%.1f +/- %.1f, %.1f +/- %.1f), radius = %.1f +/- %.1f' % (x_mirror, x_err_mirror, y_mirror, y_err_mirror, r_mirror, 0),
#             (100, int(height*0.93)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 0), thickness=5)
#     cv2.putText(img_estimated, '    Beam center = (%.1f +/- %.1f, %.1f +/- %.1f)' % (x, x_err, y, y_err),
#             (100, int(height*0.95)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 0, 255), thickness=5)
#     cv2.putText(img_estimated, 'diff from mirror center to beam center [mm] = (%.1f, %.1f)' % (diff_x_mm, diff_y_mm),
#             (100, int(height*0.97)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)
    
#     #
#     # make error bar plot of fitting
    #
    # xslice  = center_values_ave["x"]
    # yslice  = center_values_slice["x"]
    # for i in range(len(xslice)):
    #     center = (int(xslice[i]), int(yslice[i]))
    #     img_estimated =cv2.circle(img_estimated, center=center, radius=8, color=(255, 255, 0),
    #                               thickness=-1, lineType=cv2.LINE_4, shift=0) # (255,255,0) = blue
    # xslice  = center_values_slice["y"]
    # yslice  = center_values_ave["y"]
    # for i in range(len(xslice)):
    #     center = (int(xslice[i]), int(yslice[i]))
    #     img_estimated =cv2.circle(img_estimated, center=center, radius=8, color=(255, 255, 0),
    #                               thickness=-1, lineType=cv2.LINE_4, shift=0) # (255,255,0) = blue

    # # mirror center of circle with magenta
    # center=(int(x_mirror), int(y_mirror))
    # #img_estimated =cv2.circle(img_estimated, center=center, radius=8, color=(255, 0, 255),
    # img_estimated =cv2.circle(img_estimated, center=center, radius=8, color=(0, 255, 0),
    #                           thickness=-1, lineType=cv2.LINE_4, shift=0)
    # # large circle for mirror edge with magenta
    # #img_estimated =cv2.circle(img_estimated, center=center, radius=int(r_mirror), color=(255, 0, 255),
    # img_estimated =cv2.circle(img_estimated, center=center, radius=int(r_mirror), color=(0, 255, 0),
    #                           thickness=3, lineType=cv2.LINE_4, shift=0)
    # # beam center of circle with lime
    # center=(int(x), int(y))
    # img_estimated =cv2.circle(img_estimated, center=center, radius=8, color=(0, 0, 255),
    #        thickness=-1, lineType=cv2.LINE_4, shift=0)
       
    # # save fig as png
    # fname="%s/scatter_%s" % (outdirname, figname)
    # cv2.imwrite(fname, img_estimated)    
    
    plt.close('all') # windowを閉じないと、matplotlibが開きすぎとwarningを出すので
     
    return x, x_err, y, y_err





def create_symbolic_link(figname, outdirname, dir_base_fit, mirror_name):

    # only for Kamioka computer
    if os.path.isdir("/data2"):
        # change directory for relative symbolic link
        os.chdir(dir_base_fit)

        tmp = figname.split(".")[0] + "_beam_center.png"
        fname="%s/%s" % (outdirname, tmp)
        
        fname_sym = "latest.png"
        # update symbolic link
        if os.path.islink(fname_sym) == True:
            os.remove(fname_sym)
        tmp = "/".join(fname.split("/")[4:]) # example : '2022/0329/TCam_ETMX_01110_2022_0329_141445_fit.png'
        os.symlink(tmp, fname_sym)

        fname_sym = "latest.txt"
        # update symbolic link
        if os.path.islink(fname_sym) == True:
            os.remove(fname_sym)
        tmp = "/".join(fname.split("/")[4:]) # example : '2022/0329/TCam_ETMX_01110_2022_0329_141445_fit.png'
        tmp = tmp.replace('png', 'txt')
        os.symlink(tmp, fname_sym)
        
        # copy dat file including update time
        dir_base = dir_base_fit.replace('TCam_fit', 'TCam') # example : '/data2/TCam/ETMX'
        #shutil.copy('%s/lastupdate.txt' % dir_base, '%s/lastupdate.txt' % dir_base_fit)        
        cmd='cp %s/lastupdate.txt %s/lastupdate.txt > /dev/null 2>&1' % (dir_base, dir_base_fit)
        subprocess.run(cmd, shell=True)

        # make thumbnail figure
        cmd='cwebp -resize 518 0 -o thumb.webp %s > /dev/null 2>&1' % fname
        subprocess.run(cmd, shell=True)


#
# save fitting results as text and csv files
#
def save_textfile(figname, dir_base_fit, x, y, x_err, y_err,
                  x_mirror, y_mirror, r_mirror, x_err_mirror, y_err_mirror,
                  gain, T, mirror_name):

    #
    # Output result as text file (overwrite)
    #            
    textname="%s/%s_beam_center.txt" % (outdirname, figname.split(".")[0])
    with open(textname, 'w') as outfile: # a : 追記モード, w : 上書きモード
        dt_now = datetime.datetime.now()

        calibration = calibration_factor_pixel2mm(mirror_name, r_mirror)

        #line="%s %.2f %.2f %.2f %s %s %.2f %.2f %.2f\n" % (date, x, y, r, T, gain, x_err, y_err, r_err)
        line = "%s\n" % dt_now
        line = line + "gain %d\n" % gain
        line = line + "exposure_time[ms] %d\n" % T
        line = line + "x_mirror_center %.2f\n" % x_mirror
        line = line + "y_mirror_center %.2f\n" % y_mirror
        line = line + "x_err_mirror_center %.2f\n" % x_err_mirror
        line = line + "y_err_mirror_center %.2f\n" % y_err_mirror
        line = line + "r %.2f\n" % r_mirror
        line = line + "\n"
        line = line + "x_beam_center %.2f\n" % x
        line = line + "y_beam_center %.2f\n" % y
        line = line + "x_err_beam_center %.2f\n" % x_err
        line = line + "y_err_beam_center %.2f\n" % y_err
        line = line + "\n"
        line = line + "diff_x_beam-center[mm] %+.2f\n" % ((x - x_mirror) * calibration)
        line = line + "diff_y_beam-center[mm] %+.2f\n" % ((y - y_mirror) * calibration)
        outfile.write(line)
        outfile.close

    # only for Kamioka machine
    if os.path.isdir("/data2"):
        #
        # save header for csv
        # only for the first time
        #
        csvname="%s/archive/fit_beam_center_%s.csv" % (dir_base_fit, mirror_name)
        if not os.path.exists(csvname):
            csvline_header=["fitting date", "gain", "T", "x_beam", "y_beam", "x_error", "y_error", "x_mirror", "y_mirror", "r_mirror", "filename"]
            with open(csvname, 'a', newline='') as f:      
                writer_object = writer(f)
                writer_object.writerow(csvline_header)  
                f.close()            

        #
        # If the result for same figure is in csv file,
        # comment out past result by adding #, and add latest result in the bottom
        #
        if os.path.isfile(csvname):
            df = pd.read_csv(csvname)
            #
            # list of past reulsts for same TCam photo
            #
            ll = df[df["filename"] == figname]

            if len(ll) == 0: # first fitting
                with open(csvname, 'a', newline='') as f:  
                    writer_object = writer(f)
                    csvline=[dt_now, gain, T, x, y, x_err, y_err, x_mirror, y_mirror, r_mirror, figname]
                    writer_object.writerow(csvline)
                    f.close()        

            else: # not first fitting
                for i in list(ll.index):
                    # df.loc[df.index==i, ["x"]] = 10
                    # df.loc[df.index==i, ["y"]] = 10
                    df.loc[df.index==i, ["fitting date"]] = "#" + df.loc[df.index==i, ["fitting date"]]
                    
                # number of fitting
                n = len(df["gain"])
                #print(n)
                line_to_add = ll.iloc[-1].values # copy last fitting result
                df.loc[n] = line_to_add

                #
                # update the values to the latest one
                #
                df.loc[n, ["x"]] = x
                df.loc[n, ["y"]] = y
                df.loc[n, ["x_error"]] = x_err
                df.loc[n, ["y_error"]] = y_err
                df.loc[n, ["x_mirror"]] = x_mirror
                df.loc[n, ["y_mirror"]] = y_mirror
                df.loc[n, ["r_mirror"]] = r_mirror

                # save as csv (overwrite)
                df.to_csv(csvname, index=False)

            #if os.path.isfile(csvname):
            # csvname2="/mnt/cds_nfs/TCam/fit_value_%s.csv" % (mirror_name)            
            # cmd='cp %s %s > /dev/null 2>&1' % (csvname, csvname2)
            # subprocess.run(cmd, shell=True)

def dump_error_message_and_exit(message, dir_base_fit):
    print(message)

    if os.path.isdir("/data2"):
        cmd='echo "%s" > %s/lastupdate.txt' % (message, dir_base_fit)
        subprocess.run(cmd, shell=True)
    sys.exit(1)


def get_latest_mirror_center(dir_base_fit, mirror_name, dirname, figname):

    #
    # read the latest fitting result.
    #
    latest_fit_txt=dirname + "/" + figname.split(".")[0] + "_fit.txt"    
    if os.path.exists(latest_fit_txt):
        a = np.loadtxt("TCam_ETMX_01111_2023_0904_083135_fit.txt", unpack=True, dtype=str)[1]
        x_mirror = float(a[3])
        y_mirror = float(a[4])
        x_err_mirror = float(a[5])
        y_err_mirror = float(a[5])
        r_mirror = float(a[7])
    else:
        csvname="%s/archive/fit_value_%s.csv" % (dir_base_fit, mirror_name)
        if os.path.exists(csvname):
            df = pd.read_csv(csvname)
            x_mirror = df.iloc[-1]["x"]
            y_mirror = df.iloc[-1]["y"]
            x_err_mirror = df.iloc[-1]["x_error"]
            y_err_mirror = df.iloc[-1]["y_error"]
            r_mirror = df.iloc[-1]["r"]
        else:
            x_err_mirror = 0
            y_err_mirror = 0
            if mirror_name == "ETMX":
                # 2022/08/29~
                x_mirror = 1925
                y_mirror = 1410
                r_mirror = 1246
            elif mirror_name == "ETMY":
                # 2022/08/29~
                x_mirror = 2220
                y_mirror = 980
                r_mirror = 1248
            elif mirror_name == "ITMX":
                # 2022/08/29~
                x_mirror = 1825
                y_mirror = 1363
                r_mirror = 630
            elif mirror_name == "ITMY":
                # 2022/08/29~
                x_mirror = 2451
                y_mirror = 1273
                r_mirror = 640
            else:
                x_mirror = 0
                y_mirror = 0
                r_mirror = 0
            
    return x_mirror, y_mirror, x_err_mirror, y_err_mirror, r_mirror

######
#
# main
#
arg = sys.argv
input_figname = arg[1] # full path including dir + file name
figname = os.path.basename(input_figname)
dirname = os.path.dirname(input_figname)
tmp = figname.split("_") # ['TCam', 'ETMX', '01110', '2022', '0328', '102439.png']

#
# only for Kamioka computer
#
if os.path.isdir("/data2"):
    outdirname = "/data2/" + tmp[0] + "_fit" + "/" + tmp[1] + "/archive/" + tmp[3] + "/" + tmp[4]
    dir_base_fit = "/".join(input_figname.split("/")[0:4]).replace("TCam", "TCam_fit")     # /data2/TCam_fit/ETMX
#
# for other users 
#
else:
    outdirname = "./"
    dir_base_fit = "./"
outdirname_sym = tmp[1] + "/" # for doing `cd` command, only when there is `/data2` directory
mirror_name = tmp[1]
print("mirror name = %s" % mirror_name)
if mirror_name not in ["ITMX", "ETMX", "ITMY", "ETMY"]:
    message = "error : mirror name (%s) is out of support" % mirror_name
    dump_error_message_and_exit(message, dir_base_fit)

if not os.path.isfile(input_figname):
    message = 'error : figure not found. require FULL path to figure.'
    dump_error_message_and_exit(message, dir_base_fit)

config_file = dirname + "/" + figname.split(".")[0] + ".txt"
if not os.path.isfile(config_file):
    message = 'error : configure file from TCam not found. configuration file should be located in same directory as figure.'
    dump_error_message_and_exit(message, dir_base_fit)
    
if not os.path.exists(outdirname):
    os.makedirs(outdirname)

print("analysis ready!")
    
#    
# extract gain and exposure time from configuration file of TCam
#
gain, T = get_gain_T(config_file)
print("gain = %d, exposure time = %d [s]" % (gain, T/1000))

x_mirror, y_mirror, x_err_mirror, y_err_mirror, r_mirror = get_latest_mirror_center(dir_base_fit, mirror_name, dirname, figname)
print(x_mirror, y_mirror, x_err_mirror, y_err_mirror, r_mirror)

#
# run fit
#
flag_output = True
#n_slice      = 250 # ビームスポット周辺をフィットのために何分割するか、小さい = 分割数が小さい = 早い
#n_slice = 50 
n_slice = 100
#n_slice = 300

if mirror_name == "ITMX" or mirror_name == "ITMY":
    threshold_center_error = 2 # フィット結果のcenterのエラーが何ピクセル以下なら採用するかのしきい値
if mirror_name == "ETMX" or mirror_name == "ETMY":
    #threshold_center_error = 8 # フィット結果のcenterのエラーが何ピクセル以下なら採用するかのしきい値
    threshold_center_error = 50 # フィット結果のcenterのエラーが何ピクセル以下なら採用するかのしきい値
x, x_err, y, y_err = fit_image(outdirname, outdirname_sym,
                               dirname, dir_base_fit,
                               figname, mirror_name, gain, T,
                               x_mirror, y_mirror, r_mirror, x_err_mirror, y_err_mirror,
                               n_slice = n_slice, threshold_center_error = threshold_center_error,
                               flag_output=flag_output)

#
# create symbolic link (only for Kamioka computer)
#
create_symbolic_link(figname, outdirname, dir_base_fit, mirror_name)

#
# save fitting results as text and csv files
#
save_textfile(figname, dir_base_fit, x, y, x_err, y_err,
                  x_mirror, y_mirror, r_mirror, x_err_mirror, y_err_mirror,
                  gain, T, mirror_name)

print("fit_beam_center.py done without error.")
print("file = %s" % figname)
