#!/usr/bin/env python3
#******************************************#
#     File Name: take_Tcam_ITMX_ITMY.py
#        Author: Hirotaka Yuzurihara
# Last Modified: 2023/04/07
#******************************************# 

import ezca as ez
import time

#
# internal parameter for ramp start
#
t_margin = 0.2 # [s]
t_margin_IR_shutter = 4 # [s]
n_photo = 2
#exposures = [15000000, 10000000, 5000000]
#exposures = [10000000, 5000000]
exposures = [100000, 50000]

ezca = ez.Ezca()


###############################################################
#
# reset offset for OPTICALIGN
#
ezca["GRD-IO_REQUEST"] = 'PSL_LOCKED'
print('requested PSL_LOCKED state to GRD-IO_REQUEST (1/8)')
time.sleep(t_margin)
while ezca["GRD-IO_STATE"] != 'PSL_LOCKED':
    pass
print('Done : requested PSL_LOCKED state to GRD-IO_REQUEST (2/8)')

#
# 0 : IR shutter closes
# 1 : IR shutter open
#
ezca["PSL-BEAM_SHUTTER"] = 0
time.sleep(t_margin_IR_shutter)   
print('IR shutter was closed (3/8)')

#
# set configurations for taking pictures of ITMX and ITMY
#
j=1
n_total=len(exposures) * n_photo
for exposure in exposures:
    for i in range(n_photo):
        for PLACE in ['IXA', 'IYA']:
            ezca['CAM-%s_EXP'  % PLACE] = exposure
            ezca['CAM-%s_GAIN' % PLACE] = 470
            ezca['GRD-CAM_%s_REQUEST' % PLACE] = 'SNAPSHOT'

        print('Taking TCam images for ITMX and ITMY with exposure %d s (%d/%d)' % (exposure, j, n_total))
        #while not (ezca['GRD-CAM_IXA_STATE'] == 'RESTORING' and ezca['GRD-CAM_IYA_STATE'] == 'RESTORING'):
        while not (ezca['GRD-CAM_IXA_STATE'] == 'DOWN' and ezca['GRD-CAM_IYA_STATE'] == 'DOWN'):
            pass
        print('Done : Take TCam images for ITMX and ITMY with exposure %d s (%d/%d)' % (exposure, j, n_total))
        j=j+1

#
# 0 : IR shutter closes
# 1 : IR shutter open
#
ezca["PSL-BEAM_SHUTTER"] = 1
print('opened IR shutter (6/8)')

ezca['GRD-IO_REQUEST'] = 'PROVIDING_STABLE_LIGHT'
time.sleep(t_margin)
print('requested PROVIDING_STABLE_LIGHT state to GRD-IO_REQUEST (7/8)')
while ezca['GRD-IO_STATE'] != 'PROVIDING_STABLE_LIGHT':
    pass
print('Done : requested PSL_LOCKED state to GRD-IO_REQUEST (8/8)')
