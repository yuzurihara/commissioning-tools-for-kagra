#!/bin/bash -e
#******************************************#
#     File Name: run_fit_Tcam_py.sh
#        Author: Hirotaka Yuzurihara
# Last Modified: 2022/03/30
#******************************************#

LOGFILE=/tmp/fit_mirror_center.log

# conda activate
source /home/controls/miniconda3/etc/profile.d/conda.sh
conda activate fit_tcam

camera=$1

echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process for mirror edge fittting for $mirror was launched" >> $LOGFILE 2>&1

if test "$camera" == "ixa"
then
    mirror="ITMX"
elif test "$camera" == "iya"
then
    mirror="ITMY"
elif test "$camera" == "exa"
then
    mirror="ETMX"
elif test "$camera" == "eya"
then
    mirror="ETMY"
else
    echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] the camera ($camera) is unappropriate for fitting. check the camera name.">> ${LOGFILE} 2>&1
    exit
fi

fname=`readlink -f /data2/TCam/${mirror}/latest.png`
extension=${fname##*.} 
#print $name

#
# For the definition of this vector, see /opt/rtcds/userapps/release/sys/common/guardian/TCAM.py
#
# State Vector definition (from right)
# 0: Pcal ON/OFF
# 1: GR ON/OFF
# 2: IR ON/OFF
# 3: GR Lock/Unlock
# 4: IR Lock/Unlock
#
# '0..10' == Pcal ON or OFF + Green ON + IR ON or OFF + Green lock ON or Off + IR lock OFF

laser_state=`echo $fname | awk -F_ '{print $3}'`
echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] laser state = ${laser_state}" >> ${LOGFILE} 2>&1
if [ `echo "$laser_state" | grep '0..1.'` ]; then
    #if [ `echo "$laser_state" | grep '^0111'` ] || [ `echo "$laser_state" | grep '^0101'` ]; then

    #
    # process for fitting mirror edge (Yuzu)
    # only when Green laser illumites the mirror,  this process will start.
    #
    echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process for mirror edge fitting started.">> ${LOGFILE} 2>&1
    echo "fname = ${fname}" >> ${LOGFILE} 2>&1
    /home/controls/bin/fit_Tcam.py $fname >> ${LOGFILE} 2>&1
    echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process for mirror edge fitting done.">> ${LOGFILE} 2>&1
    
    #
    # process for updaing plot for trend monitor (YamaT)
    #
    echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process to update trend monitor started.">> ${LOGFILE} 2>&1
    OPTIC=`echo $fname | awk -F/ '{print $4}'`
    echo "optic = $OPTIC" >> ${LOGFILE} 2>&1
    /home/controls/bin/MirrorPositionWebPlot.sh ${OPTIC} >> ${LOGFILE} 2>&1
    echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process to update trend monitor done.">> ${LOGFILE} 2>&1

else
    echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] this TCam photo is unappropriate for fitting (we require the laser state 0..1. , but the laser state was ${laser_state})">> ${LOGFILE} 2>&1
fi


