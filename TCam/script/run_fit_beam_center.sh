#!/bin/bash -e
#******************************************#
#     File Name: run_fit_beam_center.sh
#        Author: Hirotaka Yuzurihara
# Last Modified: 2022/03/30
#******************************************#

LOGFILE=/tmp/inotify.log
DIR_WATCH=/data2/TCam

# conda activate
source /home/controls/miniconda3/etc/profile.d/conda.sh
conda activate fit_tcam

# waiting for new TCam figure
#/usr/bin/inotifywait -m -r -e "ATTRIB, CREATE" --format '%T %w%f (%e)' --timefmt '%F %T' $DIR_WATCH  |
#/usr/bin/inotifywait -m -r -e move_to --format '%T %w%f (%e)' --timefmt '%F %T' $DIR_WATCH  |

#
# we wait for new notification of `MOVED_TO`
# for log, check $LOGFILE at calcenter01
#
/usr/bin/inotifywait -m -r -e "MOVED_TO" --format '%T %w%f (%e)' --timefmt '%F %T' $DIR_WATCH  | 
    while read line; do

	echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] inotify detected one TCam photo." >> $LOGFILE 2>&1
	echo "$line" >> $LOGFILE 2>&1
	fname=`echo $line | awk '{print $3}'`
	extension=${fname##*.} 

	[[ "$extension" == "png" ]] && {

	    #
	    # For the definition of this vector, see /opt/rtcds/userapps/release/sys/common/guardian/TCAM.py
	    #
	    # State Vector definition (from right)
	    # 0: Pcal ON/OFF
	    # 1: GR ON/OFF
	    # 2: IR ON/OFF
	    # 3: GR Lock/Unlock
	    # 4: IR Lock/Unlock
	    #
	    # '0..10' == Pcal ON or OFF + Green ON + IR ON or OFF + Green lock ON or Off + IR lock OFF

	    # laser_state=`echo $fname | awk -F_ '{print $3}'`
	    # echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] laser state = ${laser_state}" >> ${LOGFILE} 2>&1
	    # if [ `echo "$laser_state" | grep '0.01.'` ]; then
	    # #if [ `echo "$laser_state" | grep '^0111'` ] || [ `echo "$laser_state" | grep '^0101'` ]; then
	    # 	#
	    # 	# process for fitting mirror edge (Yuzu)
	    # 	# only when Green laser illumites the mirror,  this process will start.
	    # 	#
	    # 	echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process for mirror edge fitting started.">> ${LOGFILE} 2>&1
	    # 	echo "fname = ${fname}" >> ${LOGFILE} 2>&1
	    # 	/home/controls/bin/fit_Tcam.py $fname >> ${LOGFILE} 2>&1
	    # 	echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process for mirror edge fitting done.">> ${LOGFILE} 2>&1
		
	    # 	#
	    # 	# process for updaing plot for trend monitor (YamaT)
	    # 	#
	    # 	echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process to update trend monitor started.">> ${LOGFILE} 2>&1
	    # 	OPTIC=`echo $fname | awk -F/ '{print $4}'`
	    # 	echo "optic = $OPTIC" >> ${LOGFILE} 2>&1
	    # 	/home/controls/bin/MirrorPositionWebPlot.sh ${OPTIC} >> ${LOGFILE} 2>&1
	    # 	echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process to update trend monitor done.">> ${LOGFILE} 2>&1

	    # else
	    # 	echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] this TCam photo is unappropriate for fitting (we require the laser state 01.1. , but the laser state was ${laser_state})">> ${LOGFILE} 2>&1
	    # fi
	    
	    
	    #
	    # script for main beam positiona (Yuzu)
	    #
	    # '...1.' == any Pcal status + Green status ON + any IR 
	    #  or
	    # '..1..' == any Pcal status + any Green status + IR ON
	    if [ `echo "$laser_state" | grep '..1..'` ] || [ `echo "$laser_state" | grep '...1.'` ];
	    then
		sleep 1
		#
		# process for finding IR center (Yuzu)
		# only when IR laser power is larger than pre-defined threshold, this process will start.
		#
		echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process for identifying beam center started.">> ${LOGFILE} 2>&1
		echo "fname = ${fname}" >> ${LOGFILE} 2>&1
		/home/controls/bin/fit_beam_center.py $fname >> ${LOGFILE} 2>&1
		echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] process for identifying beam center done.">> ${LOGFILE} 2>&1
	    fi
		
	}||{
	    echo "[ `date +"%Y/%m/%d %H:%M:%S"` ] $extension is out of support." >> ${LOGFILE} 2>&1
	}
	
    done
#
# end of inotifywait
#
