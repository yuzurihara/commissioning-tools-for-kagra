#!/bin/bash

#list="`ls /data2/TCam/ETMX/2022/0330/TCam_ETMX_*png` `ls /data2/TCam/ETMX/2022/0331/TCam_ETMX_*png` `ls /data2/TCam/ETMX/2022/0401/TCam_ETMX_*png` `ls /data2/TCam/ETMX/2022/0404/TCam_ETMX_*png`"
#list=`ls /data2/TCam/ETMX/2022/0328/TCam_ETMX_*png`
#list=`ls /data2/TCam/ETMX/2022/0412/TCam_ETMX_*png`
#list=`ls /data2/TCam/ITMX/2022/0323/TCam_*png`
#list=`ls /data2/TCam/ETMY/archive/2022/0516/TCam_*png`
#list=`ls /data2/TCam/ETMX/archive/2022/0524/TCam_*png`
#list=`ls /data2/TCam/ITMX/archive/2022/0617/TCam_*png`
#list=`ls /data2/TCam/ETMX/archive/2022/0727/TCam_*png`
#list=`ls /data2/TCam/ETMY/archive/2022/0615/TCam_*png`
#list=`ls /data2/TCam/ETMX/archive/2022/0828/TCam_*png | sort -t_ -k6`

#list=`ls ./TCam_*[0-9].png | sort -t_ -k6`
#list=`ls /Users/yuzurihara/Dropbox/ongoing/TCam/ETMY_cooling/1204/*.png`
#list=`ls /Users/yuzurihara/Dropbox/ongoing/TCam/ETMY_cooling/*/*.png`

#list=`ls /Volumes/knock_SSD/TCam/orig/2023/0818/TCam_ITMX*.png | sort -t_ -k5`
#list=`ls ./TCam_fig/TCam*ETMX*[0-9].png | sort -t_ -k5`
#list=`ls ./TCam*ETMX*.png | sort -t_ -k5`
#list=`ls /Volumes/knock_SSD/TCam/orig/2023/102[1-9]/TCam_*ETMX*.png | sort -t_ -k5`
#list=`ls /Volumes/knock_SSD/TCam/orig/2023/1023/TCam_*ETMX*.png | sort -t_ -k5`
#list=`ls /Volumes/knock_SSD/TCam/orig/2023/1020/TCam_*.png | sort -t_ -k5`
list=`ls /Volumes/knock_SSD/TCam/orig/2023/1222/*ITMX*.png | sort -t_ -k5`

for fname in $list
do
    echo $fname
    dir=`dirname $fname | sed -e "s/orig/fit/g"`
    mkdir -p $dir

    # /Volumes/knock_SSD/TCam/orig/2023/1228/TCam_ITMX_00011_2023_1228_073554.png
    day=`echo $fname | awk -F"TCam" '{print $3}' | awk -F_ '{print $4$5}'`
    #echo $day

    # laser_state=`echo $fname | awk -F_ '{print $3}'`
    # #print(laser_state)
    # if [ `echo "$laser_state" | grep '0..1.'` ]; then
    # 	/home/controls/bin/fit_Tcam.py $fname
    # fi
    #./fit_Tcam.py -i $fname -c -r
    #./fit_Tcam.py -i $fname -r 
    #./fit_Tcam.py -i $fname -c

    [[ $day > 20231221 ]] && {
	./fit_Tcam.py -i $fname
    } || {
	./fit_Tcam_20220324_20231221.py -i $fname
    }


    mv *.png $dir/
    mv *.txt $dir/    
    
    #if [ `echo "$laser_state" | grep '1.1..'` ]; then
    #./fit_beam_center.py $fname
    #fi
done
