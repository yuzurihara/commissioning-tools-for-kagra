import numpy as np
import pandas as pd
import os
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

# Gaussian (clipped)
def ClippedGaussian(x, AMP, WID, CNT, BGD):
    y = AMP * np.exp(-(x-CNT)*(x-CNT)/(2*WID*WID)) + BGD
    y = np.clip(y, None, 255)
    return y

def ClippedGaussian2(x, AMP, WID, CNT, BGD):
    y = AMP * np.exp(-(x-CNT)*(x-CNT)/(2*WID*WID))
    y = np.clip(y, None, 255)
    return y

# Gaussian (clipped)
def Gaussian(x, AMP, WID, CNT, BGD):
    y = AMP * np.exp(-(x-CNT)*(x-CNT)/(2*WID*WID)) + BGD
    #y = np.clip(y, None, 255)
    return y

#define model function and pass independant variables x and y as a list
def twoD_Gaussian(XY, amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
  x,y =  XY[0:2]
  xo  =  float(xo)
  yo  =  float(yo)
  a   =  (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
  b   = -(np.sin(2*theta)) /(4*sigma_x**2) + (np.sin(2*theta)) /(4*sigma_y**2)
  c   =  (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
  g   =  offset + amplitude*np.exp(-(a*((x-xo)**2)+2*b*(x-xo)*(y-yo)+c*((y-yo)**2)))
  return g.ravel()

#x_env = np.abs(signal.hilbert(x))f = np.fft.fft2(img)                        # 2Dフーリエ変換

def twoD_HigPass(img):
  f = np.fft.fft2(img)                        # 2Dフーリエ変換
  f_shift = np.fft.fftshift(f)                # 直流成分を画像中心に移動させるためN/2シフトさせる
  mag = 20 * np.log(np.abs(f_shift))          # 振幅成分を計算
   
  # 周波数領域にマスクをかける
  rows, cols = img.shape                      # 画像サイズを取得
  crow, ccol = int(rows / 2), int(cols / 2)   # 画像中心を計算
  mask  = 50                                  # マスクのサイズ
  f_shift[crow-mask:crow+mask, ccol-mask:ccol+mask] = 0 # high-pass
   
  # 2D逆フーリエ変換によりフィルタリング後の画像を得る
  f_ishift = np.fft.ifftshift(f_shift)         # シフト分を元に戻す
  img_back = np.fft.ifft2(f_ishift)            # 逆フーリエ変換
  img_back = np.abs(img_back)                  # 実部を計算する
  return img_back

def twoD_LowPass(img):
  f = np.fft.fft2(img)                         # 2Dフーリエ変換
  f_shift = np.fft.fftshift(f)                 # 直流成分を画像中心に移動させるためN/2シフトさせる
  mag = 20 * np.log(np.abs(f_shift))           # 振幅成分を計算
   
#   # 周波数領域にマスクをかける
#   rows, cols = img.shape                       # 画像サイズを取得
#   crow, ccol = int(rows / 2), int(cols / 2)    # 画像中心を計算
#   rmask = 10                                   # マスクのサイズ
#   cmask = 10                                   # マスクのサイズ 
# #  f_shift[crow:ccol]                       = 0 # 直流成分を落とす
#   f_shift[0:crow-cmask,0:ccol-rmask]       = 0 # 高周波成分を落とす
#   f_shift[0:crow-cmask,ccol+rmask:cols]    = 0 # 高周波成分を落とす
#   f_shift[crow+cmask:rows,0:ccol-rmask]    = 0 # 高周波成分を落とす
#   f_shift[crow+cmask:rows,ccol+rmask:cols] = 0 # 高周波成分を落とす

  # 周波数領域にマスクをかける
#  rows, cols = img.shape                       # 画像サイズを取得
#  crow, ccol = int(rows / 2), int(cols / 2)    # 画像中心を計算
#  mask = np.zeros_like(img)
#  sigma_x = 5
#  sigma_y = 5
#  for x in range(0,rows):
#      for y in range(0,cols):
#          mask[x,y] = np.exp(-(x-crow)**2/(2*sigma_x**2)-(y-ccol)**2/(2*sigma_y**2))

  # print(img.shape)
  y_max, x_max = img.shape                       # 画像サイズを取得
  xc, yc = int(x_max / 2), int(y_max / 2)    # 画像中心を計算

  x = np.arange(0, x_max)
  y = np.arange(0, y_max)
  mask = np.zeros(x_max * y_max).reshape(y_max, x_max)

  sigma_x = 2.0 * 5**2
  sigma_y = 2.0 * 5**2
    
  for i in range(y_max):
    mask[i, 0:] = np.exp( - (x[0:]-xc)**2/(sigma_x) - (y[i]-yc)**2/(sigma_y) )

  f_shift=f_shift*mask
   
  # 2D逆フーリエ変換によりフィルタリング後の画像を得る
  f_ishift = np.fft.ifftshift(f_shift)        # シフト分を元に戻す
  img_back = np.fft.ifft2(f_ishift)           # 逆フーリエ変換
  img_back = np.abs(img_back)                 # 実部を計算する
  return img_back


def get_fitting_data(img, img_ftr, ax_slice, x_slice):
    if   ax_slice == 0:
        ydata     =     img[int(x_slice),:]
        ydata_ftr = img_ftr[int(x_slice),:]
    elif ax_slice == 1:
        ydata     =     img[:,int(x_slice)]
        ydata_ftr = img_ftr[:,int(x_slice)]
    xdata = np.arange(len(ydata))
    index = np.ones(len(ydata), dtype=bool)
    index[ydata > ydata_ftr] = False                     # ydata > ydata_ftr の部分のデータを落とす
    return xdata, ydata, index
 
def get_peak(xdata_fit, ydata_fit):
    max_idx  = xdata_fit[np.argmax(ydata_fit)]
    ave_idx1 = xdata_fit[np.where( ydata_fit > np.average(ydata_fit))][0]
    ave_idx2 = xdata_fit[np.where( ydata_fit > np.average(ydata_fit))][-1]
    
#    if np.max(ydata_fit) < data_quality:
#        print('no IR beam spot seen in this slice {:d}'.format(int(x_slice[i_slice])))
#        continue
#    if max_idx < ave_idx1 or ave_idx2 < max_idx:
#        print('multiple peaks found in this slice {:d}'.format(int(x_slice[i_slice])))
#        continue
 
    #フィットする範囲
    AMP0 = np.max(ydata_fit)
    WID0 = ave_idx2-ave_idx1
    CNT0 = max_idx
    BGD0 = np.average(ydata_fit)

    #初期値を決める#####################################
    #中心の初期値を求めるために配列のmaxカウントの位置を抜き出す
#    print('fit range:(min, max, initial center) = ({:d}, {:d}, {:d})'.format(ave_idx1, ave_idx2, max_idx))

    p0 = [AMP0, WID0, CNT0, BGD0] #初期値 [amplitude, width, center, background]
    bounds=([AMP0/10., WID0/10., ave_idx1, 0], [AMP0*10, WID0*2, ave_idx2, BGD0*2]) # パラメーターの下限値と上限値
    popt, pcov = curve_fit(ClippedGaussian, xdata_fit, ydata_fit, p0=p0, bounds=bounds)
    return popt, pcov

def get_average(fit_results):
    FIT_POS = [r[0] for r in fit_results]
    FIT_CTR = [r[5] for r in fit_results] #これがcenterのパラメーター
    ERR_CTR = [r[6] for r in fit_results]
 
    df2 = pd.DataFrame(list(zip(FIT_POS, FIT_CTR)), columns = ['X_LINE','FIT_CTR'])
    para3 = 1.2
    
    # # # 四分位数、四分位範囲
    q1  = df2['FIT_CTR'].quantile(.25)
    q3  = df2['FIT_CTR'].quantile(.75)
    iqr = q3 - q1
    limit_low  = q1 - iqr * para3
    limit_high = q3 + iqr * para3
    df3 = df2.query('@limit_low < FIT_CTR < @limit_high')
    ave = np.average(df3['FIT_CTR'])
    var = np.sqrt(np.var(df3['FIT_CTR']))

    return ave, var

