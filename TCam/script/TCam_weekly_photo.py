#!/usr/bin/env python3
#******************************************#
#     File Name: TCam_weekly_photo.py
#        Author: Hirotaka Yuzurihara
# Last Modified: 2023/07/10
#******************************************# 

import ezca as ez
import time
import subprocess
import time
import sys
import argparse
import datetime

#####
#
# internal parameters
#
t_margin = 0.4 # [s]
t_margin_IR_shutter = 4 # [s]
exposures_i = [15000000, 15000000, 10000000] # 10s 15s
exposures_e = [10000000, 10000000, 5000000] # 10s 5s

### for test
# exposures_i = [1000000, 1500000] # 1s 1.5s
# exposures_e = [1000000, 500000] # 1s 5s
###########

duration_wait_for_green = 300

flag_change_pcalx = False
flag_change_pcaly = False
pre_state_pcalx = None
pre_state_pcaly = None

flag_change_LSC_LOCK = False
pre_state_LSC_LOCK = None

# flag_EX = True
# flag_EY = True
# flag_IXY = True
flag_EX = False
flag_EY = False
flag_IXY = False

optics = ['ixy', 'ex', 'ey', 'all']

###############################################################


######
#
# main
#
parser = argparse.ArgumentParser(prog="TCam_weekly_photo.py",
                                 description="This script takes the TCam image for weekly photo session.")
parser.add_argument('-o', '--optic', default='all',
                    choices = optics,
                    help = 'name of mirror to take images %s' % optics)
args = parser.parse_args()
optic = args.optic
if len(optic) == 0 or optic == 'all':
    flag_EX = True
    flag_EY = True
    flag_IXY = True

elif optic == 'ex':
    flag_EX = True

elif optic == 'ey':    
    flag_EY = True

elif optic == 'ixy':    
    flag_IXY = True    

#print(optic, flag_EX, flag_EY, flag_IXY)
#sys.exit(1)


ezca = ez.Ezca()

# 1.LSC_LOCK DOWN (save current state)
# 2.recude pcal power (save current state), if power is already low, do nothing
# 3.Green X, check the state
# 4.take image
# 5.DOWN
# 6.ASC_RESET...
# 7.GreenY
# 8.take image
# 9.request the original state to pcal guardian
# 10.LSC_LOCK request the original state

######
#
# notification for slack
#
t_now = datetime.datetime.now()
#jst_now = dt_now + datetime.timedelta(hours=9)
dt_now = t_now.strftime('%Y/%m/%d %H:%M:%S')
channel = "tcam"
msg = "%s JST   TCam script started" % dt_now
cmd = ['/kagra/bin/slack', channel, msg]
subprocess.Popen(cmd)

######
#
# check beam shutter
#
if ezca["ALS-GRX_BEAM_SHUTTER"] == 0 or ezca["ALS-GRY_BEAM_SHUTTER"] == 0:
    print('error : check status of laser shutter for Green. contact the expert')
    sys.exit(1)

######
#
# pre-treatment on guardian (LSC_LOCK/pcal)
#
pre_state_LSC_LOCK = ezca['GRD-LSC_LOCK_REQUEST']
# if pre_state_LSC_LOCK != 'DOWN':
#     flag_change_LSC_LOCK = True
ezca["GRD-LSC_LOCK_REQUEST"] = 'DOWN'
while ezca["GRD-LSC_LOCK_STATE"] != 'DOWN':
    pass
print('Done : requested LSC_LOCK state to DOWN')


#######
#
# ETMX
#
if flag_EX:
    print('#')
    print('# TCam snapshot for EX')
    print('#')
    time_beg = time.time()    

    pre_state_pcalx = ezca['GRD-CAL_PCAL_EX_REQUEST']
    if ezca['GRD-CAL_PCAL_EX_REQUEST_N'] > 20:
        flag_change_pcalx = True
        #flag_state_pcalx = True
        ezca['GRD-CAL_PCAL_EX_REQUEST'] = 'OFS_CLOSED_LOW_P'
        while ezca['GRD-CAL_PCAL_EX_STATE'] != 'OFS_CLOSED_LOW_P':
            pass
        print('Done : requested pcal-x guardian to low-power mode from %s' % pre_state_pcalx)

    for optic in ['PR3', 'ITMX', 'ETMX']:
        output=subprocess.check_output('/users/Commissioning/scripts/reset_ASC_feedback.py -i FPMI -o %s' % optic, shell=True)
        #print(output)

    ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'GRX_LOCKED'
    while ezca["GRD-INITIAL_ALIGNMENT_STATE"] != 'GRX_LOCKED':
        time_end = time.time()
        duration = time_end - time_beg
        if duration > duration_wait_for_green:
            print('waiting for Green laser more than 300 s. please contact the expert or Yuzurihara')
            sys.exit(1)            
            #ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'DOWN'
            # output=subprocess.check_output('python3 /users/ushiba/script/python/search_GRX_alignment.py', shell=True)
            # ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'GRX_LOCKED'
            # while ezca["GRD-INITIAL_ALIGNMENT_STATE"] != 'GRX_LOCKED':
            #     pass
            # break
        #pass
    print('Done : requested GRX_LOCKED')


    j=1
    n_total=len(exposures_e)
    for exposure in exposures_e:
        for PLACE in ['EXA']:
            ezca['CAM-%s_EXP'  % PLACE] = exposure
            ezca['CAM-%s_GAIN' % PLACE] = 470
            ezca['GRD-CAM_%s_REQUEST' % PLACE] = 'SNAPSHOT'

            while not (ezca['GRD-CAM_EXA_STATE'] == 'SNAPSHOT'):
                pass

            print('Taking TCam images for %s with exposure %d s (%d/%d)' % (PLACE, exposure, j, n_total))

            while not (ezca['GRD-CAM_EXA_STATE'] == 'DOWN'):
                pass

        print('Done : Taking TCam images for %s with exposure %d s (%d/%d)' % (PLACE, exposure, j, n_total))        
        j=j+1

    ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'DOWN'
    while ezca["GRD-INITIAL_ALIGNMENT_STATE"] != 'DOWN':
        pass        

    if flag_change_pcalx:
        ezca['GRD-CAL_PCAL_EX_REQUEST'] = pre_state_pcalx
        print("requested %s state for GRD-CAL_PCAL_EX" % pre_state_pcalx)
        while not (ezca['GRD-CAL_PCAL_EX_STATE'] == pre_state_pcalx):
            pass

        
#######
#
# ETMY
#

if flag_EY:
   
    print('#')
    print('# TCam snapshot for EY')
    print('#')

    time_beg = time.time()
    
    pre_state_pcaly = ezca['GRD-CAL_PCAL_EY_REQUEST']
    print('GRD-CAL_PCAL_EY_REQUEST = %s' % pre_state_pcaly)
    if ezca['GRD-CAL_PCAL_EY_REQUEST_N'] > 20:
        flag_change_pcaly = True
        #flag_state_pcaly = True
        ezca['GRD-CAL_PCAL_EY_REQUEST'] = 'OFS_CLOSED_LOW_P'
        while ezca['GRD-CAL_PCAL_EY_STATE'] != 'OFS_CLOSED_LOW_P':
            pass
        print('Done : requested pcal-y guardian to low-power mode from %s' % pre_state_pcaly)

    
    ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'DOWN'
    time.sleep(t_margin)    
    while ezca["GRD-INITIAL_ALIGNMENT_STATE"] != 'DOWN':
        pass

    ezca["GRD-LSC_LOCK_REQUEST"] = 'RESET_ASC_FEEDBACK'
    time.sleep(t_margin)    
    while ezca["GRD-LSC_LOCK_STATE"] != 'RESET_ASC_FEEDBACK':
        pass

    ezca["GRD-LSC_LOCK_REQUEST"] = 'DOWN'
    while ezca["GRD-LSC_LOCK_STATE"] != 'DOWN':
        pass

    ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'GRY_LOCKED'
    while ezca["GRD-INITIAL_ALIGNMENT_STATE"] != 'GRY_LOCKED':
        time_end = time.time()
        duration = time_end - time_beg
        if duration > duration_wait_for_green:
            print('waiting for Green laser more than 300 s. please contact the expert or Yuzurihara')
            sys.exit(1)
            #pass
    print('Done : requested GRY_LOCKED state')

    j=1
    n_total=len(exposures_e)
    for exposure in exposures_e:
        for PLACE in ['EYA']:
            ezca['CAM-%s_EXP'  % PLACE] = exposure
            ezca['CAM-%s_GAIN' % PLACE] = 470
            ezca['GRD-CAM_%s_REQUEST' % PLACE] = 'SNAPSHOT'

            while not (ezca['GRD-CAM_EYA_STATE'] == 'SNAPSHOT'):
                pass

            print('Taking TCam images for %s with exposure %d s (%d/%d)' % (PLACE, exposure, j, n_total))
            while not (ezca['GRD-CAM_EYA_STATE'] == 'DOWN'):
                pass

        print('Done : Taking TCam images for %s with exposure %d s (%d/%d)' % (PLACE, exposure, j, n_total))        
        j=j+1

    ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'DOWN'
    while ezca["GRD-INITIAL_ALIGNMENT_STATE"] != 'DOWN':
        pass        
            
            
    #######
    #
    # after-treatment on guardian (pcal)
    #
    if flag_change_pcaly:
        ezca['GRD-CAL_PCAL_EY_REQUEST'] = pre_state_pcaly
        print("requested %s state for GRD-CAL_PCAL_EY" % pre_state_pcaly)        
        while not (ezca['GRD-CAL_PCAL_EY_STATE'] == pre_state_pcaly):
            pass
    
#######
#
# ITMX/ITMY
#
if flag_IXY:
    print('#')
    print('# TCam snapshot for IX/IY')
    print('#')

    ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] = 'DOWN'
    while ezca["GRD-INITIAL_ALIGNMENT_REQUEST"] != 'DOWN':
        pass

    ezca["GRD-IO_REQUEST"] = 'PSL_LOCKED'
    print('requested PSL_LOCKED state to GRD-IO_REQUEST')
    while ezca["GRD-IO_STATE"] != 'PSL_LOCKED':
        pass
    print('Done : requested PSL_LOCKED state to GRD-IO_REQUEST')

    #
    # 0 : IR shutter closes
    # 1 : IR shutter open
    #
    ezca["PSL-BEAM_SHUTTER"] = 0
    time.sleep(t_margin_IR_shutter)
    print('IR shutter was closed')

    #
    # set configurations for taking pictures of ITMX and ITMY
    #
    j=1
    n_total=len(exposures_i)
    for exposure in exposures_i:
        for PLACE in ['IXA', 'IYA']:
            ezca['CAM-%s_EXP'  % PLACE] = exposure
            ezca['CAM-%s_GAIN' % PLACE] = 470
            ezca['GRD-CAM_%s_REQUEST' % PLACE] = 'SNAPSHOT'                
            print('Taking TCam images for ITMX and ITMY with exposure %d s (%d/%d)' % (exposure, j, n_total))

        time.sleep(t_margin_IR_shutter)
        while not (ezca['GRD-CAM_IXA_STATE'] == 'DOWN' and ezca['GRD-CAM_IYA_STATE'] == 'DOWN'):
            pass
        print('Done : Take TCam images for ITMX and ITMY with exposure %d s (%d/%d)' % (exposure, j, n_total))
        j=j+1

    #
    # 0 : IR shutter closes
    # 1 : IR shutter open
    #
    ezca["PSL-BEAM_SHUTTER"] = 1
    print('opened IR shutter')

    ezca['GRD-IO_REQUEST'] = 'PROVIDING_STABLE_LIGHT'
    time.sleep(t_margin)
    while ezca['GRD-IO_STATE'] != 'PROVIDING_STABLE_LIGHT':
        pass
    print('Done : requested PSL_LOCKED state to GRD-IO_REQUEST')


#######
#
# after-treatment on guardian (LSC_LOCK)
#
ezca["GRD-LSC_LOCK_REQUEST"] = 'RESET_ASC_FEEDBACK'
time.sleep(t_margin)
while ezca["GRD-LSC_LOCK_STATE"] != 'RESET_ASC_FEEDBACK':
    pass

#if flag_change_LSC_LOCK:
ezca["GRD-LSC_LOCK_REQUEST"] = pre_state_LSC_LOCK


######
#
# notification for slack
#
t_now = datetime.datetime.now()
#jst_now = dt_now + datetime.timedelta(hours=9)
dt_now = t_now.strftime('%Y/%m/%d %H:%M:%S')
channel = "tcam"
msg = "%s JST   TCam script done !" % dt_now
cmd = ['/kagra/bin/slack', channel, msg]
subprocess.Popen(cmd)    
