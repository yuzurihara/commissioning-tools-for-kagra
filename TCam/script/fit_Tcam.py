#!/usr/bin/env python
# coding: utf-8

####################################################
#
# 2022/3/24~ fitting and finding center of mirros (ITMX, ETMX, ...)
# developped by H. Yuzurihara, A. Toriyama
# contributed by S. Tanaka, T. Yokozawa, T. Ushiba ,T. Yamamoto, Dan Chen
#
####################################################
#
# update history
#
# 11 Jan 2024 : separated the code. To analyze the image of 2023/12/22~, please use this code. For older images, use `fit_Tcam_20220324_20231221.py`
#
# 4 April 2022 : integrated the fitting code for ETMX and ITMX
#
# 25 March 2022 : first version
#
####################################################

import cv2
import numpy as np
from matplotlib import pyplot as plt
import os
from scipy import odr # for fitting
import sys
import datetime
import glob
import shutil
import subprocess

from csv import writer
import pandas as pd
import argparse

######################################
# Option for bonus figures
######################################
flag_output = True
flag_output_fitting = False

flag_update_reference = False
flag_add_csv          = False

r_pre = 10 # global variable
area_th = 15

######################################
# Parameters (pre-defined) for ETMX
######################################
ETMX_threshold_low  = 170
ETMX_threshold_high = 200

ETMX_x_pre=1919
ETMX_y_pre=1350
#ETMX_r_pre=1245
ETMX_r_pre=1243 # 金尺試験での値を採用 https://klog.icrr.u-tokyo.ac.jp/osl/?r=28830

# crop reagion
#ETMX_r_pre_small = int(ETMX_r_pre*0.99)
#ETMX_r_pre_large = int(ETMX_r_pre*1.01)
# ETMX_r_pre_small = int(ETMX_r_pre*0.98) # 1220
# ETMX_r_pre_large = int(ETMX_r_pre*1.02) # 1269 => +/- 2.2 mm
ETMX_r_pre_small = int(ETMX_r_pre*0.985) # 1226
ETMX_r_pre_large = int(ETMX_r_pre*1.015) # 1263

# reagion to remove from fitting
#ETMX_height_ratios =[0.0, 0.30, 0.4, 0.78, 0.88, 1.0]
ETMX_height_ratios = [0.0, 0.30, 0.39, 0.80, 0.88, 1.0]
# if area is larger than this threshold, remove it from fitting

######################################
# Parameters (pre-defined) for ETMY
######################################
ETMY_x_pre=2230
ETMY_y_pre=965
ETMY_r_pre=1248

# crop reagion
#ETMY_r_pre_small = int(ETMY_r_pre*0.99)
#ETMY_r_pre_large = int(ETMY_r_pre*1.01)
ETMY_r_pre_small = int(ETMY_r_pre*0.98) # 1220
ETMY_r_pre_large = int(ETMY_r_pre*1.02) # 1269 => +/- 2.2 mm
ETMY_r_pre_small2 = int(ETMY_r_pre*0.98) # 1220
ETMY_r_pre_large2 = int(ETMY_r_pre*1.02) # 1269 => +/- 2.2 mm

# reagion to remove from fitting
#ETMY_height_ratios =[0.0, 0.20, 0.30, 0.78, 0.88, 1.0]
#ETMY_height_ratios =[0.0, 0.20, 0.30, 0.54, 0.64, 0.74]
#ETMY_height_ratios  =[0.0, 0.20, 0.30, 0.54, 0.64, 0.74]
#ETMY_height_ratios  =[0.0, 0.20, 0.30, 0.64, 0.64, 0.74]
ETMY_height_ratios  = [0.0, 0.20, 0.30, 0.64, 0.64, 0.76] # 0.64と0.64が同じなので、実際は0.3~0.78を塗りつぶしている
ETMY_width_ratios   = [0.48, 0.55, 0.7, 1.0]
#ETMY_width_ratios = [0.48, 0.55, 0.7, 0.73, ]
#533/2831 = 0.18
#686/2831 = 0.24
#1540/2831=0.54
#1830/2831=0.64
#2112/2831=0.74
#2000/4130=0.48
# if area is larger than this threshold, remove it from fitting
# ここで囲まれた領域のデータを使う

ETMY_height_ratios2 =[0.09, 0.20]
#ETMY_height_ratios2 =[0.15, 0.28]
ETMY_width_ratios2 =[0.78, 0.82]

ETMY_HSV_lower=[30, 60, 90]
ETMY_HSV_upper=[255, 210, 255]

ETMY_HSV_lower2=[30, 90, 30]
ETMY_HSV_upper2=[254, 254, 150]
# ETMY_HSV_lower2=[30, 90, 10]
# ETMY_HSV_upper2=[254, 254, 254]

#flag_hsv2 = False
flag_hsv2 = True

ETMY_threshold_low  = 100
ETMY_threshold_high = 140
ETMY_threshold_low2  = 30 # 右上を捉えるためにはこれが大事
ETMY_threshold_high2 = 90 # 右上を捉えるためにはこれが大事

######################################
# Parameters (pre-defined) for ITMX
######################################
ITMX_threshold_low  = 20
ITMX_threshold_high = 250

ITMX_x_pre = 1825
ITMX_y_pre = 1363
ITMX_r_pre = 630

# リングでmaskする際の幅(27ピクセル = 4.5mm)
# 関数の中で上書きする
ITMX_r_pre_large = 0
ITMX_r_pre_small = 0

# 2022/03 ~ 2023/07/25
# ITMX_HSV_lower=[21, 67, 230]
# ITMX_HSV_upper=[164, 210, 254]

# 2023/07/26~
# ITMX_HSV_lower=[30, 90, 120]
# ITMX_HSV_upper=[164, 200, 150]

# 2023/12/22 ~
ITMX_HSV_lower=[21, 67, 200]
ITMX_HSV_upper=[164, 200, 254]

ITMX_height_ratios =[0.0, 0.60, 0.73, 1.0]
ITMX_width_ratios =[0.43, 0.49]

######################################
# Parameters (pre-defined) for ITMY
######################################
ITMY_threshold_low  = 20
ITMY_threshold_high = 250

ITMY_x_pre = 2451
ITMY_y_pre = 1273
ITMY_r_pre = 640

# リングでmaskする際の幅(27ピクセル = 4.5mm)
# 関数の中で上書きする
ITMY_r_pre_large = 0
ITMY_r_pre_small = 0

ITMY_HSV_lower=[21, 67, 230]
ITMY_HSV_upper=[164, 210, 254]

ITMY_height_ratios =[0.0, 0.57, 0.62, 0.66]
#ITMY_height_ratios =[0.0, 0.66]


######################################
# define function
######################################

#
# フィットの初期値を時刻に応じて割り当てる
#
def get_pre_value_for_fitting(mirror_name, figname):

    tmp = figname.split(".")[0].split("_")[3:5]
    date = int("".join(tmp))
    times = int(figname.split(".")[0].split("_")[5])

    global ETMX_x_pre, ETMX_y_pre, ETMX_r_pre
    global ETMY_x_pre, ETMY_y_pre, ETMY_r_pre
    global ITMX_x_pre, ITMX_y_pre, ITMX_r_pre
    global ITMY_x_pre, ITMY_y_pre, ITMY_r_pre
    global ETMX_threshold_low, ETMX_threshold_high
    global ETMY_threshold_low, ETMY_threshold_high
    global ETMY_threshold_low2, ETMY_threshold_high2    
    global ITMX_threshold_low, ITMX_threshold_high
    global ITMY_threshold_low, ITMY_threshold_high    
    global ETMX_r_pre_small, ETMX_r_pre_large
    global ETMY_r_pre_small, ETMY_r_pre_large
    global ETMY_r_pre_small2, ETMY_r_pre_large2    
    global ITMX_r_pre_small, ITMX_r_pre_large
    global ITMY_r_pre_small, ITMY_r_pre_large
    global ETMY_height_ratios
    
    if mirror_name == "ETMX":
        if 20231222 <= date < 20240423:
            ETMX_x_pre=1955
            ETMX_y_pre=1370
            ETMX_r_pre=1246
        elif 20240423 <= date < 20240612:
            ETMX_x_pre=1945
            ETMX_y_pre=1395
        elif 20240612 <= date < 20240809:
            ETMX_x_pre=1945
            ETMX_y_pre=1410
        elif 20240809 <= date < 20240816:
            ETMX_x_pre=1970
            ETMX_y_pre=1420
        elif 20240816 <= date < 20240927:
            ETMX_x_pre=1975
            ETMX_y_pre=1425
        elif 20240927 <= date < 20241008:
            ETMX_x_pre=1960
            ETMX_y_pre=1433
        elif 20241008 <= date < 20241213:
            ETMX_x_pre=1950
            ETMX_y_pre=1433
        elif 20241213 <= date < 20241219:
            ETMX_x_pre=1930
            ETMX_y_pre=1433                        
        if 20241219 == date and times < 103000:
            ETMX_x_pre=1900
            ETMX_y_pre=1395            
        if 20241219 == date and times >= 103000: 
            ETMX_x_pre=1900
            ETMX_y_pre=1405
        if 20241223 == date and times < 100000:
            ETMX_x_pre=1880
            ETMX_y_pre=1390           
        if 20241223 == date and times >= 100000: 
            ETMX_x_pre=1880
            ETMX_y_pre=1398
        if 20241226 == date and times < 90000:
            ETMX_x_pre=1880
            ETMX_y_pre=1390           
        if 20241226 == date and times >= 90000: 
            ETMX_x_pre=1880
            ETMX_y_pre=1398            
        if 20241230 == date and times < 90000:
            ETMX_x_pre=1880
            ETMX_y_pre=1390           
        if 20241230 == date and times >= 90000: 
            ETMX_x_pre=1880
            ETMX_y_pre=1392
        if 20250107 == date and times < 90000:
            ETMX_x_pre=1880
            ETMX_y_pre=1386           
        if 20250107 == date and times >= 90000: 
            ETMX_x_pre=1880
            ETMX_y_pre=1388
        if 20250107 < date < 20250131:
            ETMX_x_pre=1880
            ETMX_y_pre=1388
        if 20250131 <= date < 20260107:
            ETMX_x_pre=1880
            ETMX_y_pre=1363
            
        ETMX_r_pre_small = int(ETMX_r_pre*0.985) # 1226
        ETMX_r_pre_large = int(ETMX_r_pre*1.015) # 1263    
        print(ETMX_r_pre, ETMX_y_pre, ETMX_r_pre)
        return ETMX_x_pre, ETMX_y_pre, ETMX_r_pre
    
    if mirror_name == "ETMY":
        if 20240315 <= date < 20240624:
            ETMY_x_pre=2230
            ETMY_y_pre=1080
        if 20240624 <= date < 20240705:
            ETMY_x_pre=2235
            ETMY_y_pre=1040
        if 20240705 <= date < 20240816:
            ETMY_x_pre=2216
            ETMY_y_pre=1030
        if 20240816 <= date < 20240906:
            ETMY_x_pre=2216
            ETMY_y_pre=1050
        if 20240906 <= date < 20240927:
            ETMY_x_pre=2236
            ETMY_y_pre=1020
        if 20240927 <= date < 20241017:
            ETMY_x_pre=2226
            ETMY_y_pre=1035
        if 20241017 <= date < 20241213:
            ETMY_x_pre=2220
            ETMY_y_pre=1025
        if 20241213 <= date < 20241219:
            ETMY_x_pre=2220
            ETMY_y_pre=1040
        if 20241219 == date and times < 103000:
            ETMY_x_pre=2220
            ETMY_y_pre=1040
        if 20241219 == date and times >= 103000: 
            ETMY_x_pre=2220
            ETMY_y_pre=1035
        if 20241223 == date and times < 100000:
            ETMY_x_pre=2215
            ETMY_y_pre=1030
        if 20241223 == date and times >= 100000: 
            ETMY_x_pre=2215
            ETMY_y_pre=1035
        if 20241226 == date and times < 90000:
            ETMY_x_pre=2215
            ETMY_y_pre=1030
        if 20241226 == date and times >= 90000: 
            ETMY_x_pre=2215
            ETMY_y_pre=1035
        if 20241230 == date and times < 90000:
            ETMY_x_pre=2222
            ETMY_y_pre=1030
        if 20241230 == date and times >= 90000: 
            ETMY_x_pre=2225
            ETMY_y_pre=1035
        if 20250107 == date and times < 90000:
            ETMY_x_pre=2222
            ETMY_y_pre=1030
        if 20250107 == date and times >= 90000: 
            ETMY_x_pre=2222
            ETMY_y_pre=1035
        if 20250107 < date:
            ETMY_x_pre=2222
            ETMY_y_pre=1035                                    
            
        ETMY_r_pre_small = int(ETMY_r_pre*0.985) # 1226
        ETMY_r_pre_large = int(ETMY_r_pre*1.015) # 1263
        ETMY_r_pre_small2 = int(ETMY_r_pre*0.999) # 右上の縁の内側が不要なので、あえて~範囲を小さくする
        ETMY_r_pre_large2 = int(ETMY_r_pre*1.010) # 
        print(ETMY_x_pre, ETMY_y_pre)
        return ETMY_x_pre, ETMY_y_pre, ETMY_r_pre

    if mirror_name == "ITMX":
        if 20231201 <= date < 20240426:
            ITMX_x_pre = 1830
            ITMX_y_pre = 1400
        if 20240426 <= date < 20241017:
            ITMX_x_pre = 1835
            ITMX_y_pre = 1390
        if 20241017 <= date < 20241219:
            ITMX_x_pre = 1815
            ITMX_y_pre = 1410
        if 20241219 == date and times < 103000:
            ITMX_x_pre = 1810
            ITMX_y_pre = 1395
        if 20241219 <= date < 20241223:
            ITMX_x_pre = 1815
            ITMX_y_pre = 1410
        if 20241223 == date and times < 103000:
            ITMX_x_pre = 1790
            ITMX_y_pre = 1395
        if 20241223 == date and times > 103000:
            ITMX_x_pre = 1815
            ITMX_y_pre = 1410
        if 20241226 == date and times < 103000:
            ITMX_x_pre = 1785
            ITMX_y_pre = 1390
        if 20241226 == date and times > 103000:
            ITMX_x_pre = 1815
            ITMX_y_pre = 1410
        if 20241230 == date and times < 90000:
            ITMX_x_pre = 1800
            ITMX_y_pre = 1390
        if 20241230 == date and times > 90000:
            ITMX_x_pre = 1820
            ITMX_y_pre = 1400
        if 20250107 == date and times < 90000:
            ITMX_x_pre = 1800
            ITMX_y_pre = 1390
        if 20250107 == date and times > 90000:
            ITMX_x_pre = 1810
            ITMX_y_pre = 1390            
        if 20250107 < date < 20260818:
            ITMX_x_pre = 1810
            ITMX_y_pre = 1390
            
        #ITMX_r_pre_large = ITMX_r_pre + 27
        #ITMX_r_pre_small = ITMX_r_pre - 27
        ITMX_r_pre_large = ITMX_r_pre + 17
        ITMX_r_pre_small = ITMX_r_pre - 17        
        return ITMX_x_pre, ITMX_y_pre, ITMX_r_pre

    if mirror_name == "ITMY":
        if 20230707 <= date < 20240624:
            ITMY_x_pre = 2241
            ITMY_y_pre = 1258
        if 20240624 <= date < 20240705:
            ITMY_x_pre = 2241
            ITMY_y_pre = 1358
        if 20240705 <= date < 20241025:
            ITMY_x_pre = 2246
            ITMY_y_pre = 1350
        if 20241025 <= date < 20241219:
            ITMY_x_pre = 2246
            ITMY_y_pre = 1340
        if 20241219 == date and times < 103000:
            ITMY_x_pre = 2246
            ITMY_y_pre = 1340            
        if 20241219 == date and times >= 103000: 
            ITMY_x_pre = 2242
            ITMY_y_pre = 1341
        if 20241223 == date and times < 90000: 
            ITMY_x_pre = 2242
            ITMY_y_pre = 1336
        if 20241223 == date and times > 90000: 
            ITMY_x_pre = 2242
            ITMY_y_pre = 1340
        if 20241226 == date and times < 90000: 
            ITMY_x_pre = 2242
            ITMY_y_pre = 1336
        if 20241226 == date and times > 90000: 
            ITMY_x_pre = 2242
            ITMY_y_pre = 1338
        if 20241230 == date and times < 90000: 
            ITMY_x_pre = 2242
            ITMY_y_pre = 1336
        if 20241230 == date and times > 90000: 
            ITMY_x_pre = 2242
            ITMY_y_pre = 1338
        if 20241230 < date < 20250321:
            ITMY_x_pre = 2242
            ITMY_y_pre = 1338                                                            
        if 20250321 <= date < 20261219:
            ITMY_x_pre = 2250
            ITMY_y_pre = 1337            
            
        ITMY_r_pre_large = int(ITMY_r_pre * 1.01)
        ITMY_r_pre_small = int(ITMY_r_pre * 0.99)
        #ITMY_r_pre_large = ITMY_r_pre + 27
        #ITMY_r_pre_small = ITMY_r_pre - 27        
        return ITMY_x_pre, ITMY_y_pre, ITMY_r_pre
            
# 円の方程式でカーブフィットする関数
def circle_fitting(xi, yi):
    M = np.array([[np.sum(xi ** 2), np.sum(xi * yi), np.sum(xi)],
                  [np.sum(xi * yi), np.sum(yi ** 2), np.sum(yi)],
                  [np.sum(xi), np.sum(yi), 1*len(xi)]])
    Y = np.array([[-np.sum(xi ** 3 + xi * yi ** 2)],
                  [-np.sum(xi ** 2 * yi + yi ** 3)],
                  [-np.sum(xi ** 2 + yi ** 2)]])
 
    M_inv = np.linalg.inv(M)
    X = np.dot(M_inv, Y)
    a = - X[0] / 2
    b = - X[1] / 2
    r = np.sqrt((a ** 2) + (b ** 2) - X[2])
    return a, b, r # 円の中心(a,b) 半径r

# 輪郭ごとに座標の平均を計算し新しい配列に格納
def data(contours):
    average_contours = []# 結果を入れるリストを用意
    for i in range(len(contours)):
        new_con =  np.mean(contours[i],axis = 0)
        average_contours.append(new_con)
    return average_contours

#
# fitting function
# r_pre is used as fixed radius
#
def f_ETMX(B, x):
    r = ETMX_r_pre
    return ((x[0]-B[0])**2+(x[1]-B[1])**2-r**2)

def f_ETMY(B, x):
    r = ETMY_r_pre
    return ((x[0]-B[0])**2+(x[1]-B[1])**2-r**2)

def f_ITMX(B, x):
    r = ITMX_r_pre
    return ((x[0]-B[0])**2+(x[1]-B[1])**2-r**2)

def f_ITMY(B, x):
    r = ITMY_r_pre
    return ((x[0]-B[0])**2+(x[1]-B[1])**2-r**2)


#
# pre-treatment ETMX figure before startubg fitting
#
def pretreatment_ETMX(img_orig, outdirname, figname, T, gain, flag_output=False):

    #if gain != 470 or T < 10:
    # if T < 10:
    #     print('error : gain and/or exposure are not proper. use gain=470 and exposure time>=10s.')
    #     sys.exit(1)
    
    height, width, _ = img_orig.shape

    # グレースケール
    img = cv2.cvtColor(img_orig, cv2.COLOR_BGR2GRAY) 
    
    # 白黒二値化
    print(ETMX_threshold_low, ETMX_threshold_high)
    img = cv2.threshold(img, ETMX_threshold_low, ETMX_threshold_high, cv2.THRESH_BINARY)[1]    

    # マスク作成 (黒く塗りつぶす画素の値は0)
    mask = np.zeros((height, width), dtype=np.uint8)
    # 円を描画する関数 circle() を利用してマスクの残したい部分を 255 にしている。
    center = (ETMX_x_pre, ETMX_y_pre)
    cv2.circle(mask, center, radius=ETMX_r_pre_large, color=255, thickness=-1)
    cv2.circle(mask, center, radius=ETMX_r_pre_small, color=0, thickness=-1)

    img[mask==0] = [0]  # mask の値が 0 の画素は黒で塗りつぶす。
   
    h0, h1, h2, h3, h4, h5 = [int(x*height) for x in ETMX_height_ratios]

#     img = cv2.rectangle(img, (0, h0), (width, h1), (255, 255, 255), 5)
#     img = cv2.rectangle(img, (0, h2), (width, h3), (255, 255, 255), 5)
#     img = cv2.rectangle(img, (0, h4), (width, h5), (255, 255, 255), 5)
    img = cv2.rectangle(img, (0, h0), (width, h1), (0, 0, 0), -1)
    img = cv2.rectangle(img, (0, h2), (width, h3), (0, 0, 0), -1)
    img = cv2.rectangle(img, (0, h4), (width, h5), (0, 0, 0), -1)
    
    if flag_output:
        fname="%s/crop_%s" % (outdirname, figname)
        cv2.imwrite(fname, img)

    # 輪郭を抽出
    contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    ccc = []
    for contour in contours:
        area = cv2.contourArea(contour)    
        if area < area_th:
            ccc.append(contour)
    #print(ccc)
    if len(ccc) > 0:
        contours = ccc    
    return contours
    


#
# pre-treatment ETMY figure before startubg fitting
#
def pretreatment_ETMY(img_orig, outdirname, figname, T, gain, flag_output=False):

    # if T < 10:
    #     print('error : gain and/or exposure are not proper. use gain=470 and exposure time>=10s.')
    #     sys.exit(1)
   
    height, width, _ = img_orig.shape

#     img_c = img_orig
#     if flag_output:
#         img_c = cv2.circle(img_c, (ETMY_x_pre, ETMY_y_pre), ETMY_r_pre, (255, 0, 255), 2)
#         # center of circle
#         img_c =cv2.circle(img_c, center=(ETMY_x_pre, ETMY_y_pre), radius=8, color=(255, 0, 255),
#            thickness=-1, lineType=cv2.LINE_4)
        
#         fname="%s/c_%s" % (outdirname, figname)
#         cv2.imwrite(fname, img_c)

    # HSVでの色抽出
    hsvLower = np.array(ETMY_HSV_lower)    # 抽出する色の下限(HSV)
    hsvUpper = np.array(ETMY_HSV_upper)    # 抽出する色の上限(HSV)

    hsv = cv2.cvtColor(img_orig, cv2.COLOR_BGR2HSV) # 画像をHSVに変換
    hsv_mask = cv2.inRange(hsv, hsvLower, hsvUpper)    # HSVからマスクを作成
    hsv_img = cv2.bitwise_and(img_orig, img_orig, mask=hsv_mask) # 元画像とマスクを合成
    if flag_output:
        fname="%s/hsv1_%s" % (outdirname, figname)
        cv2.imwrite(fname, hsv_img)
    
    # グレースケール
    img = cv2.cvtColor(hsv_img, cv2.COLOR_BGR2GRAY)     
    # 白黒二値化
    img = cv2.threshold(img, ETMY_threshold_low, ETMY_threshold_high, cv2.THRESH_BINARY)[1]    
     
    # save fig as png
    #fname="%s/binary_%s" % (outdirname, figname)
    #cv2.imwrite(fname, img)    
    
    # マスク作成 (黒く塗りつぶす画素の値は0)
    mask = np.zeros((height, width), dtype=np.uint8)
    # 円を描画する関数 circle() を利用してマスクの残したい部分を 255 にしている。
    center = (ETMY_x_pre, ETMY_y_pre)
    print(center)
    cv2.circle(mask, center, radius=ETMY_r_pre_large, color=255, thickness=-1)
    cv2.circle(mask, center, radius=ETMY_r_pre_small, color=0, thickness=-1)

    img[mask==0] = [0]  # mask の値が 0 の画素は黒で塗りつぶす。

    h0, h1, h2, h3, h4, h5 = [int(x*height) for x in ETMY_height_ratios]
    w0, w1, w2, w3 = [int(x*width) for x in ETMY_width_ratios]

#     img = cv2.rect angle(img, (0, h0), (width, h1), (255, 255, 255), 5)
#     img = cv2.rectangle(img, (0, h2), (width, h3), (255, 255, 255), 5)
#     img = cv2.rectangle(img, (0, h4), (width, h5), (255, 255, 255), 5)
    img = cv2.rectangle(img, (0, h0), (width, h1), (0, 0, 0), -1)
    img = cv2.rectangle(img, (0, h2), (width, h3), (0, 0, 0), -1)
    img = cv2.rectangle(img, (0, h4), (width, h5), (0, 0, 0), -1)
    img = cv2.rectangle(img, (w0, 0), (w1, height), (0, 0, 0), -1) 
    img = cv2.rectangle(img, (w2, 0), (w3, height), (0, 0, 0), -1) 
    

    if flag_hsv2:
        # ここからimg2の話
        # 右上を別のHSVフィルターで取り出す
        # HSVでの色抽出
        hsvLower = np.array(ETMY_HSV_lower2)    # 抽出する色の下限(HSV)
        hsvUpper = np.array(ETMY_HSV_upper2)    # 抽出する色の上限(HSV)

        hsv = cv2.cvtColor(img_orig, cv2.COLOR_BGR2HSV) # 画像をHSVに変換
        hsv_mask = cv2.inRange(hsv, hsvLower, hsvUpper)    # HSVからマスクを作成
        hsv_img = cv2.bitwise_and(img_orig, img_orig, mask=hsv_mask) # 元画像とマスクを合成

        if flag_output:
            fname="%s/hsv2_%s" % (outdirname, figname)
            cv2.imwrite(fname, hsv_img)    

        # グレースケール
        img2 = cv2.cvtColor(hsv_img, cv2.COLOR_BGR2GRAY)     
        # 白黒二値化
        img2 = cv2.threshold(img2, ETMY_threshold_low2, ETMY_threshold_high2, cv2.THRESH_BINARY)[1]        


        print(ETMY_r_pre_small2, ETMY_r_pre_large2)
        
        # マスク作成 (黒く塗りつぶす画素の値は0)
        mask = np.zeros((height, width), dtype=np.uint8)
        # 円を描画する関数 circle() を利用してマスクの残したい部分を 255 にしている。
        center = (ETMY_x_pre, ETMY_y_pre)
        cv2.circle(mask, center, radius=ETMY_r_pre_large2, color=255, thickness=-1)
        cv2.circle(mask, center, radius=ETMY_r_pre_small2, color=0, thickness=-1)

        img2[mask==0] = [0]  # mask の値が 0 の画素は黒で塗りつぶす。

        h10, h11 = [int(x*height) for x in ETMY_height_ratios2]
        w10, w11 = [int(x*width) for x in ETMY_width_ratios2]

        img2 = cv2.rectangle(img2, (0, 0), (width, h10),  (0, 0, 0), -1)     
        img2 = cv2.rectangle(img2, (0, h11), (width, height), (0, 0, 0), -1)     
        img2 = cv2.rectangle(img2, (0, 0), (w10, height), (0, 0, 0), -1)     
        img2 = cv2.rectangle(img2, (w11, 0), (width, height), (0, 0, 0), -1)     

        # ここで2枚のimageを合成している
        img = img + img2    
 
    if flag_output:
        fname="%s/crop_%s" % (outdirname, figname)
        cv2.imwrite(fname, img)

                
    # 輪郭を抽出
    contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    ccc = []
    for contour in contours:
        area = cv2.contourArea(contour)    
        if area < area_th:
            ccc.append(contour)
    #print(ccc)
    if len(ccc) > 0:
        contours = ccc    

    # h0, h1, h2, h3, h4, h5 = [int(x*height) for x in ETMY_height_ratios]
    # w0, w1, w2, w3 = [int(x*width) for x in ETMY_width_ratios]
    # img = cv2.rectangle(img, (0, h0), (width, h1), (0, 0, 0), -1)
    # img = cv2.rectangle(img, (0, h2), (width, h3), (0, 0, 0), -1)
    # img = cv2.rectangle(img, (0, h4), (width, h5), (0, 0, 0), -1)
    # img = cv2.rectangle(img, (w0, 0), (w1, height), (0, 0, 0), -1) 
    # img = cv2.rectangle(img, (w2, 0), (w3, height), (0, 0, 0), -1) 
        
    if flag_output:
        img = np.zeros((height, width))
        img = cv2.rectangle(img_orig, (0, h0), (width, h1), (255, 0, 255), -1)
        img = cv2.rectangle(img, (0, h2), (width, h3), (255, 0, 255), -1)
        img = cv2.rectangle(img, (0, h4), (width, h5), (255, 0, 255), -1)
        img = cv2.rectangle(img, (w0, 0), (w1, height), (255, 0, 255), -1)
        img = cv2.rectangle(img, (w2, 0), (w3, height), (255, 0, 255), -1)
        fname="%s/mask1_%s" % (outdirname, figname)
        cv2.imwrite(fname, img)

        # img = np.zeros((height, width))        
        # img = cv2.rectangle(img_orig, (0, 0), (width, h5),  (255, 0, 0), -1)  
        # img = cv2.rectangle(img, (0, h6), (width, height),  (255, 0, 0), -1)
        # img = cv2.rectangle(img, (0, 0), (w2, height),      (255, 0, 0), -1) 
        # img = cv2.rectangle(img, (w3, 0), (width, height),  (255, 0, 0), -1)
        # fname="%s/mask2_%s" % (outdirname, figname)
        # cv2.imwrite(fname, img)
                
    return contours


#
# pre-treatment ITMX figure before startubg fitting
#
def pretreatment_ITMX(img_orig, outdirname, figname, T, gain, flag_output=False):
    
    # if gain != 470 or T != 20:
    #     print('error : gain and/or exposure are not proper for ITMX. use gain=470 and exposure time=20s.')
    #     sys.exit(1)

    height, width, _ = img_orig.shape  
    
    img = img_orig

    # HSVでの色抽出
    hsvLower = np.array(ITMX_HSV_lower)    # 抽出する色の下限(HSV)
    hsvUpper = np.array(ITMX_HSV_upper)    # 抽出する色の上限(HSV)

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV) # 画像をHSVに変換
    hsv_mask = cv2.inRange(hsv, hsvLower, hsvUpper)    # HSVからマスクを作成
    hsv_img = cv2.bitwise_and(img, img, mask=hsv_mask) # 元画像とマスクを合成
        
    # RとBの要素を0に変換
    hsv_img[:, :, 0] = 0  # R
    hsv_img[:, :, 2] = 0  # B

    if flag_output:
        fname="%s/hsv_%s" % (outdirname, figname)
        cv2.imwrite(fname, hsv_img)
    
    # グレースケール変換
    hsv_g_gray = cv2.cvtColor(hsv_img, cv2.COLOR_BGR2GRAY)

    # 二値化
    img = cv2.threshold(hsv_g_gray, ITMX_threshold_low, ITMX_threshold_high, cv2.THRESH_BINARY)[1]
    
    # 円でのmask作成 (黒く塗りつぶす画素の値は0)
    mask = np.zeros((height, width), dtype=np.uint8)
    # 円を描画する関数 circle() を利用してマスクの残したい部分を 255 にしている。
    center = (ITMX_x_pre, ITMX_y_pre)
    cv2.circle(mask, center, radius=ITMX_r_pre_large, color=255, thickness=-1)
    cv2.circle(mask, center, radius=ITMX_r_pre_small, color=0, thickness=-1)        

    img[mask==0] = [0]  # mask の値が 0 の画素は黒で塗りつぶす。
        
    h0, h1, h2, h3 = [int(x*height) for x in ITMX_height_ratios]
    w0, w1 = [int(x*width) for x in ITMX_width_ratios]

    img = cv2.rectangle(img, (0, h0), (width, h1), (0, 0, 0), -1)
    img = cv2.rectangle(img, (0, h2), (width, h3), (0, 0, 0), -1)
    img = cv2.rectangle(img, (w0, 0), (w1, height), (0, 0, 0), -1)
    
    ###################################

    if flag_output:
        fname="%s/crop_%s" % (outdirname, figname)
        cv2.imwrite(fname, img)
       
        #img = np.zeros((height, width))
        img = cv2.rectangle(img, (0, h0), (width, h1), (255, 0, 255), -1)
        img = cv2.rectangle(img, (0, h2), (width, h3), (255, 0, 255), -1)
        img = cv2.rectangle(img, (w0, 0), (w1, height), (255, 0, 255), -1)
        
        fname="%s/mask1_%s" % (outdirname, figname)
        cv2.imwrite(fname, img)

    # 輪郭を抽出
    contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)    

    ccc = []
    for contour in contours:
        area = cv2.contourArea(contour)    
        if area < area_th:
            ccc.append(contour)
    #print(ccc)
    if len(ccc) > 0:
        contours = ccc    

    return contours



#
# pre-treatment ITMY figure before startubg fitting
#
def pretreatment_ITMY(img_orig, outdirname, figname, T, gain, flag_output=False):
    
    # if gain != 470 or T != 20:
    #     print('error : gain and/or exposure are not proper for ITMY. use gain=470 and exposure time=20s.')
    #     sys.exit(1)

    height, width, _ = img_orig.shape  
    
    img = img_orig
    
    # HSVでの色抽出
    hsvLower = np.array(ITMY_HSV_lower)    # 抽出する色の下限(HSV)
    hsvUpper = np.array(ITMY_HSV_upper)    # 抽出する色の上限(HSV)

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV) # 画像をHSVに変換
    hsv_mask = cv2.inRange(hsv, hsvLower, hsvUpper)    # HSVからマスクを作成
    hsv_img = cv2.bitwise_and(img, img, mask=hsv_mask) # 元画像とマスクを合成

    # RとBの要素を0に変換
    hsv_img[:, :, 0] = 0  # R
    hsv_img[:, :, 2] = 0  # B

    if flag_output:
        fname="%s/hsv_%s" % (outdirname, figname)
        cv2.imwrite(fname, hsv_img)
    
    # グレースケール変換
    hsv_g_gray = cv2.cvtColor(hsv_img, cv2.COLOR_BGR2GRAY)

    # 二値化
    img = cv2.threshold(hsv_g_gray, ITMY_threshold_low, ITMY_threshold_high, cv2.THRESH_BINARY)[1]

    # 円でのmask作成 (黒く塗りつぶす画素の値は0)
    mask = np.zeros((height, width), dtype=np.uint8)
    # 円を描画する関数 circle() を利用してマスクの残したい部分を 255 にしている。
    center = (ITMY_x_pre, ITMY_y_pre)
    print(ITMY_r_pre_large, ITMY_r_pre_small)
    print(center)
    cv2.circle(mask, center, radius=ITMY_r_pre_large, color=255, thickness=-1)
    cv2.circle(mask, center, radius=ITMY_r_pre_small, color=0, thickness=-1)        

    img[mask==0] = [0]  # mask の値が 0 の画素は黒で塗りつぶす。

    # h0, h1 = [int(x*height) for x in ITMY_height_ratios]
    # img = cv2.rectangle(img, (0, h0), (width, h1), (0, 0, 0), -1)
    
    h0, h1, h2, h3 = [int(x*height) for x in ITMY_height_ratios]

    # img = cv2.rectangle(img, (0, h0), (width, h1), (255, 255, 255), 5)
    # img = cv2.rectangle(img, (0, h2), (width, h3), (255, 255, 255), 5)
    img = cv2.rectangle(img, (0, h0), (width, h1), (0, 0, 0), -1)
    img = cv2.rectangle(img, (0, h2), (width, h3), (0, 0, 0), -1)

    if flag_output:
        img = cv2.rectangle(img, (0, h0), (width, h1), (255, 0, 255), -1)
        img = cv2.rectangle(img, (0, h2), (width, h3), (255, 0, 255), -1)        
        fname="%s/mask1_%s" % (outdirname, figname)
        cv2.imwrite(fname, img)
    
    # 輪郭を抽出
    contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)    

    ccc = []
    for contour in contours:
        area = cv2.contourArea(contour)    
        if area < area_th:
            ccc.append(contour)
    #print(ccc)
    if len(ccc) > 0:
        contours = ccc    

    return contours

#
# run fit
#
def fit_image(outdirname, outdirname_sym, dirname, dir_base_fit, figname, mirror_name, gain, T, flag_output=False, mode = ""):

    fname="%s/%s" % (dirname, figname)    
    
    # Load an color image
    img_orig = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
    img_orig = cv2.cvtColor(img_orig, cv2.COLOR_BGR2RGB)
    height, width, _ = img_orig.shape
   
    #
    # retreatment for ETMX, before fitting
    #
    x_pre, y_pre, r_pre = get_pre_value_for_fitting(mirror_name, figname)
    print(x_pre, y_pre, r_pre)
    if mirror_name == "ETMX":
        ETMX_x_pre, ETMX_y_pre, ETMX_r_pre = x_pre, y_pre, r_pre
        contours = pretreatment_ETMX(img_orig, outdirname, figname, T, gain, flag_output=flag_output)
    if mirror_name == "ETMY":
        ETMY_x_pre, ETMY_y_pre, ETMY_r_pre = x_pre, y_pre, r_pre        
        contours = pretreatment_ETMY(img_orig, outdirname, figname, T, gain, flag_output=flag_output)
    if mirror_name == "ITMX":
        ITMX_x_pre, ITMX_y_pre, ITMX_r_pre = x_pre, y_pre, r_pre        
        contours = pretreatment_ITMX(img_orig, outdirname, figname, T, gain, flag_output=flag_output)
    if mirror_name == "ITMY":
        ITMY_x_pre, ITMY_y_pre, ITMY_r_pre = x_pre, y_pre, r_pre        
        contours = pretreatment_ITMY(img_orig, outdirname, figname, T, gain, flag_output=flag_output)

    if len(contours) ==0:
        message = "Error : number of contour is zero. Adjust exposure time"
        dump_error_message_and_exit(message, dir_base_fit)
    
    # 余計な次元削除
    contours = [np.squeeze(cnt, axis=1) for cnt in contours]
    #print("number of island = %d" % len(contours))

    if flag_output:
        fname="%s/%s" % (dirname, figname)
        img_orig = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
        #img_orig = cv2.cvtColor(img_orig, cv2.COLOR_BGR2RGB)
        img_contour = cv2.drawContours(img_orig, contours, -1, (255, 0, 255), 2) # magenta
        fname="%s/contour_%s" % (outdirname, figname)
        cv2.imwrite(fname, img_contour)
    
    
    tmp   = np.floor(data(contours))
    xlist = tmp.transpose()[0]
    ylist = tmp.transpose()[1]
    xx    = np.array([xlist, ylist])

    if mirror_name == "ETMX":    
        mdr    = odr.Model(f_ETMX, implicit=True)
    if mirror_name == "ETMY":    
        mdr    = odr.Model(f_ETMY, implicit=True)
    if mirror_name == "ITMX":    
        mdr    = odr.Model(f_ITMX, implicit=True)        
    if mirror_name == "ITMY":    
        mdr    = odr.Model(f_ITMY, implicit=True)        
    mydata = odr.Data(xx, y=1)
    myodr  = odr.ODR(mydata, mdr, beta0=[x_pre, y_pre, r_pre])
    myoutput = myodr.run()
    #myoutput.pprint()

    a = myoutput.beta[0]
    b = myoutput.beta[1]
    r = abs(myoutput.beta[2])

    x_err, y_err, r_err = myoutput.sd_beta
    r_err = 0 # here we use fixed radius, so no error


    
    if flag_output_fitting:
        theta = np.linspace(0, 2 * np.pi, 100) #角度 [rad]
        x = r * np.sin(theta) + a
        y = r * np.cos(theta) + b

        fig = plt.figure(figsize = (12, 12))
        ax = fig.add_subplot()
        plt.scatter(a, b ,label = "estimated center", c = 'r')
        plt.plot(x, y, c = "r", label = "estimated (N of island = %d)" % len(contours))
        # データをプロット
        plt.scatter(xlist, ylist, label = "samples", c = 'black')

        # 軸の範囲設定
        ax.set_xlim(500, 3500)
        ax.set_ylim(3000, 0)
        plt.legend(loc='lower left')
        
        fname="%s/fitting_%s" % (outdirname, figname)
        fig.savefig(fname)    
       
    
    fname="%s/%s" % (dirname, figname)
    img_estimated = cv2.imread(fname, cv2.IMREAD_UNCHANGED)    

    #
    # center of circle with yellow
    # color=(255, 0, 255), # magenta
    # color=(0, 255, 255) # yellow
    # color=(0, 0, 255) # lime    
    #
    center=(int(a), int(b))
    img_estimated =cv2.circle(img_estimated, center=center, radius=16, color=(0, 255, 255), # yellow
           thickness=-1, lineType=cv2.LINE_4)
    img_estimated =cv2.circle(img_estimated, center=center, radius=int(r), color=(0, 255, 255), # yellow
           thickness=3, lineType=cv2.LINE_4)
    #
    # add reticle
    #
    img_estimated =cv2.line(img_estimated, (0, center[1]), (width, center[1]), color=(0, 255, 255), # yellow
           thickness=3, lineType=cv2.LINE_4)
    img_estimated =cv2.line(img_estimated, (center[0], 0), (center[0], height), color=(0, 255, 255), # yellow
           thickness=3, lineType=cv2.LINE_4)

    reticle_size = 128    
    for i in range(-3, 4):
        img_estimated =cv2.line(img_estimated, (center[0] - reticle_size, center[1]+int(r*i/3.0)), (center[0] + reticle_size, center[1]+int(r*i/3.0)), color=(0, 255, 255), # yellow
                                thickness=3, lineType=cv2.LINE_4)
        img_estimated =cv2.line(img_estimated, (center[0]+int(r*i/3.0), center[1] - reticle_size), (center[0]+int(r*i/3.0), center[1] + reticle_size), color=(0, 255, 255), # yellow
               thickness=3, lineType=cv2.LINE_4)
        
    #
    # extract date when TCam took the figure
    #
    year=figname.split("_")[3]
    mm=figname.split("_")[4][0:2]
    dd=figname.split("_")[4][2:4]
    hour=figname.split("_")[5][0:2]
    minute=figname.split("_")[5][2:4]
    second=figname.split("_")[5][4:6]
    date=year + "/" + mm + "/" + dd + " " + hour + ":" + minute 

    # text in left bottom
    cv2.putText(img_estimated, 'Taken at %s (%s)' % (date, mirror_name), (100, int(height*0.93)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)    
    cv2.putText(img_estimated, 'gain = %s, T = %s [s]' % (gain, T), (100, int(height*0.95)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)    
    cv2.putText(img_estimated, 'center = (%.1f +/- %.1f, %.1f +/- %.1f), radius = %.1f +/- %.1f  (%s)' % (a, x_err, b, y_err, r, r_err, mode), (100, int(height*0.97)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)
    
    # save fig as png
    tmp = figname.split(".")[0] + "_fit.png"
    fname="%s/%s" % (outdirname, tmp)
    cv2.imwrite(fname, img_estimated)

    if flag_output:
        #
        # overlay with the latest reference center
        #
        fitting_date, x_ref, y_ref, _, _, r_ref = get_latest_mirror_center(dir_base_fit, mirror_name)
        fname="%s/%s" % (dirname, figname)
        img_orig = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
        #img_orig = cv2.cvtColor(img_orig, cv2.COLOR_BGR2RGB)

        center_ref=(int(x_ref), int(y_ref))
        center=(int(a), int(b))        

        img_orig =cv2.circle(img_orig, center=center, radius=16, color=(0, 255, 255), # yellow
               thickness=-1, lineType=cv2.LINE_4)
        img_orig =cv2.circle(img_orig, center=center, radius=int(r), color=(0, 255, 255), # yellow
               thickness=3, lineType=cv2.LINE_4)
        
        #
        # add reticle
        #
        img_orig =cv2.line(img_orig, (0, center[1]), (width, center[1]), color=(0, 255, 255), # yellow
               thickness=3, lineType=cv2.LINE_4)
        img_orig =cv2.line(img_orig, (center[0], 0), (center[0], height), color=(0, 255, 255), # yellow
               thickness=3, lineType=cv2.LINE_4)

        reticle_size = 128    
        for i in range(-3, 4):
            img_orig =cv2.line(img_orig, (center[0] - reticle_size, center[1]+int(r*i/3.0)), (center[0] + reticle_size, center[1]+int(r*i/3.0)), color=(0, 255, 255), # yellow
                                    thickness=3, lineType=cv2.LINE_4)
            img_orig =cv2.line(img_orig, (center[0]+int(r*i/3.0), center[1] - reticle_size), (center[0]+int(r*i/3.0), center[1] + reticle_size), color=(0, 255, 255), # yellow
                   thickness=3, lineType=cv2.LINE_4)

        # text in left bottom
        cv2.putText(img_orig, 'Taken at %s (%s)' % (date, mirror_name), (100, int(height*0.93)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)    
        cv2.putText(img_orig, 'gain = %s, T = %s [s]' % (gain, T), (100, int(height*0.95)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)    
        cv2.putText(img_orig, 'center = (%.1f +/- %.1f, %.1f +/- %.1f), radius = %.1f +/- %.1f  (%s)' % (a, x_err, b, y_err, r, r_err, mode), (100, int(height*0.97)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(0, 255, 255), thickness=5)
        
        # text in left bottom for reference value
        cv2.putText(img_orig, 'Overray with the latest reference fitted at %s (%s)' % (fitting_date, mirror_name), (100, int(height*0.89)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)    
        cv2.putText(img_orig, 'center = (%.1f, %.1f), radius = %.1f  (%s)' % (center_ref[0], center_ref[1], r, mode), (100, int(height*0.91)), cv2.FONT_HERSHEY_PLAIN, fontScale=4.0, color=(255, 0, 255), thickness=5)

        img_orig =cv2.circle(img_orig, center=center_ref, radius=16, color=(255, 0, 255), # magenta
               thickness=-1, lineType=cv2.LINE_4)
        img_orig =cv2.circle(img_orig, center=center_ref, radius=int(r), color=(255, 0, 255), # magenta
               thickness=3, lineType=cv2.LINE_4)
        #
        # add reticle
        #
        img_orig =cv2.line(img_orig, (0, center_ref[1]), (width, center_ref[1]), color=(255, 0, 255), # magenta
               thickness=3, lineType=cv2.LINE_4)
        img_orig =cv2.line(img_orig, (center_ref[0], 0), (center_ref[0], height), color=(255, 0, 255), # magenta
               thickness=3, lineType=cv2.LINE_4)

        # reticle_size = 128
        # for i in range(-3, 4):
        #     img_orig =cv2.line(img_orig, (center[0] - reticle_size, center[1]+int(r*i/3.0)), (center[0] + reticle_size, center[1]+int(r*i/3.0)), color=(255, 0, 255), # magenta
        #                             thickness=3, lineType=cv2.LINE_4)
        #     img_orig =cv2.line(img_orig, (center[0]+int(r*i/3.0), center[1] - reticle_size), (center[0]+int(r*i/3.0), center[1] + reticle_size), color=(255, 0, 255), # magenta
        #            thickness=3, lineType=cv2.LINE_4)

        
        fname="%s/last_fitting_%s" % (outdirname, figname)
        cv2.imwrite(fname, img_orig)        
    
    plt.close('all') # windowを閉じないと、matplotlibが開きすぎとwarningを出すので

    return a, b, r, x_err, y_err, r_err

def create_symbolic_link(figname, outdirname, dir_base_fit, mirror_name):

    # only for Kamioka computer
    if os.path.isdir("/data2"):
        # change directory for relative symbolic link
        os.chdir(dir_base_fit)

        tmp = figname.split(".")[0] + "_fit.png"
        fname="%s/%s" % (outdirname, tmp)
        
        fname_sym = "latest.png"
        # update symbolic link
        if os.path.islink(fname_sym) == True:
            os.remove(fname_sym)
        tmp = "/".join(fname.split("/")[4:]) # example : '2022/0329/TCam_ETMX_01110_2022_0329_141445_fit.png'
        os.symlink(tmp, fname_sym)

        fname_sym = "latest.txt"
        # update symbolic link
        if os.path.islink(fname_sym) == True:
            os.remove(fname_sym)
        tmp = "/".join(fname.split("/")[4:]) # example : '2022/0329/TCam_ETMX_01110_2022_0329_141445_fit.png'
        tmp = tmp.replace('png', 'txt')
        os.symlink(tmp, fname_sym)
        
        # copy dat file including update time
        dir_base = dir_base_fit.replace('TCam_fit', 'TCam') # example : '/data2/TCam/ETMX'
        #shutil.copy('%s/lastupdate.txt' % dir_base, '%s/lastupdate.txt' % dir_base_fit)        
        cmd='cp %s/lastupdate.txt %s/lastupdate.txt > /dev/null 2>&1' % (dir_base, dir_base_fit)
        subprocess.run(cmd, shell=True)

        # make thumbnail figure
        cmd='cwebp -resize 518 0 -o thumb.webp %s > /dev/null 2>&1' % fname
        subprocess.run(cmd, shell=True)

#
# extract gain and exposure time from configuration file of TCam
#
def get_gain_T(config_file):

    config_file = dirname + "/" + figname.split(".")[0] + ".txt"
    #print(config_file)
    
    with open(config_file, 'r') as outfile:
        lines = outfile.readlines()
        outfile.close()

        gain = 0
        T = 0
        lines = [line.strip() for line in lines]        
        for line in lines:
            if "value_seconds" in line:
                T = line.split(" ")[1]

            if "controls_[2]_value" in line:
                gain = line.split(" ")[1]
    return int(gain), int(T)


#
# save fitting results as text and csv files
#
def save_textfile(figname, dir_base_fit, x, y, r, x_err, y_err, r_err, gain, T, mirror_name, flag_update_reference=False, flag_add_csv=False):

    #
    # Output result as text file (overwrite)
    #            
    textname="%s/%s_fit.txt" % (outdirname, figname.split(".")[0])
    with open(textname, 'w') as outfile: # a : 追記モード, w : 上書きモード
        dt_now = datetime.datetime.now()

        #line="%s %.2f %.2f %.2f %s %s %.2f %.2f %.2f\n" % (date, x, y, r, T, gain, x_err, y_err, r_err)
        line = "%s\n" % dt_now
        line = line + "gain %d\n" % gain
        line = line + "exposure_time %d\n" % T
        line = line + "x %.2f\n" % x
        line = line + "y %.2f\n" % y
        line = line + "dx %.2f\n" % x_err
        line = line + "dy %.2f\n" % y_err
        line = line + "r %.2f\n" % r
        outfile.write(line)
        outfile.close

    #print(flag_add_csv, flag_update_reference)
    if flag_update_reference or flag_add_csv:
        
        # only for Kamioka machine
        if os.path.isdir("/data2"):
            csvname="%s/archive/fit_value_%s.csv" % (dir_base_fit, mirror_name)
        else:
            csvname="%s/fit_value_%s.csv" % (dir_base_fit, mirror_name)

        #
        # save header for csv
        # only for the first time
        #
        if not os.path.exists(csvname):
            csvline_header=["fitting date", "gain", "T", "x", "y", "x_error", "y_error", "r", "filename"]
            with open(csvname, 'a', newline='') as f:      
                writer_object = writer(f, lineterminator='\n')
                writer_object.writerow(csvline_header)
                csvline=[dt_now, gain, T, x, y, x_err, y_err, r, figname]
                writer_object.writerow(csvline)                
            f.close()            
            return
            
        #
        # If the result for same figure is in csv file,
        # comment out past result by adding #, and add latest result in the bottom
        #
        if os.path.isfile(csvname):

            #
            # list of past reulsts for same TCam photo
            #
            df = pd.read_csv(csvname)
            ll = df[df["filename"] == figname]
            reference_values = df.iloc[-1].copy(deep=True)  # deepcopy last fitting result
            # print("\n\n *** reference ***\n\n")
            # print(reference_values)
            # print(ll)

            if len(ll) == 0: # first fitting
                # no need to close dataframe
                with open(csvname, 'a', newline='') as f:  
                    writer_object = writer(f, lineterminator='\n')
                    csvline=[dt_now, gain, T, x, y, x_err, y_err, r, figname]
                    writer_object.writerow(csvline)
                    if flag_add_csv: # not update reference value
                        writer_object.writerow(reference_values)
                f.close()

            else: # not first fitting
                # add # in past fitting result for same image
                for i in list(ll.index):
                    df.loc[df.index==i, ["fitting date"]] = "#" + df.loc[df.index==i, ["fitting date"]]

                #
                # update the values to the latest one
                #
                line_to_add = ll.iloc[-1].copy(deep=True)  # deepcopy last fitting result
                #print(line_to_add)
                n = len(df["gain"])
                df.loc[n] = line_to_add
                df.loc[n, ["x"]] = x
                df.loc[n, ["y"]] = y
                df.loc[n, ["x_error"]] = x_err
                df.loc[n, ["y_error"]] = y_err
                df.loc[n, ["r"]] = r

                #print(df)
                
                # save as csv (overwrite)
                df.to_csv(csvname, index=False)

                if flag_add_csv: # not update reference value == add the reference value in the tail of csv
                    #and line_to_add["fitting date"] != reference_values["fitting date"]:
                    df = pd.read_csv(csvname)
                    # ll = df[df["filename"] == figname]
                    # # add # in past fitting result for same image
                    # print(ll)
                    # for i in list(ll.index):
                    #     df.loc[df.index==i, ["fitting date"]] = "#" + df.loc[df.index==i, ["fitting date"]]

                    kk = df[df["filename"] == reference_values["filename"]]
                    #print(kk)
                    for i in list(kk.index):
                        df.loc[df.index==i, ["fitting date"]] = "#" + df.loc[df.index==i, ["fitting date"]]
                    n = len(df["gain"])
                    # print("\n\n")
                    # print(n)
                    # print("\n\n")
                    df.loc[n] = reference_values
                    #df.loc[df.index==n-1, ["fitting date"]] = "#" + df.loc[df.index==i, ["fitting date"]]
                    
                    # save as csv (overwrite)
                    df.to_csv(csvname, index=False)

            #if os.path.isfile(csvname):
            csvname2="/mnt/cds_nfs/TCam/fit_value_%s.csv" % (mirror_name)            
            cmd='cp %s %s > /dev/null 2>&1' % (csvname, csvname2)
            subprocess.run(cmd, shell=True)
            
def dump_error_message_and_exit(message, dir_base_fit):
    print(message)

    if os.path.isdir("/data2"):
        cmd='echo "%s" > %s/lastupdate.txt' % (message, dir_base_fit)
        subprocess.run(cmd, shell=True)
    sys.exit(1)


def get_latest_mirror_center(dir_base_fit, mirror_name):

    csvname="%s/archive/fit_value_%s.csv" % (dir_base_fit, mirror_name)
    if os.path.exists(csvname):
        df = pd.read_csv(csvname)
        fitting_date = df.iloc[-1]["fitting date"]
        x_mirror = df.iloc[-1]["x"]
        y_mirror = df.iloc[-1]["y"]
        x_err_mirror = df.iloc[-1]["x_error"]
        y_err_mirror = df.iloc[-1]["y_error"]
        r_mirror = df.iloc[-1]["r"]
    else:
        x_err_mirror = 0
        y_err_mirror = 0
        fitting_date = "no data"
        if mirror_name == "ETMX":
            # 2022/08/29~
            x_mirror = 1925
            y_mirror = 1410
            r_mirror = 1246
        elif mirror_name == "ETMY":
            # 2022/08/29~
            x_mirror = 2220
            y_mirror = 980
            r_mirror = 1248
        elif mirror_name == "ITMX":
            # 2022/08/29~
            x_mirror = 1825
            y_mirror = 1363
            r_mirror = 630
        elif mirror_name == "ITMY":
            # 2022/08/29~
            x_mirror = 2251
            y_mirror = 1273
            r_mirror = 640
        else:
            x_mirror = 0
            y_mirror = 0
            r_mirror = 0
            
    return fitting_date, x_mirror, y_mirror, x_err_mirror, y_err_mirror, r_mirror  
    


    
#
# main
#
parser = argparse.ArgumentParser(prog="fit_Tcam.py", description="fitting Tcam image and estimate the circle of mirror")

parser.add_argument('-i', '--input', required = True, type=str, help = 'file PATH and name of inpute TCam image')
parser.add_argument('-r', '--update_reference', action='store_true', default = False, help = 'flag to add result in csv and to update reference value by result of fitting')
parser.add_argument('-c', '--add_csv', action='store_true', default = False, help = 'flag to add result in csv, but not to update reference value')

args = parser.parse_args()
input_figname = args.input # full path including dir + file name
flag_update_reference = args.update_reference
flag_add_csv          = args.add_csv

if flag_update_reference and flag_add_csv:
    message = "error : both options are selected (flag_update_reference and flag_add_csv). select one option."
    print(message)
    sys.exit(1)

figname = os.path.basename(input_figname)
dirname = os.path.dirname(input_figname)
tmp = figname.split("_") # ['TCam', 'ETMX', '01110', '2022', '0328', '102439.png']

#
# only for Kamioka computer
#
if os.path.isdir("/data2"):
    outdirname = "/data2/" + tmp[0] + "_fit" + "/" + tmp[1] + "/archive/" + tmp[3] + "/" + tmp[4]
    dir_base_fit = "/".join(input_figname.split("/")[0:4]).replace("TCam", "TCam_fit")     # /data2/TCam_fit/ETMX
#
# for other users 
#
else:
    outdirname = "./"
    dir_base_fit = "./"
outdirname_sym = tmp[1] + "/" # for doing `cd` command, only when there is `/data2` directory
mirror_name = tmp[1]
if mirror_name not in ["ITMX", "ETMX", "ITMY", "ETMY"]:
    message = "error : mirror name (%s) is out of support" % mirror_name
    dump_error_message_and_exit(message, dir_base_fit)
    
if not os.path.isfile(input_figname):
    message = 'error : figure not found. require FULL path to figure.'
    dump_error_message_and_exit(message, dir_base_fit)

config_file = dirname + "/" + figname.split(".")[0] + ".txt" 
if not os.path.isfile(config_file):
    message = 'error : configure file from TCam not found. configuration file should be located in same directory as figure.'
    dump_error_message_and_exit(message, dir_base_fit)
    
if not os.path.exists(outdirname):
    os.makedirs(outdirname)
   
#    
# extract gain and exposure time from configuration file of TCam
#
gain, T = get_gain_T(config_file)
# if T < 5:
#     message = "error : exposure time is required than 5 s. But not %f s" % T
#     dump_error_message_and_exit(message, dir_base_fit)
    #sys.exit(1)
    #print(gain, T)

print("analysis ready!")
    
#
# run fit
#
mode = "just fitting"
if flag_update_reference:
    mode = "updated reference value"
if flag_add_csv:
    mode = "without update reference value"
x, y, r, x_err, y_err, r_err = fit_image(outdirname, outdirname_sym, dirname, dir_base_fit, figname, mirror_name, gain, T, flag_output=flag_output, mode=mode)
print(x, y, r, x_err, y_err, r_err)

#
# error check
#
if x < 1.0 or y < 1.0:
    message = 'error : fitting failed.'
    dump_error_message_and_exit(message, dir_base_fit)

#
# create symbolic link (only for Kamioka computer)
#
create_symbolic_link(figname, outdirname, dir_base_fit, mirror_name)

#
# save fitting results as text and csv files
#
#save_textfile(figname, dir_base_fit, x, y, r, x_err, y_err, r_err, gain, T, mirror_name)
save_textfile(figname, dir_base_fit, x, y, r, x_err, y_err, r_err, gain, T, mirror_name, flag_update_reference=flag_update_reference, flag_add_csv=flag_add_csv)

print("fit_Tcam.py done without error.")
print("file = %s" % figname)
